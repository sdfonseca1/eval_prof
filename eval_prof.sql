-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 21-03-2020 a las 20:52:59
-- Versión del servidor: 10.4.12-MariaDB
-- Versión de PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `eval_prof`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `area_academica`
--

CREATE TABLE `area_academica` (
  `claveArea` varchar(3) NOT NULL COMMENT 'Identificador del área académica',
  `nombreArea` varchar(50) DEFAULT NULL COMMENT 'Nombre del área académica',
  `licenciatura` varchar(4) DEFAULT NULL COMMENT 'Clave de la licenciatura a la que pertenece.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `area_academica`
--

INSERT INTO `area_academica` (`claveArea`, `nombreArea`, `licenciatura`) VALUES
('IHM', 'Interacción Hombre-Máquina', 'ICO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asesor`
--

CREATE TABLE `asesor` (
  `claveAsesor` varchar(10) NOT NULL COMMENT 'Identificador del asesor',
  `rfc` varchar(10) DEFAULT NULL COMMENT 'RFC del asesor',
  `nombre` varchar(20) DEFAULT NULL COMMENT 'Nombre del asesor',
  `apellidoPaterno` varchar(20) DEFAULT NULL COMMENT 'Apellido paterno del asesor',
  `apellidoMaterno` varchar(20) DEFAULT NULL COMMENT 'Apellido materno del asesor',
  `telefono` varchar(10) DEFAULT NULL COMMENT 'Número telefónico del asesor',
  `email` varchar(40) DEFAULT NULL COMMENT 'Correo electrónico del asesor',
  `contrasena` varchar(40) DEFAULT NULL COMMENT 'Contraseña de acceso al sistema'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `asesor`
--

INSERT INTO `asesor` (`claveAsesor`, `rfc`, `nombre`, `apellidoPaterno`, `apellidoMaterno`, `telefono`, `email`, `contrasena`) VALUES
('AATS710526', 'ALTS710526', 'Silvia Edith', 'Albarran', 'Trujillo', '8521479632', 'salbarrant@profesor.mx', '123456'),
('ALBV791219', 'ALBV791219', 'Vladimir Angel', 'Albiter', 'Bernal', '8521476935', 'valbiterb@profesor.mx', '123456'),
('MACI730709', 'MACI730709', 'Ivan Galileo', 'Martinez', 'Cienfuegos', '8521476938', 'imartinezc@profesor.mx', '123456'),
('MAGL771206', 'MAGL771206', 'Lorena Elizabeth', 'Manjarrez', 'Garduño', '8521476936', 'lmanjarrezg@profesor.mx', '123456'),
('MARR770712', 'MARR770712', 'Jose Raymundo', 'Marcial', 'Romero', '8521476937', 'jmarcialr@profesor.mx', '123456'),
('RUVS830929', 'RUVS830929', 'Susana', 'Ruiz', 'Valdes', '8521476939', 'sruizv@profesor.mx', '123456'),
('SACP690110', 'SACP690110', 'Pablo', 'Salas', 'Castillo', '8521476940', 'psalac@profesor.mx', '123456'),
('SAGJ800708', 'SAGJ800708', 'Javier', 'Salas', 'García', '8521476941', 'jsalasg@profesor.mx', '123456'),
('VIGA680507', 'VIGA680507', 'Adriana', 'Vilchis', 'Gonzalez', '8521476943', 'avilchisg@profesor.mx', '123456'),
('VIIA560201', 'VIIA560201', 'César Augusto', 'Villalta', 'Iglesias', '8521476942', 'cvillasltai@profesor.mx', '123456');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comite_evaluador`
--

CREATE TABLE `comite_evaluador` (
  `claveComite` int(10) UNSIGNED NOT NULL,
  `revisor1` varchar(10) DEFAULT NULL COMMENT 'Clave del revisor 1',
  `revisor2` varchar(10) DEFAULT NULL COMMENT 'Clave del revisor 2',
  `revisor3` varchar(10) DEFAULT NULL COMMENT 'Clave del revisor 3',
  `notas1` text DEFAULT NULL COMMENT 'Comentarios del revisor 1',
  `notas2` text DEFAULT NULL COMMENT 'Comentarios del revisor 2',
  `notas3` text DEFAULT NULL COMMENT 'Comentarios del revisor 3'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador`
--

CREATE TABLE `coordinador` (
  `claveCoordinador` varchar(10) NOT NULL COMMENT 'Identificador del coordinador de la licenciatura',
  `rfc` varchar(10) DEFAULT NULL COMMENT 'RFC del coordinador',
  `licenciatura` varchar(4) DEFAULT NULL COMMENT 'Clave de la licenciatura a la que pertenece el coordinador',
  `nombre` varchar(20) DEFAULT NULL COMMENT 'Nombre del coordinador',
  `apellidoPaterno` varchar(20) DEFAULT NULL COMMENT 'Apellido paterno del coordinador',
  `apellidoMaterno` varchar(20) DEFAULT NULL COMMENT 'Apellido materno del coordinador',
  `telefono` varchar(10) DEFAULT NULL COMMENT 'Número telefónico del coordinador',
  `email` varchar(40) DEFAULT NULL COMMENT 'Correo electrónico del coordinador',
  `contrasena` varchar(40) DEFAULT NULL COMMENT 'Contraseña de acceso del coordinador'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `coordinador`
--

INSERT INTO `coordinador` (`claveCoordinador`, `rfc`, `licenciatura`, `nombre`, `apellidoPaterno`, `apellidoMaterno`, `telefono`, `email`, `contrasena`) VALUES
('FOHS960110', 'FOHS960110', 'ICO', 'David', 'Fonseca', 'Hernández', '7227807166', 'sdfonseca1@outlook.com', '1113637');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `depto_titulacion`
--

CREATE TABLE `depto_titulacion` (
  `claveDepTitu` varchar(10) NOT NULL COMMENT 'Identificador del encargado del depto. de titulación',
  `rfc` varchar(10) DEFAULT NULL COMMENT 'RFC del encargado del depto. de titulación',
  `nombre` varchar(20) DEFAULT NULL COMMENT 'Nombre del encargado del depto. de titulación',
  `apellidoPaterno` varchar(20) DEFAULT NULL COMMENT 'Apellido paterno del encargado del depto. de titulación',
  `apellidoMaterno` varchar(20) DEFAULT NULL COMMENT 'Apellido materno del encargado del depto. de titulación',
  `telefono` varchar(10) DEFAULT NULL COMMENT 'Número telefónico del encargado del depto. de titulación',
  `email` varchar(40) DEFAULT NULL COMMENT 'Correo electrónico del encargado del depto. de titulación',
  `contrasena` varchar(20) DEFAULT NULL COMMENT 'Contraseña de acceso del encargado del depto. de titulación'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escuela`
--

CREATE TABLE `escuela` (
  `claveEscuela` varchar(10) NOT NULL COMMENT 'Identificador de una escuela',
  `nombre` varchar(100) DEFAULT NULL COMMENT 'Nombre de la escuela'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `escuela`
--

INSERT INTO `escuela` (`claveEscuela`, `nombre`) VALUES
('FI', 'Facultad de Ingeniería');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `licenciatura`
--

CREATE TABLE `licenciatura` (
  `claveLicenciatura` varchar(4) NOT NULL COMMENT 'Clave de la licenciatura',
  `nombreLicenciatura` varchar(100) DEFAULT NULL COMMENT 'Nombre de la licenciatura'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `licenciatura`
--

INSERT INTO `licenciatura` (`claveLicenciatura`, `nombreLicenciatura`) VALUES
('ICI', 'Ingeniería Civil'),
('ICO', 'Ingeniería en Computación'),
('IEL', 'Ingeniería en Electrónica'),
('IME', 'Ingeniería Mécanica'),
('ISES', 'Ingeniería en Sistemas Energeticos Sustentables');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasante`
--

CREATE TABLE `pasante` (
  `clavePasante` int(7) UNSIGNED NOT NULL COMMENT 'Identificador del pasante solicitador',
  `nombre` varchar(20) DEFAULT NULL COMMENT 'Nombre del pasante',
  `apellidoPaterno` varchar(20) DEFAULT NULL COMMENT 'Apellido paterno del pasante',
  `apellidoMaterno` varchar(20) DEFAULT NULL COMMENT 'Apellido materno del pasante',
  `fechaNacimiento` date DEFAULT NULL COMMENT 'Fecha de nacimiento del pasante',
  `escuela` varchar(10) DEFAULT NULL COMMENT 'Clave de la escuela a la que pertenece el pasante',
  `licenciatura` varchar(4) DEFAULT NULL COMMENT 'Clave de la licenciatura a la que pertenece el pasante',
  `telefono` varchar(10) DEFAULT NULL COMMENT 'Número telefónico del pasante',
  `email` varchar(40) DEFAULT NULL COMMENT 'Correo electrónico del pasante',
  `contrasena` varchar(72) DEFAULT NULL COMMENT 'Contraseña de acceso del pasante',
  `aceptado` varchar(1) DEFAULT NULL COMMENT 'Estado de acceso al sistema'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pasante`
--

INSERT INTO `pasante` (`clavePasante`, `nombre`, `apellidoPaterno`, `apellidoMaterno`, `fechaNacimiento`, `escuela`, `licenciatura`, `telefono`, `email`, `contrasena`, `aceptado`) VALUES
(1110001, 'Juan', 'Escutia', 'Almada', '1994-10-11', 'FI', 'ICO', '5214841', 's@com.c', '$2y$10$syYQDiTrsUE1co0984hR8ODHxJY6CpUWX.ZaDS1VsAgTKjvUiJvKG', '0'),
(1110002, 'Juan', 'Escutia', 'Ramírez', '1994-11-26', 'FI', 'IEL', '7225456987', 'max@micorreo.com', '$2y$10$XLYKkVZ3csdf8xH6kGVekeQHZwrNcDV2KJstGn6S0fa6K2dEWVazy', '0'),
(1110003, 'Rodrigo Maximiliano ', 'Pérez', 'Remigio', '1994-11-26', 'FI', 'IEL', '7225487962', 'max@micorreo.com', '$2y$10$ecs4Xno6rloWobVCizsJ9.OGWAEbAMKUoAD13dO7VOYEwhfDknhs2', '0'),
(1110065, 'David', 'Fonseca', 'Hernández', '1996-01-10', 'FI', 'ICO', '7227807166', 'sdfonseca1@outlook.com', '$2y$10$tfel90bs.2uouzTb9K1/ruEC8Gox5kMbMSaTQoe5aJzM43c70jKle', '0'),
(1110089, 'David', 'Fonseca', 'Hernández', '1996-01-10', 'FI', 'ICO', '7227807166', 'sdfonseca1@outlook.com', '$2y$10$fPSMPJ7/6rz7caLUJM/.1.fw6f7BY845iIKfB337Mc2KI5qKCAiR6', '0'),
(1111025, 'Juan', 'Pérez', 'Kley', '1997-06-10', 'FI', 'ICO', '8957416325', 'juan_perez@hotmail.com', '$2y$10$CEdKGAQE3Nh.K/JJNHWgDOBJuxZ8x5To5bpbpHf/2PxcFqOFtF5.i', '0'),
(1113637, 'David', 'Fonseca', 'Hernández', '1996-01-10', 'FI', 'ICO', '7227807166', 'sdfonseca1@outlook.com', '$2y$10$TJs8zZ6fezcngcgZlak4g.dTxeeFp/aGEjjJRzkaKLjlDAQFl/onu', '1'),
(1113638, 'David', 'Fonseca', 'Hernández', '1996-03-02', 'FI', 'ICO', '1921682541', 'dfh10@yopmail.com', '$2y$10$a5dHyKrd9KM8T9m2mhmHAu0p.jaVOzgRqtYJLQIW0cfFGbeTvv2Z.', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasante_protocolo`
--

CREATE TABLE `pasante_protocolo` (
  `clavePasante` int(7) UNSIGNED NOT NULL,
  `tipoTrabajo` varchar(3) NOT NULL,
  `claveProtocoloTemporal` varchar(15) NOT NULL,
  `estadoProtocolo` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pro_aem`
--

CREATE TABLE `pro_aem` (
  `claveAem` varchar(10) DEFAULT NULL COMMENT 'Identificador del autoempleo profesional',
  `claveTemporalProtocolo` varchar(15) NOT NULL COMMENT 'Clave temporal del protocolo',
  `clavePasante` int(7) UNSIGNED NOT NULL COMMENT 'Identificador del pasante',
  `fechaEntrega` date DEFAULT NULL COMMENT 'Fecha de entrega del protocolo de autoempleo profesional',
  `fechaDictamen` date DEFAULT NULL COMMENT 'Fecha de respuesta',
  `titulo` text DEFAULT NULL COMMENT 'Objeto de estudio',
  `periodoInicio` date DEFAULT NULL COMMENT 'Fecha de inicio de la participación',
  `periodoTermino` date DEFAULT NULL COMMENT 'Fecha de termino de la participación',
  `asesor` varchar(10) DEFAULT NULL COMMENT 'Clave del asesor asignado',
  `areaAcademica` varchar(3) DEFAULT NULL COMMENT 'Clave del área académica al que pertenece',
  `planteamientoProblema` text DEFAULT NULL COMMENT 'Planteamiento del problema',
  `justificacion` text DEFAULT NULL COMMENT 'Descripción de la necesidad de abordar el tema',
  `objetivoGeneral` text DEFAULT NULL COMMENT 'Indicar que experiencia se pretende documentar',
  `referencias` text DEFAULT NULL COMMENT 'Referencias bibliográficas',
  `cronograma` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Cronograma de actividades' CHECK (json_valid(`cronograma`))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Disparadores `pro_aem`
--
DELIMITER $$
CREATE TRIGGER `ELIMINAR_PROTOCOLO` AFTER DELETE ON `pro_aem` FOR EACH ROW BEGIN
    DELETE 
      FROM pasante_protocolo
     WHERE pasante_protocolo.clavePasante = old.clavePasante AND
           pasante_protocolo.claveProtocoloTemporal=old.claveTemporalProtocolo;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `REGISTRO_PROTOCOLO` AFTER INSERT ON `pro_aem` FOR EACH ROW BEGIN
INSERT INTO pasante_protocolo
(clavePasante, tipotrabajo, claveProtocoloTemporal)
VALUES
(NEW.clavePasante, "AEM", NEW.claveTemporalProtocolo);
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pro_art`
--

CREATE TABLE `pro_art` (
  `claveArticulo` varchar(10) NOT NULL COMMENT 'Identificador del arículo',
  `claveTemporalProtocolo` varchar(15) NOT NULL COMMENT 'Clave temporal del protocolo',
  `clavePasante` int(7) UNSIGNED NOT NULL COMMENT 'Identificador del pasante',
  `fechaEntrega` date DEFAULT NULL COMMENT 'Fecha de entrega del protocolo del artículo',
  `fechaDictamen` date DEFAULT NULL COMMENT 'Fecha de presentación del examen profesional',
  `tituloArticulo` varchar(100) DEFAULT NULL COMMENT 'Nombre del artículo',
  `areaAcademica` varchar(3) DEFAULT NULL COMMENT 'Clave del área académica al que pertenece',
  `asesor` varchar(10) DEFAULT NULL COMMENT 'Clave del asesor asignado',
  `coasesor` varchar(10) DEFAULT NULL COMMENT 'Clave del coasesor asignado',
  `nombreRevista` varchar(100) DEFAULT NULL COMMENT 'Nombre de la revista donde se publicará el artículo',
  `nombreIndice` varchar(100) DEFAULT NULL COMMENT 'Nombre del índice donde se publicará el artículo',
  `descripcion` text DEFAULT NULL COMMENT 'Descripción breve del artículo',
  `referencias` text DEFAULT NULL COMMENT 'Referencias bibliográficas',
  `cronograma` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Cronograma de actividades' CHECK (json_valid(`cronograma`))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pro_ens`
--

CREATE TABLE `pro_ens` (
  `claveEnsayo` varchar(10) NOT NULL COMMENT 'Identificador del ensayo',
  `claveTemporalProtocolo` varchar(15) NOT NULL COMMENT 'Clave temporal del protocolo',
  `clavePasante` int(7) UNSIGNED NOT NULL COMMENT 'Identificador del pasante',
  `fechaEntrega` date DEFAULT NULL COMMENT 'Fecha de entrega del protocolo del artículo',
  `fechaDictamen` date DEFAULT NULL COMMENT 'Fecha de presentación del examen profesional',
  `tema` varchar(100) DEFAULT NULL COMMENT 'Nombre del tema',
  `areaAcademica` varchar(3) DEFAULT NULL COMMENT 'Clave del área acdémica al que pertenece',
  `asesor` varchar(10) DEFAULT NULL COMMENT 'Clave del asesor asignado',
  `descripcion` text DEFAULT NULL COMMENT 'Descripción breve del artículo',
  `justificacion` text DEFAULT NULL COMMENT 'Descripción de la necesidad de abordar el tema',
  `referencias` text DEFAULT NULL COMMENT 'Referencias bibliográficas',
  `cronograma` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Cronograma de actividades' CHECK (json_valid(`cronograma`))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pro_mel`
--

CREATE TABLE `pro_mel` (
  `claveMel` varchar(10) NOT NULL COMMENT 'Identificador de memoria de experiencia laboral',
  `claveTemporalProtocolo` varchar(15) NOT NULL COMMENT 'Clave temporal del protocolo',
  `clavePasante` int(7) UNSIGNED NOT NULL COMMENT 'Identificador del pasante',
  `fechaEntrega` date DEFAULT NULL COMMENT 'Fecha de entrega del protocolo de memoria de experiencia laboral',
  `fechaDictamen` date DEFAULT NULL COMMENT 'Fecha de presentación del examen profesional',
  `titulo` text DEFAULT NULL COMMENT 'Objeto de estudio',
  `periodoInicio` date DEFAULT NULL COMMENT 'Fecha de inicio de la participación',
  `periodoTermino` date DEFAULT NULL COMMENT 'Fecha de termino de la participación',
  `asesor` varchar(10) DEFAULT NULL COMMENT 'Clave del asesor asignado',
  `areaAcademica` varchar(3) DEFAULT NULL COMMENT 'Clave del área académica al que pertenece',
  `descripcion` text DEFAULT NULL COMMENT 'Descripción breve de la memoria de experiencia laboral',
  `justificacion` text DEFAULT NULL COMMENT 'Descripción de la necesidad de abordar el tema',
  `objetivoGeneral` text DEFAULT NULL COMMENT 'Indicar que experiencia se pretende documentar',
  `referencias` text DEFAULT NULL COMMENT 'Referencias bibliográficas',
  `cronograma` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Cronograma de actividades' CHECK (json_valid(`cronograma`))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pro_rac`
--

CREATE TABLE `pro_rac` (
  `claveRac` varchar(10) NOT NULL COMMENT 'Identificador del reporte de aplicacion de conocimientos',
  `clavePasante` int(7) UNSIGNED NOT NULL COMMENT 'Identificador del pasante',
  `claveTemporalProtocolo` varchar(15) NOT NULL COMMENT 'Clave temporal del protocolo',
  `fechaEntrega` date DEFAULT NULL COMMENT 'Fecha de entrega del protocolo de reporte de aplicacion de conocimientos',
  `fechaDictamen` date DEFAULT NULL COMMENT 'Fecha de respuesta',
  `titulo` text DEFAULT NULL COMMENT 'Objeto de estudio',
  `asesor` varchar(10) DEFAULT NULL COMMENT 'Clave del asesor asignado',
  `areaAcademica` varchar(3) DEFAULT NULL COMMENT 'Clave del área académica al que pertenece',
  `planteamientoProblema` text DEFAULT NULL COMMENT 'Planteamiento del problema',
  `justificacion` text DEFAULT NULL COMMENT 'Descripción de la necesidad de abordar el tema',
  `objetivoGeneral` text DEFAULT NULL COMMENT 'Indicar que experiencia se pretende documentar',
  `metodologia` text DEFAULT NULL COMMENT 'Descripción de técnicas y métodos',
  `referencias` text DEFAULT NULL COMMENT 'Referencias bibliográficas',
  `cronograma` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Cronograma de actividades' CHECK (json_valid(`cronograma`))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pro_rin`
--

CREATE TABLE `pro_rin` (
  `claveRin` varchar(10) NOT NULL COMMENT 'Identificador de la residencia de investigación',
  `claveTemporalProtocolo` varchar(15) NOT NULL COMMENT 'Clave temporal del protocolo',
  `clavePasante` int(7) UNSIGNED NOT NULL COMMENT 'Identificador del pasante',
  `fechaEntrega` date DEFAULT NULL COMMENT 'Fecha de entrega del protocolo de autoempleo profesional',
  `fechaDictamen` date DEFAULT NULL COMMENT 'Fecha de respuesta',
  `periodoInicio` date DEFAULT NULL COMMENT 'Fecha de inicio de la participación',
  `periodoTermino` date DEFAULT NULL COMMENT 'Fecha de termino de la participación',
  `nombreProfInves` varchar(40) DEFAULT NULL COMMENT 'Nombre completo del profesor investigador responsable del proyecto',
  `asesor` varchar(10) DEFAULT NULL COMMENT 'Clave del asesor asignado',
  `coasesor` varchar(10) DEFAULT NULL COMMENT 'Clave del coasesor asignado',
  `areaAcademica` varchar(3) DEFAULT NULL COMMENT 'Clave del área académica al que pertenece',
  `tituloProy` text DEFAULT NULL COMMENT 'Titulo del proyecto de investigación',
  `claveRegProy` varchar(20) DEFAULT NULL COMMENT 'Clave de registro del proyecto',
  `descripcion` text DEFAULT NULL COMMENT 'Descripción general del proyecto de investigación',
  `tituloRep` text DEFAULT NULL COMMENT 'Titulo del reporte de residencia de investigación',
  `objetivoGeneral` text DEFAULT NULL COMMENT 'Indicar que experiencia se pretende documentar',
  `cronograma` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Cronograma de actividades' CHECK (json_valid(`cronograma`))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pro_tsn`
--

CREATE TABLE `pro_tsn` (
  `claveTsn` varchar(10) NOT NULL COMMENT 'Identificador de la tesina',
  `claveTemporalProtocolo` varchar(15) NOT NULL COMMENT 'Clave temporal del protocolo',
  `clavePasante` int(7) UNSIGNED NOT NULL COMMENT 'Identificador del pasante',
  `fechaEntrega` date DEFAULT NULL COMMENT 'Fecha de entrega del protocolo de autoempleo profesional',
  `fechaDictamen` date DEFAULT NULL COMMENT 'Fecha de respuesta',
  `titulo` text DEFAULT NULL COMMENT 'Objeto de estudio',
  `asesor` varchar(10) DEFAULT NULL COMMENT 'Clave del asesor asignado',
  `areaAcademica` varchar(3) DEFAULT NULL COMMENT 'Clave del área académica al que pertenece',
  `planteamientoInv` text DEFAULT NULL COMMENT 'Descripción general del tema que se quiere abordar',
  `justificacion` text DEFAULT NULL COMMENT 'Descripción de la necesidad de abordar el tema',
  `objetivoGeneral` text DEFAULT NULL COMMENT 'Indicar que experiencia se pretende documentar',
  `metodologia` text DEFAULT NULL COMMENT 'Técnicas y metodos de la investigación',
  `referencias` text DEFAULT NULL COMMENT 'Referencias bibliográficas',
  `cronograma` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Cronograma de actividades' CHECK (json_valid(`cronograma`))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pro_tss`
--

CREATE TABLE `pro_tss` (
  `claveTss` varchar(10) NOT NULL COMMENT 'Identificador de la tesis',
  `claveTemporalProtocolo` varchar(15) NOT NULL COMMENT 'Clave temporal del protocolo',
  `clavePasante` int(7) UNSIGNED NOT NULL COMMENT 'Identificador del pasante',
  `fechaEntrega` date DEFAULT NULL COMMENT 'Fecha de entrega del protocolo de autoempleo profesional',
  `fechaDictamen` date DEFAULT NULL COMMENT 'Fecha de respuesta',
  `titulo` text DEFAULT NULL COMMENT 'Objeto de estudio',
  `asesor` varchar(10) DEFAULT NULL COMMENT 'Clave del asesor asignado',
  `coasesor` varchar(10) DEFAULT NULL COMMENT 'Clave del coasesor asignado',
  `areaAcademica` varchar(3) DEFAULT NULL COMMENT 'Clave del área académica al que pertenece',
  `planteamiento` text DEFAULT NULL COMMENT 'Contexto o antecedente del problema',
  `justificacion` text DEFAULT NULL COMMENT 'Descripción de la necesidad de abordar el tema',
  `hipotesis` text DEFAULT NULL COMMENT 'Indicar lo que se está buscando o tratando de probar',
  `objetivoGeneral` text DEFAULT NULL COMMENT 'Indicar que experiencia se pretende documentar',
  `metodologia` text DEFAULT NULL COMMENT 'Técnicas y metodos de la investigación',
  `referencias` text DEFAULT NULL COMMENT 'Referencias bibliográficas',
  `cronograma` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'Cronograma de actividades' CHECK (json_valid(`cronograma`))
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `revisor`
--

CREATE TABLE `revisor` (
  `claveRevisor` varchar(10) NOT NULL COMMENT 'Identificador del revisor',
  `rfc` varchar(10) DEFAULT NULL COMMENT 'RFC del revisor',
  `nombre` varchar(20) DEFAULT NULL COMMENT 'Nombre del revisor',
  `apellidoPaterno` varchar(20) DEFAULT NULL COMMENT 'Apellido paterno del revisor',
  `apellidoMaterno` varchar(20) DEFAULT NULL COMMENT 'Apellido materno del revisor',
  `telefono` varchar(10) DEFAULT NULL COMMENT 'Número telefónico del revisor',
  `email` varchar(40) DEFAULT NULL COMMENT 'Correo electrónico del revisor',
  `contrasena` varchar(40) DEFAULT NULL COMMENT 'Contraseña de acceso al sistema'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `revisor`
--

INSERT INTO `revisor` (`claveRevisor`, `rfc`, `nombre`, `apellidoPaterno`, `apellidoMaterno`, `telefono`, `email`, `contrasena`) VALUES
('EOCL641023', 'EOCL641023', 'Jose Luis', 'Exposito', 'Castillo', '5214987463', 'jexpositoc@profesor.mx', '123456'),
('FANA830915', 'FANA830915', 'Areli', 'Fábila', 'Nuñez', '5214987464', 'afabilan@profesor.mx', '123456'),
('FOOC840827', 'FOOC840827', 'Carlos Roberto', 'Fonseca', 'Ortíz', '5214987465', 'cfonsecao@profesor.mx', '123456'),
('MASM640605', 'MASM640605', 'Moises', 'Martínez', 'Suarez', '5214987466', 'mmartinezs@profesor.mx', '123456'),
('PEMJ770927', 'PEMJ770927', 'Judith', 'Pérez', 'Morales', '5214987467', 'jperezm@profesor.mx', '123456'),
('VEMI640318', 'VEMI640318', 'Ignacio', 'Vertiz', 'Mañon', '5214987468', 'ivertizm@profesor.mx', '123456');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `revisor_area`
--

CREATE TABLE `revisor_area` (
  `claveRevisor` varchar(10) DEFAULT NULL COMMENT 'Clave del revisor asignado',
  `claveAreaAcademica` varchar(3) DEFAULT NULL COMMENT 'Clave del área académica'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_trabajo`
--

CREATE TABLE `tipo_trabajo` (
  `claveTipoTrabajo` varchar(3) NOT NULL COMMENT 'Clave que identifica el tipo de trabajo',
  `descripcion` varchar(100) DEFAULT NULL COMMENT 'Descripción del tipo de trabajo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_trabajo`
--

INSERT INTO `tipo_trabajo` (`claveTipoTrabajo`, `descripcion`) VALUES
('AEM', 'Reporte de Autoempleo Profesional'),
('ART', 'Artículo Especializado para Publicar en Revista Indizada'),
('ENS', 'Ensayo'),
('MEL', 'Memoria de Experiencia Laboral'),
('RAC', 'Reporte de Aplicación de Conocimientos'),
('RIN', 'Reporte de Residencia de Investigación'),
('TSN', 'Tesina'),
('TSS', 'Tesis');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `area_academica`
--
ALTER TABLE `area_academica`
  ADD PRIMARY KEY (`claveArea`),
  ADD KEY `licenciatura` (`licenciatura`);

--
-- Indices de la tabla `asesor`
--
ALTER TABLE `asesor`
  ADD PRIMARY KEY (`claveAsesor`),
  ADD KEY `rfc` (`rfc`);

--
-- Indices de la tabla `comite_evaluador`
--
ALTER TABLE `comite_evaluador`
  ADD PRIMARY KEY (`claveComite`),
  ADD KEY `revisor1` (`revisor1`),
  ADD KEY `revisor2` (`revisor2`),
  ADD KEY `revisor3` (`revisor3`);

--
-- Indices de la tabla `coordinador`
--
ALTER TABLE `coordinador`
  ADD PRIMARY KEY (`claveCoordinador`),
  ADD KEY `rfc` (`rfc`),
  ADD KEY `licenciatura` (`licenciatura`);

--
-- Indices de la tabla `depto_titulacion`
--
ALTER TABLE `depto_titulacion`
  ADD PRIMARY KEY (`claveDepTitu`),
  ADD KEY `rfc` (`rfc`);

--
-- Indices de la tabla `escuela`
--
ALTER TABLE `escuela`
  ADD PRIMARY KEY (`claveEscuela`);

--
-- Indices de la tabla `licenciatura`
--
ALTER TABLE `licenciatura`
  ADD PRIMARY KEY (`claveLicenciatura`);

--
-- Indices de la tabla `pasante`
--
ALTER TABLE `pasante`
  ADD PRIMARY KEY (`clavePasante`),
  ADD KEY `escuela` (`escuela`),
  ADD KEY `licenciatura` (`licenciatura`);

--
-- Indices de la tabla `pasante_protocolo`
--
ALTER TABLE `pasante_protocolo`
  ADD KEY `claveProtocoloTemporal` (`claveProtocoloTemporal`),
  ADD KEY `clavePasante` (`clavePasante`),
  ADD KEY `tipoTrabajo` (`tipoTrabajo`);

--
-- Indices de la tabla `pro_aem`
--
ALTER TABLE `pro_aem`
  ADD UNIQUE KEY `claveAem` (`claveAem`),
  ADD KEY `claveTemporalProtocolo` (`claveTemporalProtocolo`),
  ADD KEY `clavePasante` (`clavePasante`),
  ADD KEY `areaAcademica` (`areaAcademica`),
  ADD KEY `asesor` (`asesor`);

--
-- Indices de la tabla `pro_art`
--
ALTER TABLE `pro_art`
  ADD PRIMARY KEY (`claveArticulo`),
  ADD KEY `claveTemporalProtocolo` (`claveTemporalProtocolo`),
  ADD KEY `clavePasante` (`clavePasante`),
  ADD KEY `areaAcademica` (`areaAcademica`),
  ADD KEY `asesor` (`asesor`),
  ADD KEY `coasesor` (`coasesor`);

--
-- Indices de la tabla `pro_ens`
--
ALTER TABLE `pro_ens`
  ADD PRIMARY KEY (`claveEnsayo`),
  ADD KEY `claveTemporalProtocolo` (`claveTemporalProtocolo`),
  ADD KEY `clavePasante` (`clavePasante`),
  ADD KEY `areaAcademica` (`areaAcademica`),
  ADD KEY `asesor` (`asesor`);

--
-- Indices de la tabla `pro_mel`
--
ALTER TABLE `pro_mel`
  ADD PRIMARY KEY (`claveMel`),
  ADD KEY `claveTemporalProtocolo` (`claveTemporalProtocolo`),
  ADD KEY `clavePasante` (`clavePasante`),
  ADD KEY `areaAcademica` (`areaAcademica`),
  ADD KEY `asesor` (`asesor`);

--
-- Indices de la tabla `pro_rac`
--
ALTER TABLE `pro_rac`
  ADD PRIMARY KEY (`claveRac`),
  ADD KEY `claveTemporalProtocolo` (`claveTemporalProtocolo`),
  ADD KEY `clavePasante` (`clavePasante`),
  ADD KEY `areaAcademica` (`areaAcademica`),
  ADD KEY `asesor` (`asesor`);

--
-- Indices de la tabla `pro_rin`
--
ALTER TABLE `pro_rin`
  ADD PRIMARY KEY (`claveRin`),
  ADD KEY `claveTemporalProtocolo` (`claveTemporalProtocolo`),
  ADD KEY `clavePasante` (`clavePasante`),
  ADD KEY `areaAcademica` (`areaAcademica`),
  ADD KEY `asesor` (`asesor`),
  ADD KEY `coasesor` (`coasesor`);

--
-- Indices de la tabla `pro_tsn`
--
ALTER TABLE `pro_tsn`
  ADD PRIMARY KEY (`claveTsn`),
  ADD KEY `claveTemporalProtocolo` (`claveTemporalProtocolo`),
  ADD KEY `clavePasante` (`clavePasante`),
  ADD KEY `areaAcademica` (`areaAcademica`),
  ADD KEY `asesor` (`asesor`);

--
-- Indices de la tabla `pro_tss`
--
ALTER TABLE `pro_tss`
  ADD PRIMARY KEY (`claveTss`),
  ADD KEY `claveTemporalProtocolo` (`claveTemporalProtocolo`),
  ADD KEY `clavePasante` (`clavePasante`),
  ADD KEY `areaAcademica` (`areaAcademica`),
  ADD KEY `asesor` (`asesor`),
  ADD KEY `coasesor` (`coasesor`);

--
-- Indices de la tabla `revisor`
--
ALTER TABLE `revisor`
  ADD PRIMARY KEY (`claveRevisor`),
  ADD KEY `rfc` (`rfc`);

--
-- Indices de la tabla `revisor_area`
--
ALTER TABLE `revisor_area`
  ADD KEY `claveRevisor` (`claveRevisor`),
  ADD KEY `claveAreaAcademica` (`claveAreaAcademica`);

--
-- Indices de la tabla `tipo_trabajo`
--
ALTER TABLE `tipo_trabajo`
  ADD PRIMARY KEY (`claveTipoTrabajo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `comite_evaluador`
--
ALTER TABLE `comite_evaluador`
  MODIFY `claveComite` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `area_academica`
--
ALTER TABLE `area_academica`
  ADD CONSTRAINT `area_academica_ibfk_1` FOREIGN KEY (`licenciatura`) REFERENCES `licenciatura` (`claveLicenciatura`);

--
-- Filtros para la tabla `comite_evaluador`
--
ALTER TABLE `comite_evaluador`
  ADD CONSTRAINT `comite_evaluador_ibfk_1` FOREIGN KEY (`revisor1`) REFERENCES `revisor` (`claveRevisor`),
  ADD CONSTRAINT `comite_evaluador_ibfk_2` FOREIGN KEY (`revisor2`) REFERENCES `revisor` (`claveRevisor`),
  ADD CONSTRAINT `comite_evaluador_ibfk_3` FOREIGN KEY (`revisor3`) REFERENCES `revisor` (`claveRevisor`);

--
-- Filtros para la tabla `coordinador`
--
ALTER TABLE `coordinador`
  ADD CONSTRAINT `coordinador_ibfk_1` FOREIGN KEY (`licenciatura`) REFERENCES `licenciatura` (`claveLicenciatura`);

--
-- Filtros para la tabla `pasante`
--
ALTER TABLE `pasante`
  ADD CONSTRAINT `pasante_ibfk_1` FOREIGN KEY (`escuela`) REFERENCES `escuela` (`claveEscuela`),
  ADD CONSTRAINT `pasante_ibfk_2` FOREIGN KEY (`licenciatura`) REFERENCES `licenciatura` (`claveLicenciatura`);

--
-- Filtros para la tabla `pasante_protocolo`
--
ALTER TABLE `pasante_protocolo`
  ADD CONSTRAINT `pasante_protocolo_ibfk_1` FOREIGN KEY (`clavePasante`) REFERENCES `pasante` (`clavePasante`),
  ADD CONSTRAINT `pasante_protocolo_ibfk_2` FOREIGN KEY (`tipoTrabajo`) REFERENCES `tipo_trabajo` (`claveTipoTrabajo`);

--
-- Filtros para la tabla `pro_aem`
--
ALTER TABLE `pro_aem`
  ADD CONSTRAINT `pro_aem_ibfk_1` FOREIGN KEY (`clavePasante`) REFERENCES `pasante` (`clavePasante`),
  ADD CONSTRAINT `pro_aem_ibfk_2` FOREIGN KEY (`areaAcademica`) REFERENCES `area_academica` (`claveArea`),
  ADD CONSTRAINT `pro_aem_ibfk_3` FOREIGN KEY (`asesor`) REFERENCES `asesor` (`claveAsesor`);

--
-- Filtros para la tabla `pro_art`
--
ALTER TABLE `pro_art`
  ADD CONSTRAINT `pro_art_ibfk_1` FOREIGN KEY (`clavePasante`) REFERENCES `pasante` (`clavePasante`),
  ADD CONSTRAINT `pro_art_ibfk_2` FOREIGN KEY (`areaAcademica`) REFERENCES `area_academica` (`claveArea`),
  ADD CONSTRAINT `pro_art_ibfk_3` FOREIGN KEY (`asesor`) REFERENCES `asesor` (`claveAsesor`),
  ADD CONSTRAINT `pro_art_ibfk_4` FOREIGN KEY (`coasesor`) REFERENCES `asesor` (`claveAsesor`);

--
-- Filtros para la tabla `pro_ens`
--
ALTER TABLE `pro_ens`
  ADD CONSTRAINT `pro_ens_ibfk_1` FOREIGN KEY (`clavePasante`) REFERENCES `pasante` (`clavePasante`),
  ADD CONSTRAINT `pro_ens_ibfk_2` FOREIGN KEY (`areaAcademica`) REFERENCES `area_academica` (`claveArea`),
  ADD CONSTRAINT `pro_ens_ibfk_3` FOREIGN KEY (`asesor`) REFERENCES `asesor` (`claveAsesor`);

--
-- Filtros para la tabla `pro_mel`
--
ALTER TABLE `pro_mel`
  ADD CONSTRAINT `pro_mel_ibfk_1` FOREIGN KEY (`clavePasante`) REFERENCES `pasante` (`clavePasante`),
  ADD CONSTRAINT `pro_mel_ibfk_2` FOREIGN KEY (`areaAcademica`) REFERENCES `area_academica` (`claveArea`),
  ADD CONSTRAINT `pro_mel_ibfk_3` FOREIGN KEY (`asesor`) REFERENCES `asesor` (`claveAsesor`);

--
-- Filtros para la tabla `pro_rac`
--
ALTER TABLE `pro_rac`
  ADD CONSTRAINT `pro_rac_ibfk_1` FOREIGN KEY (`clavePasante`) REFERENCES `pasante` (`clavePasante`),
  ADD CONSTRAINT `pro_rac_ibfk_2` FOREIGN KEY (`areaAcademica`) REFERENCES `area_academica` (`claveArea`),
  ADD CONSTRAINT `pro_rac_ibfk_3` FOREIGN KEY (`asesor`) REFERENCES `asesor` (`claveAsesor`);

--
-- Filtros para la tabla `pro_rin`
--
ALTER TABLE `pro_rin`
  ADD CONSTRAINT `pro_rin_ibfk_1` FOREIGN KEY (`clavePasante`) REFERENCES `pasante` (`clavePasante`),
  ADD CONSTRAINT `pro_rin_ibfk_2` FOREIGN KEY (`areaAcademica`) REFERENCES `area_academica` (`claveArea`),
  ADD CONSTRAINT `pro_rin_ibfk_3` FOREIGN KEY (`asesor`) REFERENCES `asesor` (`claveAsesor`),
  ADD CONSTRAINT `pro_rin_ibfk_4` FOREIGN KEY (`coasesor`) REFERENCES `asesor` (`claveAsesor`);

--
-- Filtros para la tabla `pro_tsn`
--
ALTER TABLE `pro_tsn`
  ADD CONSTRAINT `pro_tsn_ibfk_1` FOREIGN KEY (`clavePasante`) REFERENCES `pasante` (`clavePasante`),
  ADD CONSTRAINT `pro_tsn_ibfk_2` FOREIGN KEY (`areaAcademica`) REFERENCES `area_academica` (`claveArea`),
  ADD CONSTRAINT `pro_tsn_ibfk_3` FOREIGN KEY (`asesor`) REFERENCES `asesor` (`claveAsesor`);

--
-- Filtros para la tabla `pro_tss`
--
ALTER TABLE `pro_tss`
  ADD CONSTRAINT `pro_tss_ibfk_1` FOREIGN KEY (`clavePasante`) REFERENCES `pasante` (`clavePasante`),
  ADD CONSTRAINT `pro_tss_ibfk_2` FOREIGN KEY (`areaAcademica`) REFERENCES `area_academica` (`claveArea`),
  ADD CONSTRAINT `pro_tss_ibfk_3` FOREIGN KEY (`asesor`) REFERENCES `asesor` (`claveAsesor`),
  ADD CONSTRAINT `pro_tss_ibfk_4` FOREIGN KEY (`coasesor`) REFERENCES `asesor` (`claveAsesor`);

--
-- Filtros para la tabla `revisor_area`
--
ALTER TABLE `revisor_area`
  ADD CONSTRAINT `revisor_area_ibfk_1` FOREIGN KEY (`claveRevisor`) REFERENCES `revisor` (`claveRevisor`),
  ADD CONSTRAINT `revisor_area_ibfk_2` FOREIGN KEY (`claveAreaAcademica`) REFERENCES `area_academica` (`claveArea`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

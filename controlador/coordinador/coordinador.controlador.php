<?php
	/**
	 * 
	 */
	class ControladorCoordinador{
		
		/**
		 * [ingresoCoordinadorControlador revisa los datos de ingreso del coordinador]
		 * @param  [array] $datos [los datos de ingreso al sistema del coordinador]
		 * @return [type]        [description]
		 */
		public function ingresoCoordinadorControlador($coordinador, $contrasena){
			$respuesta = ModeloCoordinador::ingresoCoordinadorModelo('coordinador', $coordinador);
			if ($respuesta['claveCoordinador'] == $coordinador && $contrasena == $respuesta['contrasena']){
					session_start();
					$_SESSION['usuario'] = $respuesta['claveCoordinador'];
					$_SESSION['esCoordinador'] = true;
					$_SESSION['licenciatura'] = $respuesta['licenciatura'];
					return 1;
			}else
				return -1;
		}

		/**
		 * [obtenerNombreCoordinadorControlador obtiene el nombre completo del coordinador]
		 * @param  [text] $clave_coordinador [clave del coordinador para la base de datos]
		 * @return [objeto] $respuesta [regresa nombre completo del coordinador o error, si lo hubiera]
		 */
		public function obtenerNombreCoordinadorControlador($clave_coordinador){
			$respuesta = ModeloCoordinador::obtenerNombreCoordinadorModelo('coordinador', $clave_coordinador);
			return $respuesta;
		}

		/**
		 * [numeroPasantesPendientesControlador obtiene el número de pasantes que se han registrado pero no han sido aprobados]
		 * @param  [text] $licenciatura [licenciatura del coordinador]
		 * @return [array]               []
		 */
		public function numeroPasantesPendientesControlador($licenciatura){
			$respuesta = ModeloCoordinador::numeroPasantesPendientesModelo('pasante', $licenciatura);
			return $respuesta;
		}

		/**
		 * [listaPasantesPendientesControlador muestra la lista de pasantes pendientes en registros de 10 en 10]
		 * @param  [text]  $licenciatura [licenciatura del coordinador]
		 * @param  [text]  $no_pag       [número de la página correspondiente]
		 * @return [array]               [description]
		 */
		public function listaPasantesPendientesControlador($licenciatura, $no_pag){
			$numero_registros = 10;
			($no_pag == 0 || $no_pag == 1)?$inicio = 0:$inicio = ($no_pag-1)*10;
			$respuesta = ModeloCoordinador::listaPasantesPendientesModelo('pasante', $licenciatura, $inicio);
			return $respuesta;
		}

		/**
		 * [aceptarPasantePendienteControlador actualiza el estado de un pasante en la base de datos a aceptado (1)]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @return [array]                [description]
		 */
		public function aceptarPasantePendienteControlador($clave_pasante){
			$respuesta = ModeloCoordinador::aceptarPasantePendienteModelo("pasante", $clave_pasante);
			//Envíe un correo dando la bienvenida
			return $respuesta;
		}

		/**
		 * [rechazarPasanteControlador actualiza el estado de un pasante en la base de datos a aceptado (2)]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @return [array]                [description]
		 */
		public function rechazarPasantePendienteControlador($clave_pasante){
			$respuesta = ModeloCoordinador::rechazarPasantePendienteModelo("pasante", $clave_pasante);
			//Envíe un correo con datos de contacto
			return $respuesta;
		}








	}
?>
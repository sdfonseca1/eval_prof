<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 13 AGOSTO 2019
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo intermediario entre las vistas y el modelo.
*  ANOTACIONES: por favor, cuando edites este archivo, recuerda seguir las reglas de codificación establecidas.
*/

/*REQUERIMIENTOS DE ARCHIVOS*/

	/**
	 * Clase controlador
	 */
	class Controlador{

		/*VARIABLES Y CONSTANTES*/

		/**
		 * [pagina encargada de cargar el archivo plantilla.php]
		 */
		public function pagina(){
			include('vista/plantilla.php');
		}

		/**
		 * [enlacesPaginasControlador encargada de recoger el valor del accion en la barra de
		 * direcciones.]
		 */
		public function enlacesPaginasControlador(){
			isset($_GET['accion']) ? $enlaces = $_GET['accion'] : $enlaces = 'index=accion=inicio';
			$respuesta = Paginas::enlacesPaginasModelo($enlaces);
			include($respuesta);	
		}

		/**
		 * [obtenerListaAsesoresControlador obtiene lista de asesores registrados en el sistema]
		 * @return [type] [description]
		 */
		public function obtenerListaAsesoresControlador(){
			$respuesta = Modelo::obtenerListaAsesoresModelo('asesor');
			return $respuesta;
		}	

		/**
		 * [obtenerAreaAcademicaControlador obtiene la lista de las áreas académicas registradas en el sistema]
		 * @param  [text] $licenciatura [la licenciatura de la cúal se requiere las áreas académicas]
		 * @return [type]               [description]
		 */
		public function obtenerAreaAcademicaControlador($licenciatura){
			$respuesta = Modelo::obtenerAreaAcademicaModelo('area_academica', $licenciatura);
			return $respuesta;
		}

		

		public function obtenerEscuelasControlador(){
			/*UTILIDAD: muestra las escuelas registradas al usuario.
			  PRECONDICION: deben existir registros en la base de datos.
			  POSTCONDICIÓN:
			*/
			mb_internal_encoding('UTF-8');
			$respuesta = Modelo::obtenerEscuelasModelo('escuela');
			foreach ($respuesta as $clave=>$valor){
   				echo "<option value='$valor[0]'>". $valor[0] . " - " . $valor[1] . "</option>";
   			}
		}



		/**
		 * [obtenerTiposDeTrabajoControlador obtiene los tipos de trabajo de modlidad escrita]
		 * @return [array] [description]
		 */
		public function obtenerTiposDeTrabajoControlador(){
			$respuesta = Modelo::obtenerTiposDeTrabajoModelo('tipo_trabajo');
			return $respuesta;
		}

		/**
		 * [eliminarBorradorProtocoloControlador elimina los datos de un protocolo AEM]
		 * @param  [type] $pasante [clave del pasante]
		 * @param  [type] $trabajo [clave del tipo de trabajo]
		 * @return [boolean]                [resultado de la sentencia]
		 */
		public function eliminarBorradorProtocoloControlador($pasante, $trabajo){
			$tipo_trabajo = strtolower($trabajo);
			$respuesta = Modelo::eliminarBorradorProtocoloModelo("pro_".$tipo_trabajo, $pasante);
			return $respuesta;
		}

		



		function latin1ToUTF8($datos){
	    	if (is_string($datos)) {
	        	return utf8_encode($datos);
			}elseif (is_array($datos)) {
	        	$ret = [];
	        	foreach ($datos as $i => $d) $ret[ $i ] = self::latin1ToUTF8($d);
				return $ret;
	    	}elseif (is_object($datos)) {
	        	foreach ($datos as $i => $d) $datos->$i = self::latin1ToUTF8($d);
				return $datos;
	      	}else {
	        	return $datos;
	      	}
    	}
	}


?>




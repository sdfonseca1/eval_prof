<?php
	/**
	 * 
	 */
	class ControladorTitulacion{
		
		/**
		 * [ingresoTitulacionControlador revisa los datos de ingreso del depto. titulacion]
		 * @param  [array] $datos [los datos de ingreso al sistema del depto. titulacion]
		 * @return [type]        [description]
		 */
		public function ingresoTitulacionControlador($titulacion, $contrasena){
			$respuesta = ModeloTitulacion::ingresoTitulacionModelo('depto_titulacion', $titulacion);
			if ($titulacion == $respuesta['claveDepTitu'] && $contrasena == $respuesta['contrasena']){
					session_start();
					$_SESSION['usuario'] = $respuesta['claveDepTitu'];
					$_SESSION['esTitulacion'] = true;
					return 1;
			}else
				return -1;
		}

		/**
		 * [obtenerNombreTitulacionControlador obtiene el nombre completo del depto. titulacion]
		 * @param  [text] $clave_titulacion [clave del depto. titulacion para la base de datos]
		 * @return [objeto] $respuesta [regresa nombre completo del depto. titulacion o error, si lo hubiera]
		 */
		public function obtenerNombreTitulacionControlador($clave_titulacion){
			$respuesta = ModeloTitulacion::obtenerNombreTitulacionModelo('depto_titulacion', $clave_titulacion);
			// var_dump($respuesta);
			return $respuesta;
		}

		/**
		 * [numeroSolicitudesPendientesControlador obtiene el número de solicitudes que se han registrado pero no han sido asignadas]
		 * @return [array]               []
		 */
		public function numeroSolicitudesPendientesControlador(){
			$respuesta = ModeloTitulacion::numeroSolicitudesPendientesModelo('pasante_protocolo');
			return $respuesta;
		}

		/**
		 * [listaSolicitudesPendientesControlador muestra las solicitudes que no tienen clave del protocolo]
		 * @param  [int] $no_pag [número de página donde empezaran los registros]
		 * @return [type]         [description]
		 */
		public function listaSolicitudesPendientesControlador($no_pag){
			$numero_registros = 10;
			($no_pag == 0 || $no_pag == 1)?$inicio = 0:$inicio = ($no_pag-1)*10;
			$respuesta = ModeloTitulacion::listaSolicitudesPendientesModelo('pasante_protocolo',$inicio);
			return $respuesta;
		}

		public function obtenerSolicitudControlador($clave_temporal, $tipo_trabajo){
			$respuesta = ModeloTitulacion::obtenerSolicitudProtocoloModelo('pro_'.$tipo_trabajo, $clave_temporal);
			// var_dump($respuesta);
			return $respuesta;
		}





	}
?>
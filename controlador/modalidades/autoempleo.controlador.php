<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 5 de ABRIL 2020
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo intermediario entre las vistas y el modelo.
*  ANOTACIONES:
*/

/*REQUERIMIENTOS DE ARCHIVOS*/

	/**
	 * Clase ControladorArtículo
	 */
	class ControladorAutoempleo{

		/**
		 * [guardarProtocoloAEMControlador guarda por primera vez los datos de un protocolo AEM]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [contiene los datos del protocolo AEM]
		 * @return [type]          [resultado de la sentencia]
		 */
		public function guardarProtocoloAEMControlador($pasante, $datos){
			$nuevos_datos = array(
				'titulo' => $datos['titulo'],
				'fecha_ini' => $datos['fecha_ini'] == '' ? null : $datos['fecha_ini'],
				'fecha_fin' => $datos['fecha_fin'] == '' ? null : $datos['fecha_fin'],
				'asesor' => $datos['asesor'],
				'area' => $datos['area'],
				'planteamiento' => $datos['planteamiento'],
				'justificacion' => $datos['justificacion'],
				'asesor' => $datos['asesor'],
				'objetivo' => $datos['objetivo'],
				'referencias' => $datos['referencias'],
				'cronograma' => $datos['cronograma']);
			$respuesta = ModeloAutoempleo::guardarProtocoloAEMModelo("pro_aem", $pasante, $nuevos_datos);
			return $respuesta;
		}

		/**
		 * [obtenerProtocoloAEMControlador obtiene los datos del protocolo AEM]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @return [array]               [resultado de la sentencia]
		 */
		public function obtenerProtocoloAEMControlador($clave_pasante){
			$respuesta = ModeloAutoempleo::obtenerProtocoloAEMModelo("pasante_protocolo", $clave_pasante, 'aem');
			return $respuesta;
		}

		/**
		 * [actualizarProtocoloAEMControlador actualiza los valores del protocolo AEM]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @param  [array] $datos         [contiene los datos del protocolo AEM]
		 * @return [array]                [resultado de la sentencia]
		 */
		public function actualizarProtocoloAEMControlador($clave_pasante, $datos){
			if ($datos['fecha_ini'] == '') 
				$datos['fecha_ini'] = null;
			if ($datos['fecha_fin'] == '')
				$datos['fecha_fin'] = null;
			$respuesta = ModeloAutoempleo::actualizarProtocoloAEMModelo("pro_aem", $clave_pasante, $datos);
			return $respuesta;
		}

		/**
		 * [registrarProtocoloAEMControlador genera una solicitud de revisión del protocolo]
		 * @param  [type] $clave_pasante [clave del pasante]
		 * @param  [type] $trabajo       [clave del tipo de trabajo]
		 * @return [type]                [description]
		 */
		public function registrarProtocoloAEMControlador($clave_pasante, $tipo_trabajo, $datos){
			$guardar = ControladorAutoempleo::guardarProtocoloAEMControlador($clave_pasante, $datos);
			$resultado;
			if ($guardar) {
				$respuesta = ModeloAutoempleo::registrarProtocoloAEMModelo("pasante_protocolo", $clave_pasante, $tipo_trabajo);
				return $respuesta;
			}else
				return $guardar;	
		}
	}
?>
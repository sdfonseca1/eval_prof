<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 5 de ABRIL 2020
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo intermediario entre las vistas y el modelo.
*  ANOTACIONES:
*/

/*REQUERIMIENTOS DE ARCHIVOS*/

	/**
	 * Clase ControladorEnsayo
	 */
	class ControladorEnsayo{

		/**
		 * [guardarProtocoloENSControlador guarda por primera vez los datos de un protocolo ENS]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [contiene los datos del protocolo ENS]
		 * @return [type]          [resultado de la sentencia]
		 */
		public function guardarProtocoloENSControlador($pasante, $datos){
			$nuevos_datos = array(
				'tema' => $datos['tema'],
				'area' => $datos['area'],
				'asesor' => $datos['asesor'],
				'descripcion' => $datos['descripcion'],
				'justificacion' => $datos['justificacion'],
				'referencias' => $datos['referencias'],
				'cronograma' => $datos['cronograma']);
			$respuesta = ModeloEnsayo::guardarProtocoloENSModelo("pro_ens", $pasante, $nuevos_datos);
			return $respuesta;
		}

		/**
		 * [obtenerProtocoloENSControlador obtiene los datos del protocolo ENS]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @return [array]               [resultado de la sentencia]
		 */
		public function obtenerProtocoloENSControlador($clave_pasante){
			$respuesta = ModeloEnsayo::obtenerProtocoloENSModelo("pasante_protocolo", $clave_pasante, 'ens');
			return $respuesta;
		}

		/**
		 * [actualizarProtocoloENSControlador actualiza los valores del protocolo ENS]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @param  [array] $datos         [contiene los datos del protocolo ENS]
		 * @return [array]                [resultado de la sentencia]
		 */
		public function actualizarProtocoloENSControlador($clave_pasante, $datos){
			$respuesta = ModeloEnsayo::actualizarProtocoloENSModelo("pro_ens", $clave_pasante, $datos);
			return $respuesta;
		}

		/**
		 * [cambioSolicitudProtocoloENSControlador description]
		 * @param  [type] $clave_pasante [description]
		 * @param  [type] $trabajo       [description]
		 * @return [type]                [description]
		 */
		public function registrarProtocoloENSControlador($clave_pasante, $trabajo, $datos){
			$guardar = ControladorEnsayo::guardarProtocoloENSControlador($clave_pasante, $datos);
			if ($guardar) {
				$respuesta = ModeloEnsayo::registrarProtocoloENSModelo("pasante_protocolo", $clave_pasante, $tipo_trabajo);
				return $respuesta;
			}else
				return $guardar;
		}
	}
?>
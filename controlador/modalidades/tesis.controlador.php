<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 7 de ABRIL 2020
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo intermediario entre las vistas y el modelo.
*  ANOTACIONES:
*/

/*REQUERIMIENTOS DE ARCHIVOS*/

	/**
	 * Clase ControladorTesis
	 */
	class ControladorTesis{

		/**
		 * [guardarProtocoloTSSControlador guarda por primera vez los datos de un protocolo TSS]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [contiene los datos del protocolo TSS]
		 * @return [type]          [resultado de la sentencia]
		 */
		public function guardarProtocoloTSSControlador($pasante, $datos){
			$nuevos_datos = array(
				'titulo' => $datos['titulo'],
				'asesor' => $datos['asesor'],
				'coasesor' => $datos['coasesor'],
				'area' => $datos['area'],
				'planteamiento' => $datos['planteamiento'],
				'justificacion' => $datos['justificacion'],
				'hipotesis' => $datos['hipotesis'],
				'objetivo' => $datos['objetivo'],
				'metodologia' => $datos['metodologia'],
				'referencias' => $datos['referencias'],
				'cronograma' => $datos['cronograma']);
			$respuesta = ModeloTesis::guardarProtocoloTSSModelo("pro_tss", $pasante, $nuevos_datos);
			return $respuesta;
		}

		/**
		 * [obtenerProtocoloTSSControlador obtiene los datos del protocolo TSS]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @return [array]               [resultado de la sentencia]
		 */
		public function obtenerProtocoloTSSControlador($clave_pasante){
			$respuesta = ModeloTesis::obtenerProtocoloTSSModelo("pasante_protocolo", $clave_pasante, 'tss');
			return $respuesta;
		}

		/**
		 * [actualizarProtocoloTSSControlador actualiza los valores del protocolo TSS]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @param  [array] $datos         [contiene los datos del protocolo TSS]
		 * @return [array]                [resultado de la sentencia]
		 */
		public function actualizarProtocoloTSSControlador($clave_pasante, $datos){
			if ($datos['fecha_ini'] == '') 
				$datos['fecha_ini'] = null;
			if ($datos['fecha_fin'] == '')
				$datos['fecha_fin'] = null;
			$respuesta = ModeloTesis::actualizarProtocoloTSSModelo("pro_tss", $clave_pasante, $datos);
			return $respuesta;
		}

		/**
		 * [cambioSolicitudProtocoloTSSControlador genera una solicitud de revisión del protocolo]
		 * @param  [type] $clave_pasante [clave del pasante]
		 * @param  [type] $trabajo       [clave del tipo de trabajo]
		 * @return [type]                [description]
		 */
		public function cambioSolicitudProtocoloTSSControlador($clave_pasante, $trabajo){
			$respuesta = ModeloTesis::cambioSolicitudProtocoloTSSModelo("pasante_protocolo", $clave_pasante, $trabajo);
			return $respuesta;
		}
	}
?>
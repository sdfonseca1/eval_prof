<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 7 de ABRIL 2020
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo intermediario entre las vistas y el modelo.
*  ANOTACIONES:
*/

/*REQUERIMIENTOS DE ARCHIVOS*/

	/**
	 * Clase ControladorTesina
	 */
	class ControladorTesina{

		/**
		 * [guardarProtocoloTSNControlador guarda por primera vez los datos de un protocolo TSN]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [contiene los datos del protocolo TSN]
		 * @return [type]          [resultado de la sentencia]
		 */
		public function guardarProtocoloTSNControlador($pasante, $datos){
			$nuevos_datos = array(
				'titulo' => $datos['titulo'],
				'asesor' => $datos['asesor'],
				'area' => $datos['area'],
				'planteamiento' => $datos['planteamiento'],
				'justificacion' => $datos['justificacion'],
				'objetivo' => $datos['objetivo'],
				'metodologia' => $datos['metodologia'],
				'referencias' => $datos['referencias'],
				'cronograma' => $datos['cronograma']);
			$respuesta = ModeloTesina::guardarProtocoloTSNModelo("pro_tsn", $pasante, $nuevos_datos);
			return $respuesta;
		}

		/**
		 * [obtenerProtocoloTSNControlador obtiene los datos del protocolo TSN]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @return [array]               [resultado de la sentencia]
		 */
		public function obtenerProtocoloTSNControlador($clave_pasante){
			$respuesta = ModeloTesina::obtenerProtocoloTSNModelo("pasante_protocolo", $clave_pasante, 'tsn');
			return $respuesta;
		}

		/**
		 * [actualizarProtocoloTSNControlador actualiza los valores del protocolo TSN]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @param  [array] $datos         [contiene los datos del protocolo TSN]
		 * @return [array]                [resultado de la sentencia]
		 */
		public function actualizarProtocoloTSNControlador($clave_pasante, $datos){
			if ($datos['fecha_ini'] == '') 
				$datos['fecha_ini'] = null;
			if ($datos['fecha_fin'] == '')
				$datos['fecha_fin'] = null;
			$respuesta = ModeloTesina::actualizarProtocoloTSNModelo("pro_tsn", $clave_pasante, $datos);
			return $respuesta;
		}

		/**
		 * [cambioSolicitudProtocoloTSNControlador genera una solicitud de revisión del protocolo]
		 * @param  [type] $clave_pasante [clave del pasante]
		 * @param  [type] $trabajo       [clave del tipo de trabajo]
		 * @return [type]                [description]
		 */
		public function cambioSolicitudProtocoloTSNControlador($clave_pasante, $trabajo){
			$respuesta = ModeloTesina::cambioSolicitudProtocoloTSNModelo("pasante_protocolo", $clave_pasante, $trabajo);
			return $respuesta;
		}
	}
?>
<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 6 de ABRIL 2020
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo intermediario entre las vistas y el modelo.
*  ANOTACIONES:
*/

/*REQUERIMIENTOS DE ARCHIVOS*/

	/**
	 * Clase ControladorResidencia
	 */
	class ControladorResidencia{

		/**
		 * [guardarProtocoloRINControlador guarda por primera vez los datos de un protocolo RIN]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [contiene los datos del protocolo RIN]
		 * @return [type]          [resultado de la sentencia]
		 */
		public function guardarProtocoloRINControlador($pasante, $datos){
			$nuevos_datos = array(
				'fecha_ini' => $datos['fecha_ini'] == '' ? null : $datos['fecha_ini'],
				'fecha_fin' => $datos['fecha_fin'] == '' ? null : $datos['fecha_fin'],
				'investigador' => $datos['investigador'],
				'asesor' => $datos['asesor'],
				'coasesor' => $datos['coasesor'],
				'area' => $datos['area'],
				'titulo' => $datos['titulo'],
				'cve_proy' => $datos['cve_proy'],
				'descripcion' => $datos['descripcion'],
				'reporte' => $datos['reporte'],
				'objetivo' => $datos['objetivo'],
				'cronograma' => $datos['cronograma']);
			$respuesta = ModeloResidencia::guardarProtocoloRINModelo("pro_rin", $pasante, $nuevos_datos);
			return $respuesta;
		}

		/**
		 * [obtenerProtocoloRINControlador obtiene los datos del protocolo RIN]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @return [array]               [resultado de la sentencia]
		 */
		public function obtenerProtocoloRINControlador($clave_pasante){
			$respuesta = ModeloResidencia::obtenerProtocoloRINModelo("pasante_protocolo", $clave_pasante, 'rin');
			return $respuesta;
		}

		/**
		 * [actualizarProtocoloRINControlador actualiza los valores del protocolo RIN]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @param  [array] $datos         [contiene los datos del protocolo RIN]
		 * @return [array]                [resultado de la sentencia]
		 */
		public function actualizarProtocoloRINControlador($clave_pasante, $datos){
			if ($datos['fecha_ini'] == '') 
				$datos['fecha_ini'] = null;
			if ($datos['fecha_fin'] == '')
				$datos['fecha_fin'] = null;
			$respuesta = ModeloResidencia::actualizarProtocoloRINModelo("pro_rin", $clave_pasante, $datos);
			return $respuesta;
		}

		/**
		 * [cambioSolicitudProtocoloRINControlador genera una solicitud de revisión del protocolo]
		 * @param  [type] $clave_pasante [clave del pasante]
		 * @param  [type] $trabajo       [clave del tipo de trabajo]
		 * @return [type]                [description]
		 */
		public function cambioSolicitudProtocoloRINControlador($clave_pasante, $trabajo){
			$respuesta = ModeloResidencia::cambioSolicitudProtocoloRINModelo("pasante_protocolo", $clave_pasante, $trabajo);
			return $respuesta;
		}
	}
?>
<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 5 de ABRIL 2020
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo intermediario entre las vistas y el modelo.
*  ANOTACIONES:
*/

/*REQUERIMIENTOS DE ARCHIVOS*/

	/**
	 * Clase ControladorAplicacion
	 */
	class ControladorAplicacion{

		/**
		 * [guardarProtocoloRACControlador guarda por primera vez los datos de un protocolo RAC]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [contiene los datos del protocolo RAC]
		 * @return [type]          [resultado de la sentencia]
		 */
		public function guardarProtocoloRACControlador($pasante, $datos){
			$nuevos_datos = array('titulo' => $datos['tituloArticulo'],
								  'area' => $datos['area'],
								  'asesor' => $datos['asesor'],
								  'planteamiento' => $datos['planteamiento'],
								  'justificacion' => $datos['justificacion'],
								  'objetivo' => $datos['objetivo'],
								  'metodologia' => $datos['metodologia'],
								  'referencias' => $datos['referencias'],
								  'cronograma' => $datos['cronograma']);
			$respuesta = ModeloAplicacion::guardarProtocoloRACModelo("pro_rac", $pasante, $nuevos_datos);
			return $respuesta;
		}

		/**
		 * [obtenerProtocoloRACControlador obtiene los datos del protocolo RAC]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @return [array]               [resultado de la sentencia]
		 */
		public function obtenerProtocoloRACControlador($clave_pasante){
			$respuesta = ModeloAplicacion::obtenerProtocoloRACModelo("pasante_protocolo", $clave_pasante, 'rac');
			return $respuesta;
		}

		/**
		 * [actualizarProtocoloRACControlador actualiza los valores del protocolo RAC]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @param  [array] $datos         [contiene los datos del protocolo RAC]
		 * @return [array]                [resultado de la sentencia]
		 */
		public function actualizarProtocoloRACControlador($clave_pasante, $datos){
			$respuesta = ModeloAplicacion::actualizarProtocoloRACModelo("pro_rac", $clave_pasante, $datos);
			return $respuesta;
		}

		/**
		 * [cambioSolicitudProtocoloRACControlador description]
		 * @param  [type] $clave_pasante [description]
		 * @param  [type] $trabajo       [description]
		 * @return [type]                [description]
		 */
		public function cambioSolicitudProtocoloRACControlador($clave_pasante, $trabajo, $datos){
			$guardar = ControladorAplicacion::guardarProtocoloRACControlador($clave_pasante, $datos);
			if ($guardar) {
				$respuesta = ModeloAplicacion::registrarProtocoloAEMModelo("pasante_protocolo", $clave_pasante, $tipo_trabajo);
				return $respuesta;
			}else
				return $guardar;	
		}
	}
?>
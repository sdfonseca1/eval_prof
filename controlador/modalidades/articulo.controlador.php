<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 3 de ABRIL 2020
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo intermediario entre las vistas y el modelo.
*  ANOTACIONES:
*/

/*REQUERIMIENTOS DE ARCHIVOS*/

	/**
	 * Clase ControladorArtículo
	 */
	class ControladorArticulo{

		/**
		 * [guardarProtocoloARTControlador guarda por primera vez los datos de un protocolo ART]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [contiene los datos del protocolo ART]
		 * @return [type]          [resultado de la sentencia]
		 */
		public function guardarProtocoloARTControlador($pasante, $datos){
			$nuevos_datos = array('titulo' => $datos['tituloArticulo'],
								  'area' => $datos['area'],
								  'asesor' => $datos['asesor'],
								  'coasesor' => $datos['coasesor'],
								  'area' => $datos['area'],
								  'revista' => $datos['nombreRevista'],
								  'indice' => $datos['nombreIndice'],
								  'descripcion' => $datos['descripcion'],
								  'referencias' => $datos['referencias'],
								  'cronograma' => $datos['cronograma']);
			$respuesta = ModeloArticulo::guardarProtocoloARTModelo("pro_art", $pasante, $nuevos_datos);
			return $respuesta;
		}

		/**
		 * [obtenerProtocoloARTControlador obtiene los datos del protocolo ART]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @return [array]               [resultado de la sentencia]
		 */
		public function obtenerProtocoloARTControlador($clave_pasante){
			$respuesta = ModeloArticulo::obtenerProtocoloARTModelo("pasante_protocolo", $clave_pasante, 'art');
			return $respuesta;
		}

		/**
		 * [actualizarProtocoloARTControlador actualiza los valores del protocolo ART]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @param  [array] $datos         [contiene los datos del protocolo ART]
		 * @return [array]                [resultado de la sentencia]
		 */
		public function actualizarProtocoloARTControlador($clave_pasante, $datos){
			$respuesta = ModeloArticulo::actualizarProtocoloARTModelo("pro_art", $clave_pasante, $datos);
			return $respuesta;
		}

		public function existeProtocoloARTControlador($clave_pasante){
			// $respuesta = ModeloArticulo::existeProtocoloARTModelo($clave_pasante);
			// if (isset($respuesta['COD_ERR']){
			// 	return false;
			// }
			// else if(empty($respuesta)){
			// 	return false;
			// }
			// else{
			// 	return true;
			// }
				
		}

		/**
		 * [cambioSolicitudProtocoloARTControlador description]
		 * @param  [type] $clave_pasante [description]
		 * @param  [type] $trabajo       [description]
		 * @return [type]                [description]
		 */
		public function registrarProtocoloARTControlador($clave_pasante, $trabajo, $datos){
			// var_dump(ControladorArticulo::existeProtocoloARTControlador($clave_pasante));
			// if (ControladorArticulo::existeProtocoloARTControlador($clave_pasante)) {
				$guardar = ControladorArticulo::actualizarProtocoloARTControlador($clave_pasante, $datos);
				if ($guardar) {
					$respuesta = ModeloArticulo::registrarProtocoloARTModelo("pasante_protocolo", $clave_pasante, $tipo_trabajo);
					return $respuesta;
				}else
					return $guardar;
			// }
		}
	}
?>
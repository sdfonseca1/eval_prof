<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 5 de ABRIL 2020
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo intermediario entre las vistas y el modelo.
*  ANOTACIONES:
*/

/*REQUERIMIENTOS DE ARCHIVOS*/

	/**
	 * Clase ControladorMemoria
	 */
	class ControladorMemoria{

		/**
		 * [guardarProtocoloMELControlador guarda por primera vez los datos de un protocolo MEL]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [contiene los datos del protocolo MEL]
		 * @return [type]          [resultado de la sentencia]
		 */
		public function guardarProtocoloMELControlador($pasante, $datos){
			$nuevos_datos = array(
				'titulo' => $datos['titulo'],
				'fecha_ini' => $datos['fecha_ini'] == '' ? null : $datos['fecha_ini'],
				'fecha_fin' => $datos['fecha_fin'] == '' ? null : $datos['fecha_fin'],
				'asesor' => $datos['asesor'],
				'area' => $datos['area'],
				'descripcion' => $datos['descripcion'],
				'justificacion' => $datos['justificacion'],
				'asesor' => $datos['asesor'],
				'objetivo' => $datos['objetivo'],
				'referencias' => $datos['referencias'],
				'cronograma' => $datos['cronograma']);
			$respuesta = ModeloMemoria::guardarProtocoloMELModelo("pro_mel", $pasante, $nuevos_datos);
			return $respuesta;
		}

		/**
		 * [obtenerProtocoloMELControlador obtiene los datos del protocolo MEL]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @return [array]               [resultado de la sentencia]
		 */
		public function obtenerProtocoloMELControlador($clave_pasante){
			$respuesta = ModeloMemoria::obtenerProtocoloMELModelo("pasante_protocolo", $clave_pasante, 'MEL');
			return $respuesta;
		}

		/**
		 * [actualizarProtocoloMELControlador actualiza los valores del protocolo MEL]
		 * @param  [text] $clave_pasante [clave del pasante]
		 * @param  [array] $datos         [contiene los datos del protocolo MEL]
		 * @return [array]                [resultado de la sentencia]
		 */
		public function actualizarProtocoloMELControlador($clave_pasante, $datos){
			if ($datos['fecha_ini'] == '') 
				$datos['fecha_ini'] = null;
			if ($datos['fecha_fin'] == '')
				$datos['fecha_fin'] = null;
			$respuesta = ModeloMemoria::actualizarProtocoloMELModelo("pro_mel", $clave_pasante, $datos);
			return $respuesta;
		}

		/**
		 * [cambioSolicitudProtocoloMELControlador genera una solicitud de revisión del protocolo]
		 * @param  [type] $clave_pasante [clave del pasante]
		 * @param  [type] $trabajo       [clave del tipo de trabajo]
		 * @return [type]                [description]
		 */
		public function cambioSolicitudProtocoloMELControlador($clave_pasante, $trabajo){
			$respuesta = ModeloMemoria::cambioSolicitudProtocoloMELModelo("pasante_protocolo", $clave_pasante, $trabajo);
			return $respuesta;
		}
	}
?>
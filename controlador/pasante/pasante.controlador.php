<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 9 DE ABRIL 2020
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo intermediario entre las vistas y el modelo.
*  ANOTACIONES:
*/

	/**
	 * Clase de comunicación con el modelo de datos para el pasante.
	 */
	class ControladorPasante{

		/**
		 * [buscarClavePasanteControlador busca la clave del pasante en la base de datos]
		 * @param  [text] $clave_pasante [la clave del pasante]
		 * @return [array]               []
		 */
		public function buscarClavePasanteControlador($clave_pasante){
			$respuesta = ModeloPasante::buscarClavePasanteModelo('pasante', $clave_pasante);
			if ($respuesta['clavePasante'] == $clave_pasante) {
				return true;
			}
			return $respuesta;
		}
		
		/**
		 * [registrarPasanteControlador registra a un nuevo pasante]
		 * @param  [array] $datos_pasante [arreglo con los datos del nuevo pasante]
		 * @return [array]                []
		 */
		public function registrarPasanteControlador($datos_pasante){
		    $respuesta = ModeloPasante::registrarPasanteModelo('pasante', $datos_pasante);
		    //Agregar línea para envíar correo electrónico de confirmación.
		    return $respuesta;
		    // Modelo::obtenerLicenciaturasModelo('licenciatura');
		    // echo "ADios";
		    // var_dump($respuesta);
		    // return $respuesta;
		    // if ($respuesta) {
		    // 	Controlador::enviarCorreoRegistroControlador($datos_pasante['correo']);
		    // 	return true;
		    // }else{
		    // 	return false;
		    // }
		}

		/**
		 * [ingresoPasanteControlador revisa los datos de ingreso del pasante]
		 * @param  [array] $datos [los datos de ingreso al sistema del pasante]
		 * @return [type]        [description]
		 */
		public function ingresoPasanteControlador($datos){
			$respuesta = ModeloPasante::ingresoPasanteModelo('pasante', $datos['usuario']);
			if ($respuesta['clavePasante'] == $datos['usuario'] && password_verify($datos['contrasena'], $respuesta['contrasena'])){
				if ($respuesta['aceptado'] == 1) {
					session_start();
					$_SESSION['usuario'] = $datos['usuario'];
					$_SESSION['esPasante'] = true;
					$_SESSION['licenciatura'] = $respuesta['licenciatura'];
					return 1;//Todo correcto y redirigirá a la página de inicio del pasante
				}else{
					return 0;//Vuelve a redirigir a la página de ingreso
				}
			}else
				return -1;
		}

		/**
		 * [obtenerNombrePasanteControlador obtiene el nombre completo del pasante]
		 * @param  [text] $clave_pasante [clave del pasante para la base de datos]
		 * @return [objeto] $respuesta [regresa nombre completo del pasante o error, si lo hubiera]
		 */
		public function obtenerNombrePasanteControlador($clave_pasante){
			$respuesta = ModeloPasante::obtenerNombrePasanteModelo('pasante', $clave_pasante);
			return $respuesta;
		}

		/**
		 * [obtenerMisDatosPasanteControlador muestra los datos básicos del pasante]
		 * @param  [text] $clave_pasante [la clave del pasante]
		 * @return [type]                [description]
		 */
		public function obtenerMisDatosPasanteControlador($clave_pasante){
			$respuesta = ModeloPasante::obtenerMisDatosPasanteModelo('pasante', $clave_pasante);
			return $respuesta;
		}

		/**
		 * [actualizarDatosPasanteControlador actualiza los datos del pasante]
		 * @param  [text] $clave_pasante [la clave del pasante]
		 * @param  [text] $datos         [los nuevos datos del pasante
		 * @return [array]               [description]
		 */
		public function actualizarDatosPasanteControlador($clave_pasante, $datos) {
			$respuesta = ModeloPasante::actualizarDatosPasanteModelo("pasante", $clave_pasante, $datos);
			return $respuesta;
		}
		
		/**
		 * [actualizarContrasenaPasanteControlador cambia la contraseña actual del pasante]
		 * @param  [text] $clave_pasante [la clave del pasante]
		 * @param  [array] $datos        [la nueva contraseña del pasante]
		 * @return [array]               [description]
		 */
		public function actualizarContrasenaPasanteControlador($clave_pasante, $datos){
			$respuesta = ModeloPasante::actualizarContrasenaPasanteModelo('pasante', $clave_pasante, $datos['contrasena']);
			return $respuesta;
		}

		/**
		 * [existeSolicitudRegistradaControlador verifica si el pasante ya tiene una solicitud registrada]
		 * @param  [text] $clave_pasante [la clave del pasante]
		 * @return [type]                [description]
		 */
		public function existeSolicitudRegistradaControlador($clave_pasante){
			$respuesta = ModeloPasante::existeSolicitudRegistradaModelo('pasante_protocolo', $clave_pasante);
			return $respuesta;
		}

		/**
		 * [existeBorradorProtocoloControlador verifica si hay un protocolo en edición]
		 * @param  [type] $clave_pasante [description]
		 * @return [type]          [description]
		 */
		public function existeBorradorProtocoloControlador($clave_pasante){
			$respuesta = ModeloPasante::existeBorradorProtocoloModelo("pasante_protocolo", $clave_pasante);
			return $respuesta;
		}

		/**
		 * [mostrarDatosAEdicionControlador muestra los datos para editar del pasante]
		 * @param  [text] $clave_pasante [la clave del pasante]
		 * @return [array]               [description]
		 */
		public function mostrarDatosAEdicionControlador($clave_pasante){
			$respuesta = ModeloPasante::mostrarDatosAEdicionModelo('pasante', $clave_pasante);
			return $respuesta;
		}

		/**
		 * [obtenerEtapaProcesoControlador obtiene la etapa en la que se encuentra el pasante]
		 * @param  [text] $pasante [clave del pasante]
		 * @return [array]          [resultado de la sentencia]
		 */
		public function obtenerEtapaProcesoControlador($pasante){
			$respuesta = ModeloPasante::obtenerEtapaProcesoModelo('pasante_protocolo', $pasante);
			return $respuesta;
		}

		/**
		 * [obtenerLicenciaturasControlador obtiene la lista de licenciaturas registradas en el sistema]
		 * @return [array] [description]
		 */
		public function obtenerLicenciaturasControlador(){
			$respuesta = ModeloPasante::obtenerLicenciaturasModelo('licenciatura');
			return $respuesta;
		}

		/**
		 * [obtenerEscuelasControlador btiene la lista de escuelas registradas en el sistema]
		 * @return [array] [description]
		 */
		public function obtenerEscuelasControlador(){
			$respuesta = ModeloPasante::obtenerEscuelasModelo('escuela');
			return $respuesta;
		}

	}
?>
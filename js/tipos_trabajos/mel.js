'use strict';
$(document).ready(function() {

	$('#btnGuardarProtocoloModal').hide();

	obtenerProtocoloMEL();

	//Guardar protocolo MEL validación.
	$(document).on('click', '#btnGuardarProtocoloModal', function(event) {
		event.preventDefault();
		let d = obtenerDatosMEL();
		if (d.asesor != '' && d.area != '')
			mostrarModal('guardarModal');
		else
			mostrarModalAlerta('error', 'Para guardar el protocolo debe seleccionar obligatoriamente Asesor y Área Académica');
	});

	//Guardar datos del protocolo MEL YA VALIDADOS
	$(document).on('click', '#btnGuardarProtocoloMEL', function(event) {
		let d = obtenerDatosMEL();
		guardarProtocoloMEL(d);
	});

	// Botón actualizar protocolo
	$('#btnActualizarProtocoloMEL').click(function (event){
		let mel = obtenerDatosMEL();
		actualizarProtocoloMEL(mel);
	});

	// Botón registrar solicitud
	$(document).on('click', '#btnRegistrarSolicitud', function (event){
		let mel = obtenerDatosMEL();
		if (datosLlenosMEL(mel))
			mostrarModal('registroModal');
		else
			mostrarModalAlerta('error', 'Para registrar la solictud TODOS los campos deben estar llenos.');
	})

	// Registrar protocolo confirmación del modal
	$(document).on('click', '#btnRegistrarProtocoloMEL', function(event) {
		let mel = obtenerDatosMEL();
		registrarProtocoloMEL(mel);
	});

});

/**
 * [registrarProtocoloMEL registra los datos del protocolo MEL]
 * @param  {[array]} datos [datos del formulario MEL]
 * @return {[type]}       [description]
 */
function registrarProtocoloMEL(datos){
	var datos = {
		funcion: 'actualizarProtocoloMEL',
		protocolo: datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.memoria.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Se han guardado los datos con éxito.');
				cerrarModal('actualizarModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [cambioEstadoSolicitud cambia el estado del protocolo temporal a Solicitud registrada (1)]
 * @return {[type]} [description]
 */
function cambioEstadoSolicitud(){
	var datos = {
		funcion: 'cambioSolicitudProtocoloMEL'
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.memoria.php",
		data: datos,
		dataType: 'text',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				cerrarModal('registroModal');
				mostrarModalAlerta('exito', 'Solicitud registrada correctamente. Puede consultar el estado'+
					' en la sección "Estado del proceso de evaluación de mi protocolo".\nLa página será recargada de nuevo en 5 segundos.');
				setTimeout("location.reload(true);", 5000);
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [datosLlenosMEL verifica si todos los campos del protocolo están llenos]
 * @param  {[arrray]} datos [arreglo de datos del formulario MEL]
 * @return {[boolean]}       [falso o verdadero]
 */
function datosLlenosMEL(datos){
	if (datos['titulo'] != '' && datos['fecha_ini'] != '' && datos['fecha_fin'] != '' &&
		datos['asesor'] != '' && datos['area'] != '' && datos['descripcion'] != '' &&
		datos['justificacion'] != '' && datos['objetivo'] != '' && datos['referencias'] != '' &&
		datos['cronograma'] != '')
		return true;
	else
		return false;
}

/**
 * [actualizarProtocoloMEL actualiza los campos del protocolo]
 * @param  {[array]} datos [nuevos datos del formulario del protocolo MEL]
 * @return {[type]}       [description]
 */
function actualizarProtocoloMEL(datos){
	var datos = {
		funcion: 'actualizarProtocoloMEL',
		protocolo: datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.memoria.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Datos del protocolo actualizados correctamente.');
				cerrarModal('actualizarModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [llenarProtocoloMEL llena el formulario del protocolo MEL con los datos guardados de la base de datos]
 * @param  {[array]} datos [datos obtenidos de la base de datos]
 * @return {[type]}       [description]
 */
function llenarProtocoloMEL(datos){
	$("#formGroupTituloMEL").val(datos['titulo']);
	$("#formGroupPeriodoInicioMEL").val(datos['periodoInicio']);
	$("#formGroupPeriodoFinMEL").val(datos['periodoTermino']);
	$('#formGroupAsesor').val(datos['asesor']);
	$("#formGroupAreaAcademica").val(datos['areaAcademica']);
	$("#formGroupDescripcionMEL").val(datos['descripcion']);
	$("#formGroupJustificacionMEL").val(datos['justificacion']);
	$("#formGroupObjetivoGeneralMEL").val(datos['objetivoGeneral']);
	$("#formGroupReferenciasMEL").val(datos['referencias']);
	if (datos['cronograma'] != 'null')
		convertirJSONTabla(datos['cronograma']);
	$('#btnGuardarProtocoloModal').hide();
}

/**
 * [obtenerProtocoloMEL obtiene los campos del protocolo MEL de la base de datos]
 * @return {[type]} [description]
 */
function obtenerProtocoloMEL(){
	var dato = {
		funcion: 'obtenerProtocoloMEL'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.memoria.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.length > 0)
				llenarProtocoloMEL(respuesta[0]);
			else{
				$('#btnGuardarProtocoloModal').show();
				$('#btnActualizarProtocoloModal').hide();
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			console.log('Error obtenrProtocoloMEL')
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [obtenerDatosMEL obtiene los valores del formularo MEL]
 * @return {[array]} datos_MEL [obtiene los datos del formulario MEL sin validar]
 */
function obtenerDatosMEL(){
	var datos_mel = {
		titulo: $("#formGroupTituloMEL").val(),
		fecha_ini: $("#formGroupPeriodoInicioMEL").val(),
		fecha_fin: $("#formGroupPeriodoFinMEL").val(),
		asesor: $("#formGroupAsesor").val(),
		area: $("#formGroupAreaAcademica").val(),
		descripcion: $("#formGroupDescripcionMEL").val(),
		justificacion: $("#formGroupJustificacionMEL").val(),
		objetivo: $("#formGroupObjetivoGeneralMEL").val(),
		referencias: $("#formGroupReferenciasMEL").val(),
		cronograma: convertirTablaJSON("#cronograma-mel"),
		completo: ''
	};
	return datos_mel;
}

/**
 * [existeBorradorProtocolo verifica si ya existe una solicitud guardado en la base de datos]
 * @return {[type]} [description]
 */
function existeBorradorProtocolo(){
	var dato = {
		funcion: 'existeBorradorProtocolo'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.memoria.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			var valor = tipoTrabajo(respuesta[1]);
			if (respuesta) {
				$("#alerta_solicitud").css('display','none');
				$('#tipos_trabajo').hide();
				$('#notaExistencia').append('<div class="text-center row">'+
					'<p class="alert alert-info col-12">Ya existe un protocolo de '+valor+' en borrador. Si desea iniciar con una '+
					'nueva modalidad de trabajo escrito, deberá <strong>ELIMINAR</strong> el actual borrador.<br>'+
					'<button class="btn btn-success col-sm-12 col-md-2" id="editarProtocolo">Editar</button>'+
					'<button class="btn btn-danger col-sm-12 offset-md-3 col-md-2" id="eliminarProtocolo">Eliminar</button></div>');
			}else
				obtenerTiposDeTrabajo();
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [guardarProtocoloMEL guarda los datos del formulario del protocolo MEL]
 * @param  {[array]} mel_datos [datos del protocolo MEL para ser guardados]
 * @return {[type]}           [description]
 */
function guardarProtocoloMEL(mel_datos){
	var datos = {
		funcion: 'guardarProtocoloMEL',
		datos:mel_datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.memoria.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else {
				mostrarModalAlerta('exito', 'Se han guardado los datos con éxito.');
				cerrarModal('guardarModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [convertirJSONTabla convierte un json a la tabla del cronograma]
 * @param  {[text]} cronograma [id del objeto JSON]
 * @return {[type]}            [description]
 */
function convertirJSONTabla(cronograma){
	let datos_cronograma = JSON.parse(cronograma);
	datos_cronograma.forEach( function(e, i) {
		$("#contenido-actividad").append("<tr id='actividad"+e['#']+
	 	"'><td><button type='button' class='close btn-danger' data-dismiss='alert' value='"+e['#']+
	 	"'>&times;</button></td><td name='numero' class='no_act'>"+e['#']+"</td><td name='nombre"+e['#']+
	 	"' class='nombre_act'>"+e['Actividad']+"</td>");
	 	for (var i = 24; i > 0; i--) {
	 		if (e[i] != '') 
	 			$('td[name=nombre'+e['#']+']').after("<td name='m'"+i+" class='bg-success seleccionado mes'>"+e[i]+"</td>");
	 		else
	 			$('td[name=nombre'+e['#']+']').after("<td name='m'"+i+" class='mes'>"+e[i]+"</td>");
		}
		$('td[name=m4]').after("</tr>");
	});
}

/**
 * [convertirTablaJSON convierte una tabla html a archivo json]
 * @param  {[text]} cronograma [id del objeto del DOM que pertenece al cronograma]
 * @return {[json]}            [tabla en formato JSON]
 */
function convertirTablaJSON(cronograma){
	var tabla = $(cronograma).tableToJSON();
	return tabla;
}
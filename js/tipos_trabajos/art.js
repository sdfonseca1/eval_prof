'use strict';
$(document).ready(function() {

	$('#btnGuardarProtocoloModal').hide();

	obtenerProtocoloART();

	//Guardar protocolo ART validación.
	$(document).on('click', '#btnGuardarProtocoloModal', function(event) {
		event.preventDefault();
		let d = obtenerDatosART();
		if (d.asesor != '' && d.area != '' && d.coasesor)
			mostrarModal('guardarModal');
		else
			mostrarModalAlerta('error', 'Para guardar el protocolo debe seleccionar obligatoriamente Área Académica, Asesor y Coasesor');
	});

	//Guardar datos del protocolo ART YA VALIDADOS
	$(document).on('click', '#btnGuardarProtocoloART', function(event) {
		let d = obtenerDatosART();
		guardarProtocoloART(d);
	});

	// Botón actualizar protocolo
	$('#btnActualizarProtocoloART').click(function (event){
		let ART = obtenerDatosART();
		actualizarProtocoloART(ART);
	});

	// Botón registrar solicitud
	$(document).on('click', '#btnRegistrarSolicitud', function (event){
		let ART = obtenerDatosART();
		if (datosLlenosART(ART))
			mostrarModal('registroModal');
		else
			mostrarModalAlerta('error', 'Para registrar la solictud TODOS los campos deben estar llenos.');
	})

	// Registrar protocolo confirmación del modal
	$(document).on('click', '#btnRegistrarProtocoloART', function(event) {
		let art = obtenerDatosART();
		registrarProtocoloART(art);
	});

});

/**
 * [registrarProtocoloaART registra los datos del protocolo ART]
 * @param  {[array]} datos [datos del formulario ART]
 * @return {[type]}       [description]
 */
function registrarProtocoloART(datos){
	var datos = {
		funcion: 'cambioSolicitudProtocoloART',
		protocolo: datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.articulo.php",
		data: datos,
		dataType: 'text',
		encode: true,
		success: function(respuesta){
			console.log(respuesta)
			// if (respuesta.hasOwnProperty("COD_ERR"))
			// 	mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			// else{
			// 	mostrarModalAlerta('exito', 'Ha registrado su protocolo para revisión. Consulte la sección'+
			// 		'"Estado del proceso de evaluación de protocolos" para conocer el estado actual de esta solicitud.');
			// 	cerrarModal('registroModal');
			// }
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [cambioEstadoSolicitud cambia el estado del protocolo temporal a Solicitud registrada (1)]
 * @return {[type]} [description]
 */
function cambioEstadoSolicitud(){
	var datos = {
		funcion: 'cambioSolicitudProtocoloART'
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.articulo.php",
		data: datos,
		dataType: 'text',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				cerrarModal('registroModal');
				mostrarModalAlerta('exito', 'Solicitud registrada correctamente. Puede consultar el estado'+
					' en la sección "Estado del proceso de evaluación de mi protocolo".\nLa página será recargada de nuevo en 5 segundos.');
				setTimeout("location.reload(true);", 5000);
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [datosLlenosART verifica si todos los campos del protocolo están llenos]
 * @param  {[arrray]} datos [arreglo de datos del formulario ART]
 * @return {[boolean]}       [falso o verdadero]
 */
function datosLlenosART(datos){
	if (datos['titulo'] != ''  && datos['area'] != '' && datos['asesor'] != '' && 
		datos['coasesor'] != '' && datos['revista'] != '' && datos['indice'] != '' &&
		datos['descripcion'] != '' && datos['referencias'] != '' &&datos['cronograma'] != '')
		return true;
	else
		return false;
}

/**
 * [actualizarProtocoloART actualiza los campos del protocolo]
 * @param  {[array]} datos [nuevos datos del formulario del protocolo ART]
 * @return {[type]}       [description]
 */
function actualizarProtocoloART(datos){
	var datos = {
		funcion: 'actualizarProtocoloART',
		protocolo: datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.articulo.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Se han guardado los datos con éxito.');
				cerrarModal('actualizarModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [llenarProtocoloART llena el formulario del protocolo ART con los datos guardados de la base de datos]
 * @param  {[array]} datos [datos obtenidos de la base de datos]
 * @return {[type]}       [description]
 */
function llenarProtocoloART(datos){
	$("#formGroupTituloART").val(datos['tituloArticulo']);
	$("#formGroupAreaAcademica").val(datos['areaAcademica']);
	$('#formGroupAsesor').val(datos['asesor']);
	$('#formGroupCoasesor').val(datos['coasesor']);
	$('#formGroupRevistaART').val(datos['nombreRevista'])
	$('#formGroupRevistaIndiceART').val(datos['nombreIndice'])
	$("#formGroupDescripcionART").val(datos['descripcion']);
	$("#formGroupReferenciasART").val(datos['referencias']);
	if (datos['cronograma'] != 'null')
		convertirJSONTabla(datos['cronograma']);
	$('#btnGuardarProtocoloModal').hide();
}

/**
 * [obtenerDatosART obtiene los valores del formularo ART]
 * @return {[array]} datos_art [obtiene los datos del formulario ART sin validar]
 */
function obtenerDatosART(){
	var datos_art = {
		titulo: $("#formGroupTituloART").val(),
		area: $("#formGroupAreaAcademica").val(),
		asesor: $("#formGroupAsesor").val(),
		coasesor: $("#formGroupCoasesor").val(),
		revista: $('#formGroupRevistaART').val(),
		indice: $('#formGroupRevistaIndiceART').val(),
		descripcion: $("#formGroupDescripcionART").val(),
		referencias: $("#formGroupReferenciasART").val(),
		cronograma: convertirTablaJSON("#cronograma-art"),
		completo: ''
	};
	return datos_art;
}

/**
 * [obtenerProtocoloART obtiene los campos del protocolo ART de la base de datos]
 * @return {[type]} [description]
 */
function obtenerProtocoloART(){
	var dato = {
		funcion: 'obtenerProtocoloART'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.articulo.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.length > 0)
				llenarProtocoloART(respuesta[0]);
			else{
				$('#btnGuardarProtocoloModal').show();
				$('#btnActualizarProtocoloModal').hide();
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [existeBorradorProtocolo verifica si ya existe una solicitud guardado en la base de datos]
 * @return {[type]} [description]
 */
function existeBorradorProtocolo(){
	var dato = {
		funcion: 'existeBorradorProtocolo'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.articulo.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			var valor = tipoTrabajo(respuesta[1]);
			if (respuesta) {
				$("#alerta_solicitud").css('display','none');
				$('#tipos_trabajo').hide();
				$('#notaExistencia').append('<div class="text-center row">'+
					'<p class="alert alert-info col-12">Ya existe un protocolo de '+valor+' en borrador. Si desea iniciar con una '+
					'nueva modalidad de trabajo escrito, deberá <strong>ELIMINAR</strong> el actual borrador.<br>'+
					'<button class="btn btn-success col-sm-12 col-md-2" id="editarProtocolo">Editar</button>'+
					'<button class="btn btn-danger col-sm-12 offset-md-3 col-md-2" id="eliminarProtocolo">Eliminar</button></div>');
			}else
				obtenerTiposDeTrabajo();
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [guardarProtocoloART guarda los datos del formulario del protocolo ART]
 * @param  {[array]} art_datos [datos del protocolo ART para ser guardados]
 * @return {[type]}           [description]
 */
function guardarProtocoloART(art_datos){
	var datos = {
		funcion: 'guardarProtocoloART',
		datos:art_datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.articulo.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Se han guardado los datos con éxito.');
				cerrarModal('guardarModal');	
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [convertirJSONTabla convierte un json a la tabla del cronograma]
 * @param  {[text]} cronograma [id del objeto JSON]
 * @return {[type]}            [description]
 */
function convertirJSONTabla(cronograma){
	let datos_cronograma = JSON.parse(cronograma);
	datos_cronograma.forEach( function(e, i) {
		$("#contenido-actividad").append("<tr id='actividad"+e['#']+
	 	"'><td><button type='button' class='close btn-danger' data-dismiss='alert' value='"+e['#']+
	 	"'>&times;</button></td><td name='numero' class='no_act'>"+e['#']+"</td><td name='nombre"+e['#']+
	 	"' class='nombre_act'>"+e['Actividad']+"</td>");
	 	for (var i = 24; i > 0; i--) {
	 		if (e[i] != '') 
	 			$('td[name=nombre'+e['#']+']').after("<td name='m'"+i+" class='bg-success seleccionado mes'>"+e[i]+"</td>");
	 		else
	 			$('td[name=nombre'+e['#']+']').after("<td name='m'"+i+" class='mes'>"+e[i]+"</td>");
		}
		$('td[name=m4]').after("</tr>");
	});
}

/**
 * [convertirTablaJSON convierte una tabla html a archivo json]
 * @param  {[text]} cronograma [id del objeto del DOM que pertenece al cronograma]
 * @return {[json]}            [tabla en formato JSON]
 */
function convertirTablaJSON(cronograma){
	var tabla = $(cronograma).tableToJSON(); // Convert the table into a javascript object
	return tabla;
}
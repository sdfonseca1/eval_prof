'use strict';
$(document).ready(function() {

	$('#btnGuardarProtocoloModal').hide();

	obtenerProtocoloAEM();

	//Guardar protocolo AEM validación.
	$(document).on('click', '#btnGuardarProtocoloModal', function(event) {
		event.preventDefault();
		let d = obtenerDatosAEM();
		if (d.asesor != '' && d.area != '')
			mostrarModal('guardarModal');
		else
			mostrarModalAlerta('error', 'Para guardar el protocolo debe seleccionar obligatoriamente Asesor y Área Académica');
	});

	//Guardar datos del protocolo AEM YA VALIDADOS
	$(document).on('click', '#btnGuardarProtocoloAEM', function(event) {
		let d = obtenerDatosAEM();
		guardarProtocoloAEM(d);
	});

	// Botón actualizar protocolo
	$('#btnActualizarProtocoloAEM').click(function (event){
		let aem = obtenerDatosAEM();
		actualizarProtocoloAEM(aem);
	});

	// Botón registrar solicitud
	$(document).on('click', '#btnRegistrarSolicitud', function (event){
		let aem = obtenerDatosAEM();
		if (datosLlenosAEM(aem))
			mostrarModal('registroModal');
		else
			mostrarModalAlerta('error', 'Para registrar la solictud TODOS los campos deben estar llenos.');
	})

	// Registrar protocolo confirmación del modal
	$(document).on('click', '#btnRegistrarProtocoloAEM', function(event) {
		let aem = obtenerDatosAEM();
		registrarProtocoloAEM(aem);
	});

});

/**
 * [registrarProtocoloAEM registra los datos del protocolo AEM]
 * @param  {[array]} datos [datos del formulario AEM]
 * @return {[type]}       [description]
 */
function registrarProtocoloAEM(datos){
	var datos = {
		funcion: 'registrarProtocoloAEM',
		protocolo: datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.autoempleo.php",
		data: datos,
		dataType: 'text',
		encode: true,
		success: function(respuesta){
			console.log(respuesta)
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Ha registrado su protocolo para revisión. Consulte la sección'+
					'"Estado del proceso de evaluación de protocolos" para conocer el estado actual de esta solicitud.');
				cerrarModal('registroModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [cambioEstadoSolicitud cambia el estado del protocolo temporal a Solicitud registrada (1)]
 * @return {[type]} [description]
 */
function cambioEstadoSolicitud(){
	var datos = {
		funcion: 'cambioSolicitudProtocoloAEM'
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.autoempleo.php",
		data: datos,
		dataType: 'text',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				cerrarModal('registroModal');
				mostrarModalAlerta('exito', 'Solicitud registrada correctamente. Puede consultar el estado'+
					' en la sección "Estado del proceso de evaluación de mi protocolo".\nLa página será recargada de nuevo en 5 segundos.');
				setTimeout("location.reload(true);", 5000);
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [datosLlenosAEM verifica si todos los campos del protocolo están llenos]
 * @param  {[arrray]} datos [arreglo de datos del formulario AEM]
 * @return {[boolean]}       [falso o verdadero]
 */
function datosLlenosAEM(datos){
	if (datos['titulo'] != '' && datos['fecha_ini'] != '' && datos['fecha_fin'] != '' &&
		datos['asesor'] != '' && datos['area'] != '' && datos['planteamiento'] != '' &&
		datos['justificacion'] != '' && datos['objetivo'] != '' && datos['referencias'] != '' &&
		datos['cronograma'] != '')
		return true;
	else
		return false;
}

/**
 * [actualizarProtocoloAEM actualiza los campos del protocolo]
 * @param  {[array]} datos [nuevos datos del formulario del protocolo AEM]
 * @return {[type]}       [description]
 */
function actualizarProtocoloAEM(datos){
	var datos = {
		funcion: 'actualizarProtocoloAEM',
		protocolo: datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.autoempleo.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Datos del protocolo actualizados correctamente.');
				cerrarModal('actualizarModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [llenarProtocoloAEM llena el formulario del protocolo AEM con los datos guardados de la base de datos]
 * @param  {[array]} datos [datos obtenidos de la base de datos]
 * @return {[type]}       [description]
 */
function llenarProtocoloAEM(datos){
	$("#formGroupTituloAEM").val(datos['titulo']);
	$("#formGroupPeriodoInicioAEM").val(datos['periodoInicio']);
	$("#formGroupPeriodoFinAEM").val(datos['periodoTermino']);
	$('#formGroupAsesor').val(datos['asesor']);
	$("#formGroupAreaAcademica").val(datos['areaAcademica']);
	$("#formGroupPlanteamientoAEM").val(datos['planteamientoProblema']);
	$("#formGroupJustificacionAEM").val(datos['justificacion']);
	$("#formGroupObjetivoGeneralAEM").val(datos['objetivoGeneral']);
	$("#formGroupReferenciasAEM").val(datos['referencias']);
	if (datos['cronograma'] != 'null')
		convertirJSONTabla(datos['cronograma']);
	$('#btnGuardarProtocoloModal').hide();
}

/**
 * [obtenerProtocoloAEM obtiene los campos del protocolo AEM de la base de datos]
 * @return {[type]} [description]
 */
function obtenerProtocoloAEM(){
	var dato = {
		funcion: 'obtenerProtocoloAEM'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.autoempleo.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.length > 0)
				llenarProtocoloAEM(respuesta[0]);
			else{
				$('#btnGuardarProtocoloModal').show();//css('display', 'block');
				$('#btnActualizarProtocoloModal').hide();
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			// console.log('obtenerProtocoloAEM');
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [obtenerDatosAEM obtiene los valores del formularo AEM]
 * @return {[array]} datos_aem [obtiene los datos del formulario AEM sin validar]
 */
function obtenerDatosAEM(){
	var datos_aem = {
		titulo: $("#formGroupTituloAEM").val(),
		fecha_ini: $("#formGroupPeriodoInicioAEM").val(),
		fecha_fin: $("#formGroupPeriodoFinAEM").val(),
		asesor: $("#formGroupAsesor").val(),
		area: $("#formGroupAreaAcademica").val(),
		planteamiento: $("#formGroupPlanteamientoAEM").val(),
		justificacion: $("#formGroupJustificacionAEM").val(),
		objetivo: $("#formGroupObjetivoGeneralAEM").val(),
		referencias: $("#formGroupReferenciasAEM").val(),
		cronograma: convertirTablaJSON("#cronograma-aem"),
		completo: ''
	};
	return datos_aem;
}

/**
 * [existeBorradorProtocolo verifica si ya existe una solicitud guardado en la base de datos]
 * @return {[type]} [description]
 */
function existeBorradorProtocolo(){
	var dato = {
		funcion: 'existeBorradorProtocolo'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.autoempleo.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			var valor = tipoTrabajo(respuesta[1]);
			if (respuesta) {
				$("#alerta_solicitud").css('display','none');
				$('#tipos_trabajo').hide();
				$('#notaExistencia').append('<div class="text-center row">'+
					'<p class="alert alert-info col-12">Ya existe un protocolo de '+valor+' en borrador. Si desea iniciar con una '+
					'nueva modalidad de trabajo escrito, deberá <strong>ELIMINAR</strong> el actual borrador.<br>'+
					'<button class="btn btn-success col-sm-12 col-md-2" id="editarProtocolo">Editar</button>'+
					'<button class="btn btn-danger col-sm-12 offset-md-3 col-md-2" id="eliminarProtocolo">Eliminar</button></div>');
			}else
				obtenerTiposDeTrabajo();
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [guardarProtocoloAEM guarda los datos del formulario del protocolo AEM]
 * @param  {[array]} aem_datos [datos del protocolo AEM para ser guardados]
 * @return {[type]}           [description]
 */
function guardarProtocoloAEM(aem_datos){
	var datos = {
		funcion: 'guardarProtocoloAEM',
		datos:aem_datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.autoempleo.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			console.log(respuesta);
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else {
				mostrarModalAlerta('exito', 'Se han guardado los datos con éxito.');
				cerrarModal('guardarModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [convertirJSONTabla convierte un json a la tabla del cronograma]
 * @param  {[text]} cronograma [id del objeto JSON]
 * @return {[type]}            [description]
 */
function convertirJSONTabla(cronograma){
	let datos_cronograma = JSON.parse(cronograma);
	datos_cronograma.forEach( function(e, i) {
		$("#contenido-actividad").append("<tr id='actividad"+e['#']+
	 	"'><td><button type='button' class='close btn-danger' data-dismiss='alert' value='"+e['#']+
	 	"'>&times;</button></td><td name='numero' class='no_act'>"+e['#']+"</td><td name='nombre"+e['#']+
	 	"' class='nombre_act'>"+e['Actividad']+"</td>");
	 	for (var i = 24; i > 0; i--) {
	 		if (e[i] != '') 
	 			$('td[name=nombre'+e['#']+']').after("<td name='m'"+i+" class='bg-success seleccionado mes'>"+e[i]+"</td>");
	 		else
	 			$('td[name=nombre'+e['#']+']').after("<td name='m'"+i+" class='mes'>"+e[i]+"</td>");
		}
		$('td[name=m4]').after("</tr>");
	});
}

/**
 * [convertirTablaJSON convierte una tabla html a archivo json]
 * @param  {[text]} cronograma [id del objeto del DOM que pertenece al cronograma]
 * @return {[json]}            [tabla en formato JSON]
 */
function convertirTablaJSON(cronograma){
	var tabla = $(cronograma).tableToJSON();
	return tabla;
}
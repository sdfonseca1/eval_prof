'use strict';
$(document).ready(function() {

	$('#btnGuardarProtocoloModal').hide();

	obtenerProtocoloENS();

	//Guardar protocolo ENS validación.
	$(document).on('click', '#btnGuardarProtocoloModal', function(event) {
		event.preventDefault();
		let d = obtenerDatosENS();
		if (d.asesor != '' && d.area != '')
			mostrarModal('guardarModal');
		else
			mostrarModalAlerta('error', 'Para guardar el protocolo debe seleccionar obligatoriamente Área Académica, Asesor y Coasesor');
	});

	//Guardar datos del protocolo ENS YA VALIDADOS
	$(document).on('click', '#btnGuardarProtocoloENS', function(event) {
		let d = obtenerDatosENS();
		guardarProtocoloENS(d);
	});

	// Botón actualizar protocolo
	$('#btnActualizarProtocoloENS').click(function (event){
		let ens = obtenerDatosENS();
		actualizarProtocoloENS(ens);
	});

	// Botón registrar solicitud
	$(document).on('click', '#btnRegistrarSolicitud', function (event){
		let ens = obtenerDatosENS();
		if (datosLlenosENS(ens))
			mostrarModal('registroModal');
		else
			mostrarModalAlerta('error', 'Para registrar la solictud TODOS los campos deben estar llenos.');
	})

	// Registrar protocolo confirmación del modal
	$(document).on('click', '#btnRegistrarProtocoloENS', function(event) {
		let ens = obtenerDatosENS();
		registrarProtocoloENS(ens);
	});

});

/**
 * [registrarProtocoloaENS registra los datos del protocolo ENS]
 * @param  {[array]} datos [datos del formulario ENS]
 * @return {[type]}       [description]
 */
function registrarProtocoloENS(datos){
	var datos = {
		funcion: 'cambioSolicitudProtocoloENS',
		protocolo: datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.ensayo.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Ha registrado su protocolo para revisión. Consulte la sección'+
					'"Estado del proceso de evaluación de protocolos" para conocer el estado actual de esta solicitud.');
				cerrarModal('registroModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [cambioEstadoSolicitud cambia el estado del protocolo temporal a Solicitud registrada (1)]
 * @return {[type]} [description]
 */
function cambioEstadoSolicitud(){
	var datos = {
		funcion: 'cambioSolicitudProtocoloENS'
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.ENSiculo.php",
		data: datos,
		dataType: 'text',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				cerrarModal('registroModal');
				mostrarModalAlerta('exito', 'Solicitud registrada correctamente. Puede consultar el estado'+
					' en la sección "Estado del proceso de evaluación de mi protocolo".\nLa página será recargada de nuevo en 5 segundos.');
				setTimeout("location.reload(true);", 5000);
			}	
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [datosLlenosENS verifica si todos los campos del protocolo están llenos]
 * @param  {[arrray]} datos [arreglo de datos del formulario ENS]
 * @return {[boolean]}       [falso o verdadero]
 */
function datosLlenosENS(datos){
	if (datos['tema'] != ''  && datos['area'] != '' && datos['asesor'] != '' && 
		datos['descripcion'] != '' && datos['justificacion'] != '' && datos['referencias'] != '' && 
		datos['cronograma'] != '')
		return true;
	else
		return false;
}

/**
 * [actualizarProtocoloENS actualiza los campos del protocolo]
 * @param  {[array]} datos [nuevos datos del formulario del protocolo ENS]
 * @return {[type]}       [description]
 */
function actualizarProtocoloENS(datos){
	var datos = {
		funcion: 'actualizarProtocoloENS',
		protocolo: datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.ensayo.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Datos del protocolo actualizados correctamente.');
				cerrarModal('actualizarModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [llenarProtocoloENS llena el formulario del protocolo ENS con los datos guardados de la base de datos]
 * @param  {[array]} datos [datos obtenidos de la base de datos]
 * @return {[type]}       [description]
 */
function llenarProtocoloENS(datos){
	$("#formGroupTemaENS").val(datos['tema']);
	$("#formGroupAreaAcademica").val(datos['areaAcademica']);
	$('#formGroupAsesor').val(datos['asesor']);
	$('#formGroupDescripcionENS').val(datos['descripcion'])
	$('#formGroupJustificacionENS').val(datos['justificacion'])
	$("#formGroupReferenciasENS").val(datos['referencias']);
	if (datos['cronograma'] != 'null')
		convertirJSONTabla(datos['cronograma']);
	$('#btnGuardarProtocoloModal').hide();
}

/**
 * [obtenerDatosENS obtiene los valores del formularo ENS]
 * @return {[array]} datos_ENS [obtiene los datos del formulario ENS sin validar]
 */
function obtenerDatosENS(){
	var datos_ens = {
		tema: $("#formGroupTemaENS").val(),
		area: $("#formGroupAreaAcademica").val(),
		asesor: $("#formGroupAsesor").val(),
		descripcion: $("#formGroupDescripcionENS").val(),
		justificacion: $("#formGroupJustificacionENS").val(),
		referencias: $("#formGroupReferenciasENS").val(),
		cronograma: convertirTablaJSON("#cronograma-ens"),
		completo: ''
	};
	return datos_ens;
}

/**
 * [obtenerProtocoloENS obtiene los campos del protocolo ENS de la base de datos]
 * @return {[type]} [description]
 */
function obtenerProtocoloENS(){
	var dato = {
		funcion: 'obtenerProtocoloENS'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.ensayo.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.length > 0)
				llenarProtocoloENS(respuesta[0]);
			else{
				$('#btnGuardarProtocoloModal').show();
				$('#btnActualizarProtocoloModal').hide();
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [existeBorradorProtocolo verifica si ya existe una solicitud guardado en la base de datos]
 * @return {[type]} [description]
 */
function existeBorradorProtocolo(){
	var dato = {
		funcion: 'existeBorradorProtocolo'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.ensayo.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			var valor = tipoTrabajo(respuesta[1]);
			if (respuesta) {
				$("#alerta_solicitud").css('display','none');
				$('#tipos_trabajo').hide();
				$('#notaExistencia').append('<div class="text-center row">'+
					'<p class="alert alert-info col-12">Ya existe un protocolo de '+valor+' en borrador. Si desea iniciar con una '+
					'nueva modalidad de trabajo escrito, deberá <strong>ELIMINAR</strong> el actual borrador.<br>'+
					'<button class="btn btn-success col-sm-12 col-md-2" id="editarProtocolo">Editar</button>'+
					'<button class="btn btn-danger col-sm-12 offset-md-3 col-md-2" id="eliminarProtocolo">Eliminar</button></div>');
			}else
				obtenerTiposDeTrabajo();
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [guardarProtocoloENS guarda los datos del formulario del protocolo ENS]
 * @param  {[array]} ENS_datos [datos del protocolo ENS para ser guardados]
 * @return {[type]}           [description]
 */
function guardarProtocoloENS(ENS_datos){
	var datos = {
		funcion: 'guardarProtocoloENS',
		datos:ENS_datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.ensayo.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Se han guardado los datos con éxito.');
				cerrarModal('guardarModal');	
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [convertirJSONTabla convierte un json a la tabla del cronograma]
 * @param  {[text]} cronograma [id del objeto JSON]
 * @return {[type]}            [description]
 */
function convertirJSONTabla(cronograma){
	let datos_cronograma = JSON.parse(cronograma);
	datos_cronograma.forEach( function(e, i) {
		$("#contenido-actividad").append("<tr id='actividad"+e['#']+
	 	"'><td><button type='button' class='close btn-danger' data-dismiss='alert' value='"+e['#']+
	 	"'>&times;</button></td><td name='numero' class='no_act'>"+e['#']+"</td><td name='nombre"+e['#']+
	 	"' class='nombre_act'>"+e['Actividad']+"</td>");
	 	for (var i = 24; i > 0; i--) {
	 		if (e[i] != '') 
	 			$('td[name=nombre'+e['#']+']').after("<td name='m'"+i+" class='bg-success seleccionado mes'>"+e[i]+"</td>");
	 		else
	 			$('td[name=nombre'+e['#']+']').after("<td name='m'"+i+" class='mes'>"+e[i]+"</td>");
		}
		$('td[name=m4]').after("</tr>");
	});
}

/**
 * [convertirTablaJSON convierte una tabla html a archivo json]
 * @param  {[text]} cronograma [id del objeto del DOM que pertenece al cronograma]
 * @return {[json]}            [tabla en formato JSON]
 */
function convertirTablaJSON(cronograma){
	var tabla = $(cronograma).tableToJSON(); // Convert the table into a javascript object
	return tabla;
}
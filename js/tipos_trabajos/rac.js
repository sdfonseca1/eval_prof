'use strict';
$(document).ready(function() {

	$('#btnGuardarProtocoloModal').hide();

	obtenerProtocoloRAC();

	//Guardar protocolo RAC validación.
	$(document).on('click', '#btnGuardarProtocoloModal', function(event) {
		event.preventDefault();
		let d = obtenerDatosRAC();
		if (d.asesor != '' && d.area != '')
			mostrarModal('guardarModal');
		else
			mostrarModalAlerta('error', 'Para guardar el protocolo debe seleccionar obligatoriamente Área Académica, Asesor y Coasesor');
	});

	//Guardar datos del protocolo RAC YA VALIDADOS
	$(document).on('click', '#btnGuardarProtocoloRAC', function(event) {
		let d = obtenerDatosRAC();
		guardarProtocoloRAC(d);
	});

	// Botón actualizar protocolo
	$('#btnActualizarProtocoloRAC').click(function (event){
		let rac = obtenerDatosRAC();
		actualizarProtocoloRAC(rac);
	});

	// Botón registrar solicitud
	$(document).on('click', '#btnRegistrarSolicitud', function (event){
		let rac = obtenerDatosRAC();
		if (datosLlenosRAC(rac))
			mostrarModal('registroModal');
		else
			mostrarModalAlerta('error', 'Para registrar la solictud TODOS los campos deben estar llenos.');
	})

	// Registrar protocolo confirmación del modal
	$(document).on('click', '#btnRegistrarProtocoloRAC', function(event) {
		let rac = obtenerDatosRAC();
		registrarProtocoloRAC(rac);
	});

});

/**
 * [registrarProtocoloaRAC registra los datos del protocolo RAC]
 * @param  {[array]} datos [datos del formulario RAC]
 * @return {[type]}       [description]
 */
function registrarProtocoloRAC(datos){
	var datos = {
		funcion: 'actualizarProtocoloRAC',
		protocolo: datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.aplicacion.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Se han guardado los datos con éxito.');
				cerrarModal('actualizarModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [cambioEstadoSolicitud cambia el estado del protocolo temporal a Solicitud registrada (1)]
 * @return {[type]} [description]
 */
function cambioEstadoSolicitud(){
	var datos = {
		funcion: 'cambioSolicitudProtocoloRAC'
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.aplicacion.php",
		data: datos,
		dataType: 'text',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				cerrarModal('registroModal');
				mostrarModalAlerta('exito', 'Solicitud registrada correctamente. Puede consultar el estado'+
					' en la sección "Estado del proceso de evaluación de mi protocolo".\nLa página será recargada de nuevo en 5 segundos.');
				setTimeout("location.reload(true);", 5000);
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [datosLlenosRAC verifica si todos los campos del protocolo están llenos]
 * @param  {[arrray]} datos [arreglo de datos del formulario RAC]
 * @return {[boolean]}       [falso o verdadero]
 */
function datosLlenosRAC(datos){
	if (datos['titulo'] != ''  && datos['area'] != '' && datos['asesor'] != '' && 
		datos['coasesor'] != '' && datos['revista'] != '' && datos['indice'] != '' &&
		datos['descripcion'] != '' && datos['referencias'] != '' &&datos['cronograma'] != '')
		return true;
	else
		return false;
}

/**
 * [actualizarProtocoloRAC actualiza los campos del protocolo]
 * @param  {[array]} datos [nuevos datos del formulario del protocolo RAC]
 * @return {[type]}       [description]
 */
function actualizarProtocoloRAC(datos){
	var datos = {
		funcion: 'actualizarProtocoloRAC',
		protocolo: datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.aplicacion.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Se han guardado los datos con éxito.');
				cerrarModal('actualizarModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [llenarProtocoloRAC llena el formulario del protocolo RAC con los datos guardados de la base de datos]
 * @param  {[array]} datos [datos obtenidos de la base de datos]
 * @return {[type]}       [description]
 */
function llenarProtocoloRAC(datos){
	$("#formGroupTituloRAC").val(datos['tituloRACiculo']);
	$("#formGroupAreaAcademica").val(datos['areaAcademica']);
	$('#formGroupAsesor').val(datos['asesor']);
	$('#formGroupPlanteamientoRAC').val(datos['planteamientoProblema']);
	$('#formGroupJustificacionRAC').val(datos['justificacion']);
	$("#formGroupObjetivoGeneralRAC").val(datos['objetivoGeneral']);
	$("#formGroupMetodologiaRAC").val(datos['metodologia']);
	$("#formGroupReferenciasRAC").val(datos['referencias']);
	if (datos['cronograma'] != 'null')
		convertirJSONTabla(datos['cronograma']);
	$('#btnGuardarProtocoloModal').hide();
}

/**
 * [obtenerDatosRAC obtiene los valores del formularo RAC]
 * @return {[array]} datos_RAC [obtiene los datos del formulario RAC sin validar]
 */
function obtenerDatosRAC(){
	var datos_rac = {
		titulo: $("#formGroupTituloRAC").val(),
		area: $("#formGroupAreaAcademica").val(),
		asesor: $("#formGroupAsesor").val(),
		planteamiento: $("#formGroupPlanteamientoRAC").val(),
		justificacion: $('#formGroupJustificacionRAC').val(),
		objetivo: $('#formGroupObjetivoGeneralRAC').val(),
		metodologia: $("#formGroupMetodologiaRAC").val(),
		referencias: $("#formGroupReferenciasRAC").val(),
		cronograma: convertirTablaJSON("#cronograma-rac"),
		completo: ''
	};
	return datos_rac;
}

/**
 * [obtenerProtocoloRAC obtiene los campos del protocolo RAC de la base de datos]
 * @return {[type]} [description]
 */
function obtenerProtocoloRAC(){
	var dato = {
		funcion: 'obtenerProtocoloRAC'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.aplicacion.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.length > 0)
				llenarProtocoloRAC(respuesta[0]);
			else{
				$('#btnGuardarProtocoloModal').show();
				$('#btnActualizarProtocoloModal').hide();
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [existeBorradorProtocolo verifica si ya existe una solicitud guardado en la base de datos]
 * @return {[type]} [description]
 */
function existeBorradorProtocolo(){
	var dato = {
		funcion: 'existeBorradorProtocolo'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.aplicacion.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			var valor = tipoTrabajo(respuesta[1]);
			if (respuesta) {
				$("#alerta_solicitud").css('display','none');
				$('#tipos_trabajo').hide();
				$('#notaExistencia').append('<div class="text-center row">'+
					'<p class="alert alert-info col-12">Ya existe un protocolo de '+valor+' en borrador. Si desea iniciar con una '+
					'nueva modalidad de trabajo escrito, deberá <strong>ELIMINAR</strong> el actual borrador.<br>'+
					'<button class="btn btn-success col-sm-12 col-md-2" id="editarProtocolo">Editar</button>'+
					'<button class="btn btn-danger col-sm-12 offset-md-3 col-md-2" id="eliminarProtocolo">Eliminar</button></div>');
			}else
				obtenerTiposDeTrabajo();
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [guardarProtocoloRAC guarda los datos del formulario del protocolo RAC]
 * @param  {[array]} RAC_datos [datos del protocolo RAC para ser guardados]
 * @return {[type]}           [description]
 */
function guardarProtocoloRAC(rac_datos){
	var datos = {
		funcion: 'guardarProtocoloRAC',
		datos:rac_datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.aplicacion.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Se han guardado los datos con éxito.');
				cerrarModal('guardarModal');	
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [convertirJSONTabla convierte un json a la tabla del cronograma]
 * @param  {[text]} cronograma [id del objeto JSON]
 * @return {[type]}            [description]
 */
function convertirJSONTabla(cronograma){
	let datos_cronograma = JSON.parse(cronograma);
	datos_cronograma.forEach( function(e, i) {
		$("#contenido-actividad").append("<tr id='actividad"+e['#']+
	 	"'><td><button type='button' class='close btn-danger' data-dismiss='alert' value='"+e['#']+
	 	"'>&times;</button></td><td name='numero' class='no_act'>"+e['#']+"</td><td name='nombre"+e['#']+
	 	"' class='nombre_act'>"+e['Actividad']+"</td>");
	 	for (var i = 24; i > 0; i--) {
	 		if (e[i] != '') 
	 			$('td[name=nombre'+e['#']+']').after("<td name='m'"+i+" class='bg-success seleccionado mes'>"+e[i]+"</td>");
	 		else
	 			$('td[name=nombre'+e['#']+']').after("<td name='m'"+i+" class='mes'>"+e[i]+"</td>");
		}
		$('td[name=m4]').after("</tr>");
	});
}

/**
 * [convertirTablaJSON convierte una tabla html a archivo json]
 * @param  {[text]} cronograma [id del objeto del DOM que pertenece al cronograma]
 * @return {[json]}            [tabla en formato JSON]
 */
function convertirTablaJSON(cronograma){
	var tabla = $(cronograma).tableToJSON(); // Convert the table into a javascript object
	console.log(tabla);
	return tabla;
}
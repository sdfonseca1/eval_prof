'use strict';
$(document).ready(function() {

	$('#btnGuardarProtocoloModal').hide();

	obtenerProtocoloTSN();

	//Guardar protocolo TSN validación.
	$(document).on('click', '#btnGuardarProtocoloModal', function(event) {
		event.preventDefault();
		let d = obtenerDatosTSN();
		if (d.asesor != '' && d.area != '')
			mostrarModal('guardarModal');
		else
			mostrarModalAlerta('error', 'Para guardar el protocolo debe seleccionar obligatoriamente Área Académica, Asesor y Coasesor');
	});

	//Guardar datos del protocolo TSN YA VALIDADOS
	$(document).on('click', '#btnGuardarProtocoloTSN', function(event) {
		let d = obtenerDatosTSN();
		guardarProtocoloTSN(d);
	});

	// Botón actualizar protocolo
	$('#btnActualizarProtocoloTSN').click(function (event){
		let tsn = obtenerDatosTSN();
		actualizarProtocoloTSN(tsn);
	});

	// Botón registrar solicitud
	$(document).on('click', '#btnRegistrarSolicitud', function (event){
		let tsn = obtenerDatosTSN();
		if (datosLlenosTSN(tsn))
			mostrarModal('registroModal');
		else
			mostrarModalAlerta('error', 'Para registrar la solictud TODOS los campos deben estar llenos.');
	})

	// Registrar protocolo confirmación del modal
	$(document).on('click', '#btnRegistrarProtocoloTSN', function(event) {
		let tsn = obtenerDatosTSN();
		registrarProtocoloTSN(tsn);
	});

});

/**
 * [registrarProtocoloaTSN registra los datos del protocolo TSN]
 * @param  {[array]} datos [datos del formulario TSN]
 * @return {[type]}       [description]
 */
function registrarProtocoloTSN(datos){
	var datos = {
		funcion: 'actualizarProtocoloTSN',
		protocolo: datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.tesina.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MTSNAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Se han guardado los datos con éxito.');
				cerrarModal('actualizarModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [cambioEstadoSolicitud cambia el estado del protocolo temporal a Solicitud registrada (1)]
 * @return {[type]} [description]
 */
function cambioEstadoSolicitud(){
	var datos = {
		funcion: 'cambioSolicitudProtocoloTSN'
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.tesina.php",
		data: datos,
		dataType: 'text',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MTSNAJE: '+respuesta['ERR_MSG']);
			else{
				cerrarModal('registroModal');
				mostrarModalAlerta('exito', 'Solicitud registrada correctamente. Puede consultar el estado'+
					' en la sección "Estado del proceso de evaluación de mi protocolo".\nLa página será recargada de nuevo en 5 segundos.');
				setTimeout("location.reload(true);", 5000);
			}	
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [datosLlenosTSN verifica si todos los campos del protocolo están llenos]
 * @param  {[arrray]} datos [arreglo de datos del formulario TSN]
 * @return {[boolean]}       [falso o verdadero]
 */
function datosLlenosTSN(datos){
	if (datos['titulo'] != ''  && datos['asesor'] != '' && datos['area'] != '' && 
		datos['planteamiento'] != '' && datos['justificacion'] != '' && datos['objetivo'] != '' && 
		datos['metodologia'] != '' && datos['referencias'] != '' && datos['cronograma'] != '')
		return true;
	else
		return false;
}

/**
 * [actualizarProtocoloTSN actualiza los campos del protocolo]
 * @param  {[array]} datos [nuevos datos del formulario del protocolo TSN]
 * @return {[type]}       [description]
 */
function actualizarProtocoloTSN(datos){
	var datos = {
		funcion: 'actualizarProtocoloTSN',
		protocolo: datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.tesina.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MTSNAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Datos del protocolo actualizados correctamente.');
				cerrarModal('actualizarModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [llenarProtocoloTSN llena el formulario del protocolo TSN con los datos guardados de la base de datos]
 * @param  {[array]} datos [datos obtenidos de la base de datos]
 * @return {[type]}       [description]
 */
function llenarProtocoloTSN(datos){
	$("#formGroupTituloTSN").val(datos['titulo']);
	$('#formGroupAsesor').val(datos['asesor']);
	$("#formGroupAreaAcademica").val(datos['areaAcademica']);
	$("#formGroupPlanteamientoTSN").val(datos['planteamientoInv'])
	$('#formGroupJustificacionTSN').val(datos['justificacion'])
	$('#formGroupObjetivoGeneralTSN').val(datos['objetivoGeneral'])
	$('#formGroupMetodologiaTSN').val(datos['metodologia'])	
	$("#formGroupReferenciasTSN").val(datos['referencias']);
	if (datos['cronograma'] != 'null')
		convertirJSONTabla(datos['cronograma']);
	$('#btnGuardarProtocoloModal').hide();
}

/**
 * [obtenerDatosTSN obtiene los valores del formularo TSN]
 * @return {[array]} datos_TSN [obtiene los datos del formulario TSN sin validar]
 */
function obtenerDatosTSN(){
	var datos_tsn = {
		titulo: $("#formGroupTemaTSN").val(),
		asesor: $("#formGroupAsesor").val(),
		area: $("#formGroupAreaAcademica").val(),
		planteamiento: $("#formGroupPlanteamientoTSN").val(),
		justificacion: $("#formGroupJustificacionTSN").val(),
		objetivo: $("#formGroupObjetivoGeneralTSN").val(),
		metodologia: $("#formGroupMetodologiaTSN").val(),
		referencias: $("#formGroupReferenciasTSN").val(),
		cronograma: convertirTablaJSON("#cronograma-tsn"),
		completo: ''
	};
	return datos_tsn;
}

/**
 * [obtenerProtocoloTSN obtiene los campos del protocolo TSN de la base de datos]
 * @return {[type]} [description]
 */
function obtenerProtocoloTSN(){
	var dato = {
		funcion: 'obtenerProtocoloTSN'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.tesina.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			console.log(respuesta);
			if (respuesta.length > 0)
				llenarProtocoloTSN(respuesta[0]);
			else{
				$('#btnGuardarProtocoloModal').show();
				$('#btnActualizarProtocoloModal').hide();
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			// console.log('protoclo tsn')
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [existeBorradorProtocolo verifica si ya existe una solicitud guardado en la base de datos]
 * @return {[type]} [description]
 */
function existeBorradorProtocolo(){
	var dato = {
		funcion: 'existeBorradorProtocolo'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.tesina.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			var valor = tipoTrabajo(respuesta[1]);
			if (respuesta) {
				$("#alerta_solicitud").css('display','none');
				$('#tipos_trabajo').hide();
				$('#notaExistencia').append('<div class="text-center row">'+
					'<p class="alert alert-info col-12">Ya existe un protocolo de '+valor+' en borrador. Si desea iniciar con una '+
					'nueva modalidad de trabajo escrito, deberá <strong>ELIMINAR</strong> el actual borrador.<br>'+
					'<button class="btn btn-success col-sm-12 col-md-2" id="editarProtocolo">Editar</button>'+
					'<button class="btn btn-danger col-sm-12 offset-md-3 col-md-2" id="eliminarProtocolo">Eliminar</button></div>');
			}else
				obtenerTiposDeTrabajo();
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [guardarProtocoloTSN guarda los datos del formulario del protocolo TSN]
 * @param  {[array]} TSN_datos [datos del protocolo TSN para ser guardados]
 * @return {[type]}           [description]
 */
function guardarProtocoloTSN(TSN_datos){
	var datos = {
		funcion: 'guardarProtocoloTSN',
		datos:TSN_datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.tesina.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MTSNAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Se han guardado los datos con éxito.');
				cerrarModal('guardarModal');	
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [convertirJSONTabla convierte un json a la tabla del cronograma]
 * @param  {[text]} cronograma [id del objeto JSON]
 * @return {[type]}            [description]
 */
function convertirJSONTabla(cronograma){
	let datos_cronograma = JSON.parse(cronograma);
	datos_cronograma.forEach( function(e, i) {
		$("#contenido-actividad").append("<tr id='actividad"+e['#']+
	 	"'><td><button type='button' class='close btn-danger' data-dismiss='alert' value='"+e['#']+
	 	"'>&times;</button></td><td name='numero' class='no_act'>"+e['#']+"</td><td name='nombre"+e['#']+
	 	"' class='nombre_act'>"+e['Actividad']+"</td>");
	 	for (var i = 24; i > 0; i--) {
	 		if (e[i] != '') 
	 			$('td[name=nombre'+e['#']+']').after("<td name='m'"+i+" class='bg-success seleccionado mes'>"+e[i]+"</td>");
	 		else
	 			$('td[name=nombre'+e['#']+']').after("<td name='m'"+i+" class='mes'>"+e[i]+"</td>");
		}
		$('td[name=m4]').after("</tr>");
	});
}

/**
 * [convertirTablaJSON convierte una tabla html a archivo json]
 * @param  {[text]} cronograma [id del objeto del DOM que pertenece al cronograma]
 * @return {[json]}            [tabla en formato JSON]
 */
function convertirTablaJSON(cronograma){
	var tabla = $(cronograma).tableToJSON(); // Convert the table into a javascript object
	return tabla;
}
'use strict';
$(document).ready(function() {

	$('#btnGuardarProtocoloModal').hide();

	obtenerProtocoloRIN();

	//Guardar protocolo RIN validación.
	$(document).on('click', '#btnGuardarProtocoloModal', function(event) {
		event.preventDefault();
		let d = obtenerDatosRIN();
		if (d.asesor != '' && d.area != '')
			mostrarModal('guardarModal');
		else
			mostrarModalAlerta('error', 'Para guardar el protocolo debe seleccionar obligatoriamente Asesor y Área Académica');
	});

	//Guardar datos del protocolo RIN YA VALIDADOS
	$(document).on('click', '#btnGuardarProtocoloRIN', function(event) {
		let d = obtenerDatosRIN();
		guardarProtocoloRIN(d);
	});

	// Botón actualizar protocolo
	$('#btnActualizarProtocoloRIN').click(function (event){
		let rin = obtenerDatosRIN();
		actualizarProtocoloRIN(rin);
	});

	// Botón registrar solicitud
	$(document).on('click', '#btnRegistrarSolicitud', function (event){
		let rin = obtenerDatosRIN();
		if (datosLlenosRIN(rin))
			mostrarModal('registroModal');
		else
			mostrarModalAlerta('error', 'Para registrar la solictud TODOS los campos deben estar llenos.');
	})

	// Registrar protocolo confirmación del modal
	$(document).on('click', '#btnRegistrarProtocoloRIN', function(event) {
		let rin = obtenerDatosRIN();
		registrarProtocoloRIN(rin);
	});

});

/**
 * [registrarProtocoloRIN registra los datos del protocolo RIN]
 * @param  {[array]} datos [datos del formulario RIN]
 * @return {[type]}       [description]
 */
function registrarProtocoloRIN(datos){
	var datos = {
		funcion: 'actualizarProtocoloRIN',
		protocolo: datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.residencia.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Se han guardado los datos con éxito.');
				cerrarModal('actualizarModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [cambioEstadoSolicitud cambia el estado del protocolo temporal a Solicitud registrada (1)]
 * @return {[type]} [description]
 */
function cambioEstadoSolicitud(){
	var datos = {
		funcion: 'cambioSolicitudProtocoloRIN'
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.residencia.php",
		data: datos,
		dataType: 'text',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				cerrarModal('registroModal');
				mostrarModalAlerta('exito', 'Solicitud registrada correctamente. Puede consultar el estado'+
					' en la sección "Estado del proceso de evaluación de mi protocolo".\nLa página será recargada de nuevo en 5 segundos.');
				setTimeout("location.reload(true);", 5000);
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [datosLlenosRIN verifica si todos los campos del protocolo están llenos]
 * @param  {[arrray]} datos [arreglo de datos del formulario RIN]
 * @return {[boolean]}       [falso o verdadero]
 */
function datosLlenosRIN(datos){
	if (datos['fecha_ini'] != '' && datos['fecha_fin'] != '' && datos['investigador'] != '' &&
		datos['asesor'] != '' && datos['coasesor'] != '' && datos['area'] != '' && datos['titulo'] != '' &&
		datos['cve_proy'] != '' && datos['descripcion'] != '' && datos['reporte'] != '' &&
		datos['objetivo'] != '' && datos['referencias'] != '' && datos['cronograma'] != '')
		return true;
	else
		return false;
}

/**
 * [actualizarProtocoloRIN actualiza los campos del protocolo]
 * @param  {[array]} datos [nuevos datos del formulario del protocolo RIN]
 * @return {[type]}       [description]
 */
function actualizarProtocoloRIN(datos){
	var datos = {
		funcion: 'actualizarProtocoloRIN',
		protocolo: datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.residencia.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito', 'Datos del protocolo actualizados correctamente.');
				cerrarModal('actualizarModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [llenarProtocoloRIN llena el formulario del protocolo RIN con los datos guardados de la base de datos]
 * @param  {[array]} datos [datos obtenidos de la base de datos]
 * @return {[type]}       [description]
 */
function llenarProtocoloRIN(datos){
	$("#formGroupPeriodoInicioRIN").val(datos['periodoInicio']);
	$("#formGroupPeriodoFinRIN").val(datos['periodoTermino']);
	$('#formGroupInvestigadorRIN').val(datos['nombreProfInves']);
	$("#formGroupAsesor").val(datos['asesor']);
	$("#formGroupCosesor").val(datos['coasesor']);
	$("#formGroupAreaAcademica").val(datos['areaAcademica']);
	$("#formGroupTituloProyectoRIN").val(datos['tituloProy']);
	$('#formGroupClaveProyectoRIN').val(datos['claveRegProy']);
	$("#formGroupDescripcionRIN").val(datos['descripcion']);
	$("#formGroupTituloReporteRIN").val(datos['tituloRep']);
	$("#formGroupObjetivoGeneralRIN").val(datos['objetivoGeneral']);
	if (datos['cronograma'] != 'null')
		convertirJSONTabla(datos['cronograma']);
	$('#btnGuardarProtocoloModal').hide();
}

/**
 * [obtenerProtocoloRIN obtiene los campos del protocolo RIN de la base de datos]
 * @return {[type]} [description]
 */
function obtenerProtocoloRIN(){
	var dato = {
		funcion: 'obtenerProtocoloRIN'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.residencia.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.length > 0)
				llenarProtocoloRIN(respuesta[0]);
			else{
				$('#btnGuardarProtocoloModal').show();
				$('#btnActualizarProtocoloModal').hide();
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			// console.log('obtenerProtocoloRIN');
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [obtenerDatosRIN obtiene los valores del formularo RIN]
 * @return {[array]} datos_RIN [obtiene los datos del formulario RIN sin validar]
 */
function obtenerDatosRIN(){
	var datos_rin = {
		fecha_ini: $("#formGroupPeriodoInicioRIN").val(),
		fecha_fin: $("#formGroupPeriodoFinRIN").val(),
		investigador: $('#formGroupInvestigadorRIN').val(),
		asesor: $("#formGroupAsesor").val(),
		coasesor: $("#formGroupCosesor").val(),
		area: $("#formGroupAreaAcademica").val(),
		titulo: $("#formGroupTituloProyectoRIN").val(),
		cve_proy: $('#formGroupClaveProyectoRIN').val(),
		descripcion: $("#formGroupDescripcionRIN").val(),
		reporte: $("#formGroupTituloReporteRIN").val(),
		objetivo: $("#formGroupObjetivoGeneralRIN").val(),
		referencias: $("#formGroupReferenciasRIN").val(),
		cronograma: convertirTablaJSON("#cronograma-rin"),
		completo: ''
	};
	return datos_rin;
}

/**
 * [existeBorradorProtocolo verifica si ya existe una solicitud guardado en la base de datos]
 * @return {[type]} [description]
 */
function existeBorradorProtocolo(){
	var dato = {
		funcion: 'existeBorradorProtocolo'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.residencia.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			var valor = tipoTrabajo(respuesta[1]);
			if (respuesta) {
				$("#alerta_solicitud").css('display','none');
				$('#tipos_trabajo').hide();
				$('#notaExistencia').append('<div class="text-center row">'+
					'<p class="alert alert-info col-12">Ya existe un protocolo de '+valor+' en borrador. Si desea iniciar con una '+
					'nueva modalidad de trabajo escrito, deberá <strong>ELIMINAR</strong> el actual borrador.<br>'+
					'<button class="btn btn-success col-sm-12 col-md-2" id="editarProtocolo">Editar</button>'+
					'<button class="btn btn-danger col-sm-12 offset-md-3 col-md-2" id="eliminarProtocolo">Eliminar</button></div>');
			}else
				obtenerTiposDeTrabajo();
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [guardarProtocoloRIN guarda los datos del formulario del protocolo RIN]
 * @param  {[array]} RIN_datos [datos del protocolo RIN para ser guardados]
 * @return {[type]}           [description]
 */
function guardarProtocoloRIN(rin_datos){
	var datos = {
		funcion: 'guardarProtocoloRIN',
		datos:rin_datos
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/modalidades/ajax.residencia.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else {
				mostrarModalAlerta('exito', 'Se han guardado los datos con éxito.');
				cerrarModal('guardarModal');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [convertirJSONTabla convierte un json a la tabla del cronograma]
 * @param  {[text]} cronograma [id del objeto JSON]
 * @return {[type]}            [description]
 */
function convertirJSONTabla(cronograma){
	let datos_cronograma = JSON.parse(cronograma);
	datos_cronograma.forEach( function(e, i) {
		$("#contenido-actividad").append("<tr id='actividad"+e['#']+
	 	"'><td><button type='button' class='close btn-danger' data-dismiss='alert' value='"+e['#']+
	 	"'>&times;</button></td><td name='numero' class='no_act'>"+e['#']+"</td><td name='nombre"+e['#']+
	 	"' class='nombre_act'>"+e['Actividad']+"</td>");
	 	for (var i = 24; i > 0; i--) {
	 		if (e[i] != '') 
	 			$('td[name=nombre'+e['#']+']').after("<td name='m'"+i+" class='bg-success seleccionado mes'>"+e[i]+"</td>");
	 		else
	 			$('td[name=nombre'+e['#']+']').after("<td name='m'"+i+" class='mes'>"+e[i]+"</td>");
		}
		$('td[name=m4]').after("</tr>");
	});
}

/**
 * [convertirTablaJSON convierte una tabla html a archivo json]
 * @param  {[text]} cronograma [id del objeto del DOM que pertenece al cronograma]
 * @return {[json]}            [tabla en formato JSON]
 */
function convertirTablaJSON(cronograma){
	var tabla = $(cronograma).tableToJSON();
	return tabla;
}
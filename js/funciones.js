'use strict';

$(document).ready(function(){

/********** FUNCIONES GLOBALES **********/

	$('.btnModal').click(function (){
		limpiarContenido('msg-alerta');
	});

	// Verifica si el número de cuenta ya está registrado
	$("#pasanteClave").blur(function(){
		var clave = $(this).val();
		buscarClavePasante(clave);
	});

	// Botón ingreso pasante
	$('#btnIngresoPasante').click(function (e){
		e.preventDefault();
		let usuario = $('#clavePasante').val();
		let pass = $('#contrasenaPasante').val();
		if (usuario != '' && pass != '') {
			var datos = {
				funcion: 'ingresoPasante',
				usuario: usuario,
				contrasena:pass
			};
			ingresoPasante(datos);
		}else{
			mostrarModalAlerta('error', 'Todos los campos deben estar llenos.');
		}
	});

	// Botón ingreso coordinador
	$('#btnIngresoCoordinador').click(function (e){
		e.preventDefault();
		let usuario = $('#claveCoordinador').val();
		let pass = $('#contrasenaCoordinador').val();
		if (usuario != '' && pass != '') {
			var datos = {
				funcion: 'ingresoCoordinador',
				usuario: usuario,
				contrasena:pass
			};
			ingresoCoordinador(datos);
		}else{
			mostrarModalAlerta('error', 'Todos los campos deben estar llenos.');
		}
	});

	// Botón ingreso titulación
	$('#btnIngresoTitulacion').click(function (e){
		e.preventDefault();
		let usuario = $('#claveTitulacion').val();
		let pass = $('#contrasenaTitulacion').val();
		if (usuario != '' && pass != '') {
			var datos = {
				funcion: 'ingresoTitulacion',
				usuario: usuario,
				contrasena:pass
			};
			ingresoTitulacion(datos);
		}else{
			mostrarModalAlerta('error', 'Todos los campos deben estar llenos.');
		}
	});

	//Obtener datos registrados
	obtenerConfiguracion();

});

function obtenerConfiguracion(){

}

/**
 * [ingresoPasante recupera clave y contraseña del pasante para saber si está registrado. Si es verdadero,
 * lo redirige a la paǵina de inicio]
 * @param  {[object]} datos [clave y contraseña del pasante que intenta ingresar]
 * @return {[type]}       [description]
 */
function ingresoPasante(datos){
	$.ajax({
		type: 'POST',
		url: "vista/ajax/pasante/pasante.ajax.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			console.log(respuesta);
			if (respuesta == 1){
				history.pushState(null, "", "index.php?accion=pasante");
				location.reload();
			}else if (respuesta == 0)
				mostrarModalAlerta('info', 'Aún no puedes ingresar al sistema, deberás esperar a que tu coordinador de carrera apruebe tu solicitud en no más de 48 Hrs desde tu registro.');
			else
				mostrarModalAlerta('error', 'Usuario y/o contraseña incorrecto. Intente con datos diferentes.');
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});	
}

/**
 * [ingresoCoordinador recupera clave y contraseña del coordinador para saber si está registrado. Si es verdadero,
 * lo redirige a la paǵina de inicio]
 * @param  {[array]} datos [clave y contraseña del coordinador que intenta ingresar]
 * @return {[type]}       [description]
 */
function ingresoCoordinador(datos){
	$.ajax({
		type: 'POST',
		url: "vista/ajax/coordinador/coordinador.ajax.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta == 1){
				history.pushState(null, "", "index.php?accion=coordinador");
				location.reload();
			}else
				mostrarModalAlerta('error', 'Usuario y/o contraseña incorrecto. Asegure que sus datos son correctos.');
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": "+ errorThrown);
		}
	});	
}

/**
 * [ingresoTitulacion recupera clave y contraseña del depto. titulacion para saber si está registrado. Si es verdadero,
 * lo redirige a la paǵina de inicio]
 * @param  {[array]} datos [clave y contraseña del depto. titulacion que intenta ingresar]
 * @return {[type]}       [description]
 */
function ingresoTitulacion(datos){
	$.ajax({
		type: 'POST',
		url: "vista/ajax/titulacion/titulacion.ajax.php",
		data: datos,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta == 1){
				history.pushState(null, "", "index.php?accion=titulacion");
				location.reload();
			}else
				mostrarModalAlerta('error', 'Usuario y/o contraseña incorrecto. Asegure que sus datos son correctos.');
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": "+ errorThrown);
		}
	});	
}

/**
 * [buscarClavePasante muestra mensaje/bloquea boton registrar si la clave ya existe, caso contrario no]
 * @param  {[text]} clave [la clave del supuesto pasante]
 * @return {[type]}       []
 */
function buscarClavePasante(clave){
	var datos = {
		funcion: 'buscarClavePasante',
		clave: clave,
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/pasante/pasante.ajax.php",
		data: datos,
		dataType: 'json',
		encode: false,
		success: function(respuesta){
			if (respuesta) {
				$("#clave_existente").css('display','block');
				deshabilitarElementoID('btnRegistrarPasante');
			}else{
				$("#clave_existente").css('display','none');
				habilitarElementoID('btnRegistrarPasante');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": "+ errorThrown);
		}
	});
}

/********************** FUNCIONES GLOBALES GENERALES **********************/

/**
 * [limpiarFormulario limpia los datos de un formulario dado su id]
 * @param  {[text]} nombre_formulario [id del formulario]
 * @return {[type]}                   [description]
 */
function limpiarFormulario(nombre_formulario) {
	$('#'+nombre_formulario)[0].reset();
}

/**
 * [mostrarModalAlerta mostrar modal de acción exitosa]
 * @param  {[text} tipo  [tipo de mensaje: error, info, exito]
 * @param  {[text} texto [mensaje que será desplegado]
 */
function mostrarModalAlerta(tipo, texto){
	$('#texto_mensaje_'+tipo).append(texto);
	$("#alerta_"+tipo).modal("show");
}

/**
 * [cerrarModal cierra un modal, identificado por id]
 * @param  {[text]} id [id del modal a cerrar]
 * @return {[type]}    [description]
 */
function cerrarModal(id){
	$('#'+id).modal('hide');
}

/**
 * [mostrarModal muestra un modal, identificado por id]
 * @param  {[ttext]} id [id del modal a mostrar]
 * @return {[type]}    [description]
 */
function mostrarModal(id){
	$('#'+id).modal('show');
}

/**
 * [limpiarContenido vacia el contenido de un elemento por su id]
 * @param  {[text]} id [id del objeto html]
 * @return {[type]}    [description]
 */
function limpiarContenido(clase){
	$('.'+clase).empty();
}

/**
 * [deshabilitarElementoID deshabilita un elemento por id]
 * @param  {[text]} id [id del objeto a deshabilitar]
 * @return {[type]}       [description]
 */
function deshabilitarElementoID(id){
	$("#"+id).attr('disabled', '');
}

/**
 * [habilitarElementoID habilita un elemento por id]
 * @param  {[text]} id [id del objeto a habilitar]
 * @return {[type]}       [description]
 */
function habilitarElementoID(id){
	$("#"+id).removeAttr('disabled', '');
}
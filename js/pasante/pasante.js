'use strict';

var trabajo = '';

$(document).ready(function (){

	//Variables
	var no_actividad = 0;

	licenciaturas();

	//Div de la escuela
	$(document).on('change','#pasanteLicenciatura',function () {
		console.log('click')
		mostrarEscuelaProcedencia();
	});

	

	escuelas();

	//Muestra el nombre del pasante en la página de inicio.
	mostrarNombrePasante();

	//Click al accordeon mis datos pasante.
	$("a#misDatosPasante").one("click",function(){
		obtenerMisDatosPasante();
	});

	//Editar mis datos
	$('#btnEditarDatos').click(function () {
		mostrarDatosAEdicion();
	});

	//Limpiar datos de modal editar datos
	$('#btnCerrarModalDatos').click(function () {
		limpiarFormulario('cambio_datos');
	});

	//Botón guardar datos del modal editar datos
	$('#bntActualizarDatos').click(function (){
		let nombre = $('#cambioNombre').val();
		let paterno = $('#cambioApellidoPaterno').val();
		let materno = $('#cambioApellidoMaterno').val();
		let fecha = $('#cambioFechaNacimiento').val();
		let telefono = $('#cambioTelefono').val();
		let correo = $('#cambioCorreo').val();
		if (nombre != '' && paterno != '' && materno != '' && paterno != '' && fecha != '' && telefono != '' && correo != '') {
			var datos = {
				funcion: 'actualizarDatosPasante',
				nombre: nombre,
				ape_pat: paterno,
				ape_mat: materno,
				fecha: fecha,
				tel: telefono,
				correo: correo
			};
			actualizarDatosPasante(datos);
		}else{
			mostrarModalAlerta('error', 'Todos los campos deben estar llenos.');
		}
	});

	//Botón Actualizar Contraseña
	$('#btnActualizaContrasena').click(function (){
		let pass1 = $('#cambioContrasena1').val();
		let pass2 = $('#cambioContrasena2').val();
		if (pass1 != pass2)
			mostrarModalAlerta('error', 'Las contraseñas no coinciden. Asegúrese de escribir la misma contraseña en ambos campos.');
		else
			actualizarContrasena(pass1);
	});

	//Verifica si ya existe un borrador de solicitud en la base de datos.
	$("a#solicitudPasante").one("click",function(){
		solicitudRegistrada();
	});

	// Sección estado de proceso
	$("a#estadoSolicitud").one("click", function(){
		etapaProceso();
	});

	// ENLACES DE TIPO DE TRABAJOS
	$('#tipos_trabajo').change(function(event) {
		let tipo_trabajo = $(this).val();
		if (tipo_trabajo != ''){
			cargarTipoTrabajo(tipo_trabajo);
			no_actividad = 0;
		}
	});

	//Botón agregar actividad al cronograma
	$(document).on('click', 'a.btnAgregarActividad', function(event) {
		event.preventDefault();
		var campo_act = $('#campoActividad').val();
		if (campo_act != "") {
			$("#alertaActividad").css('display','none');
			no_actividad++;
			$("#contenido-actividad").append("<tr id='actividad"+no_actividad+
	 		"'><td><button type='button' class='close btn-danger' data-dismiss='alert' value='"+no_actividad+
	 		"'>&times;</button></td><td name='numero' class='no_act'>"+no_actividad+"</td><td name='nombre"+
	 		no_actividad+"' class='nombre_act'>"+campo_act+"</td>"+
	 		"<td name='m1' class='mes'></td><td name='m2' class='mes'></td><td name='m3' class='mes'></td>"+
	 		"<td name='m4' class='mes'></td>"+"<td name='m5' class='mes'></td><td name='m6' class='mes'></td>"+
	 		"<td name='m7' class='mes'></td><td name='m8' class='mes'></td><td name='m9' class='mes'></td>"+
	 		"<td name='m10' class='mes'></td><td name='m11' class='mes'></td><td name='m12' class='mes'></td>"+
	 		"<td name='m13' class='mes'></td><td name='m14' class='mes'></td><td name='m15' class='mes'></td>"+
	 		"<td name='m16' class='mes'></td><td name='m17' class='mes'></td><td name='m18' class='mes'></td>"+
	 		"<td name='m19' class='mes'></td><td name='m20' class='mes'></td><td name='m21' class='mes'></td>"+
	 		"<td name='m22' class='mes'></td><td name='m23' class='mes'></td><td name='m24' class='mes'></td>");
			$('#campoActividad').val('');
		}else{
			$("#alertaActividad").css('display','block');
		}	 	
	});

	//Eliminar una actividad del cronograma.
	$(document).on('click', 'button.close', function(event) {
		$(this).closest('tr').remove();
		no_actividad--;
	});

	// Colorear los td de una tabla al hacer click.
	$(document).on('click', 'td.mes', function(event) {
		var tiene_clase = $(this).hasClass('seleccionado');
		if (tiene_clase) {
			$(this).removeClass('bg-success seleccionado');
			$(this).text('');
		}else{
			$(this).attr('class', 'bg-success seleccionado mes');
			$(this).text('X').css("color","white");
		}
	});

	// Editar Protocolo
	$(document).on('click', '#editarProtocolo', function() {
		$('#notaExistencia').hide();
		cargarTipoTrabajo($(this).val());
	});

	// Botón registrar pasante
	$('#btnRegistrarPasante').click(function(event) {
		event.preventDefault();
		let r = obtenerDatosRegistroPasante();
		if (r.cuenta != '' && r.nombre != '' && r.ape_pat != '' && r.ape_mat != '' &&
		    r.fecha_nac != '' && r.licenciatura != '' && r.escuela != '' && r.telefono != '' &&
		    r.correo != '' && r.contrasena != '' && r.contrasena2 != ''){
			if (r.contrasena != r.contrasena2)
				mostrarModalAlerta('error', 'Las contraseñas no coinciden. Asegúrese de escribir la misma contraseña en ambos campos.');
			else{
				registrarPasante(r);
			}

		}else
			mostrarModalAlerta('error', 'Todos los campos deben estar llenos.');
	});

	// Eliminar borrador protocolo modal
	$(document).on('click', '#eliminarProtocoloModal', function(event) {
		// console.log($('#btnEliminarProtocolo').val());
		mostrarModal('eliminarModal');

	});

	// Elinar protocolo borrador aem
	$('#btnEliminarProtocolo').click(function(event) {
		// console.log($('#editarProtocolo').val());
		eliminarBorradorProtocolo($('#editarProtocolo').val());
	});	

});

/**
 * [escuelas muesta las escuelas]
 * @return {[type]} [description]
 */
function escuelas(){
	var datos = {
		funcion: 'obtenerEscuelas'
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/pasante/pasante.ajax.php",
		data: datos,
		dataType: 'json',
		encode: false,
		success: function(respuesta){
			if (respuesta!= null) {
				respuesta.forEach(function (e){
					$("#pasanteEscuela").append('<option value="'+e[0]+'">'+e[0]+' - '+e[1]+'</option>')
				});
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": "+ errorThrown);
		}
	});
}

/**
 * [licenciaturas muesta las licenciaturas]
 * @return {[type]} [description]
 */
function licenciaturas(){
	var datos = {
		funcion: 'obtenerLicenciaturas'
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/pasante/pasante.ajax.php",
		data: datos,
		dataType: 'json',
		encode: false,
		success: function(respuesta){
			if (respuesta!= null) {
				respuesta.forEach(function (e){
					$("#pasanteLicenciatura").append('<option value="'+e[0]+'">'+e[0]+' - '+e[1]+'</option>')
				});
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": "+ errorThrown);
		}
	});
}

/**
 * [etapaProceso obtiene la etapa del proceso de evaluación de protocolos para el pasante]
 * @return {[type]} [description]
 */
function etapaProceso(){
	var dato = {
		funcion: 'etapaProceso',
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/pasante/pasante.ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else if (Object.keys(respuesta).length === 0) {
				ProgressBar.init(
					[ 'Solicitud','Registro', 'Asignación de Evaluación', 'Evaluación', 'Dictamen'],
					'1',
					'progress-bar-wrapper'
				);
			}else{
				switch(respuesta[0]['estadoProtocolo']){
					case '1':
						ProgressBar.init(
				  			[ 'Solicitud','Registro', 'Asignación de Evaluación', 'Evaluación', 'Dictamen'],
							'Solicitud',
							'progress-bar-wrapper'
						);
						break;
					case '2':
						ProgressBar.init(
				  			[ 'Solicitud','Registro', 'Asignación de Evaluación', 'Evaluación', 'Dictamen'],
							'Registro',
							'progress-bar-wrapper'
						);
						break;
					case '3':
						ProgressBar.init(
				  			[ 'Solicitud','Registro', 'Asignación de Evaluación', 'Evaluación', 'Dictamen'],
							'Asignación de Evaluación',
							'progress-bar-wrapper'
						);
						break;
					case '4':
						ProgressBar.init(
				  			[ 'Solicitud','Registro', 'Asignación de Evaluación', 'Evaluación', 'Dictamen'],
							'Evaluación',
							'progress-bar-wrapper'
						);
						break;
					case '5':
						ProgressBar.init(
				  			[ 'Solicitud','Registro', 'Asignación de Evaluación', 'Evaluación', 'Dictamen'],
							'Dictamen',
							'progress-bar-wrapper'
						);
						break;
					default:
						ProgressBar.init(
				  			[ 'Solicitud','Registro', 'Asignación de Evaluación', 'Evaluación', 'Dictamen'],
							'',
							'progress-bar-wrapper'
						);
						break;

				}
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [eliminarBorradorProtocolo elimina el borrador de un protocolo que pertenece al pasante]
 * @return {[type]} [description]
 */
function eliminarBorradorProtocolo(trabajo){
	var dato = {
		funcion: 'eliminarBorradorProtocolo',
		trabajo: trabajo
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else {
				cerrarModal('eliminarModal');
				mostrarModalAlerta('exito', 'Borrador del protocolo eliminado correctamente.');
				setTimeout("location.reload(true);", 3000);
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [solicitudRegistrada verifica si el pasante tiene una solicitud ya registrada]
 * @return {[type]} [description]
 */
function solicitudRegistrada(){
	var dato = {
		funcion: 'solicitudRegistrada'
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/pasante/pasante.ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta){
				$('#alerta_solicitud').empty().append('Ya tienes una solicitud en proceso de revisión.'+
				  ' Consulta la sección: Estado del proceso de evaluación de mi protocolo, para más '+
				  'información.');
				$('#notaExistencia').hide();
			}else
				existeBorradorProtocolo();
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [registrarPasante registra los datos del formulario de registro del pasante]
 * @param  {[array]} datos_pasante [datos del pasante para ser registrado]
 * @return {[type]}               [description]
 */
function registrarPasante(datos_pasante){
	var datos = {
		funcion: 'registrarPasante',
		registro: datos_pasante 
	};
	$.ajax({
		type: 'POST',
		url: "vista/ajax/pasante/pasante.ajax.php",
		data: datos,
		dataType: 'json',
		encode: false,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				mostrarModalAlerta('exito','El registro se ha realizado correctamente. En un plazo máximo a 48 Hrs, tu solicitud será aprobada por tu coordinador.');
				cerrarModal('actualizarModal');
				limpiarFormulario('formPasanteRegistro');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [obtenerDatosRegistroPasante obtiene los datos de registro del pasante]
 * @return {[array]} [datos finales para registrar un pasante]
 */
function obtenerDatosRegistroPasante(){
	let pass1 = $('#pasanteContrasena').val();
	let pass2 = $('#pasanteContrasenaRep').val();
	let cuenta = $('#pasanteClave').val();
	let nombre = $('#pasanteNombre').val();
	let paterno = $('#pasanteApePat').val();
	let materno = $('#pasanteApeMat').val();
	let nacimiento = $('#pasanteFechaNacimiento').val();
	let licenciatura = $('#pasanteLicenciatura').val();
	var escuela;
	if (licenciatura == 'ICO')
		escuela = $('#pasanteEscuela').val();
	else
		escuela = 'FI';
	let telefono = $('#pasanteTelefono').val();
	let correo = $('#pasanteCorreo').val();
	var registro = {
		cuenta: cuenta,
		nombre: nombre,
		ape_pat: paterno,
		ape_mat: materno,
		fecha_nac: nacimiento,
		licenciatura: licenciatura,
		escuela: escuela,
		telefono: telefono,
		correo: correo,
		contrasena: pass1,
		contrasena2: pass2
	};
	return registro;
}

/**
 * [existeBorradorProtocolo verifica si ya existe una solicitud guardado en la base de datos]
 * @return {[type]} [description]
 */
function existeBorradorProtocolo(){
	var dato = {
		funcion: 'existeBorradorProtocolo'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/pasante/pasante.ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			var valor = tipoTrabajo(respuesta[1]);
			if (respuesta) {
				$("#alerta_solicitud").css('display','none');
				$('#tipos_trabajo').hide();
				$('#notaExistencia').append('<div class="text-center row">'+
					'<p class="alert alert-info col-12">Ya existe un protocolo de '+valor+' en borrador. Si desea iniciar con una '+
					'nueva modalidad de trabajo escrito, deberá <strong>ELIMINAR</strong> el actual borrador.<br>'+
					'<button class="btn btn-success col-sm-12 col-md-2" id="editarProtocolo" value="'+respuesta[1]+'">Editar</button>'+
					'<button class="btn btn-danger col-sm-12 offset-md-3 col-md-2" id="eliminarProtocoloModal" data-toggle="modal" data-target="eliminarModal">Eliminar</button></div>');
				
			}else{
				obtenerTiposDeTrabajo();
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [actualizarContrasena actualiza la nueva contraseña del pasante]
 * @param  {[type]} contrasena [la nueva contraseña que se le asignará al pasante]
 * @return {[type]}            [description]
 */
function actualizarContrasena(contrasena){
	var dato = {
		funcion: 'actualizarContrasena',
		contrasena: contrasena
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/pasante/pasante.ajax.php",
		data: dato,
		dataType: 'text',
		encode: true,
		success: function(respuesta){
			if (respuesta) {
				cerrarModal('modal_contrasena');
				mostrarModalAlerta('exito', 'Contraseña actualizada corectamente.');
				limpiarContenido('cambio_contraena');
			}else 
				mostrarModalAlerta('error', 'Hubo un error durante la actualización de la contraseña. Intente Nuevamente.');

		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [mostrarNombrePasante muestra el nombre del pasante en la página de inicio]
 * @return {[type]} [description]
 */
function mostrarNombrePasante(){
	var dato = {
		funcion: 'obtenerNombrePasante'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/pasante/pasante.ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			$('#nombre_pasante').append(respuesta['nombre']+" "+respuesta['apellidoPaterno']+" "+respuesta['apellidoMaterno']);
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [actualizarDatosPasante actualiza los datos del pasante y los muestra en pantalla]
 * @param  {[objeto]} datos [datos a actualizar del pasante]
 * @return {[type]}       [description]
 */
function actualizarDatosPasante(datos){
	$.ajax({
		type: 'POST',
		url: "vista/ajax/pasante/pasante.ajax.php",
		data: datos,
		dataType: 'text',
		encode: true,
		success: function(respuesta){
			if (respuesta) {
				cerrarModal('modal_editar_datos');
				mostrarModalAlerta('exito', 'Datos actualizados correctamente');
				limpiarContenido('mdPasante');
				obtenerMisDatosPasante();
			}else{
				mostrarModalAlerta('error', 'Hubo un error durante la actualización. Intente Nuevamente.');
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});	
}

/**
 * [mostrarDatosAEdicion obtiene los datos del pasante y los muestra en el modal de modificación.]
 * @return {[type]} [description]
 */
function mostrarDatosAEdicion() {
	var dato = {
		funcion: 'obtenerMisDatosPasante'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/pasante/pasante.ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			$('#cambioNombre').val(respuesta[0]);
			$('#cambioApellidoPaterno').val(respuesta[1]);
			$('#cambioApellidoMaterno').val(respuesta[2]);
			$('#cambioFechaNacimiento').val(respuesta[9]);
			$('#cambioTelefono').val(respuesta[7]);
			$('#cambioCorreo').val(respuesta[8]);
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [obtenerMisDatosPasante obtiene los datos del pasante y los muestra en pantalla]
 * @return {[type]} [description]
 */
function obtenerMisDatosPasante(){
	var dato = {
		funcion: 'obtenerMisDatosPasante'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/pasante/pasante.ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			$("#mdPasante").append("<p>Nombre: "+respuesta[0]+" "+respuesta[1]+" "+respuesta[2]+" "+"<p>");
			$("#mdPasante").append("<p>Fecha de nacimiento: " + respuesta[9] + "<p>");
			$("#mdPasante").append("<p>Escuela: " + respuesta[3] + " - " + respuesta[4] +"<p>");
			$("#mdPasante").append("<p>Carrera: " + respuesta[5] + " - " + respuesta[6] +"<p>");
			$("#mdPasante").append("<p>Teléfono: " + respuesta[7] + "<p>");
			$("#mdPasante").append("<p>Correo: " + respuesta[8] + "<p>");
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [mostrarEscuelaProcedencia muestra las opciones de escuelas para la licenciatura ICO]
 * @return {[type]} [description]
 */
function mostrarEscuelaProcedencia(){
	var carrera = $('#pasanteLicenciatura').val();
    if (carrera == "ICO")
    	$('#divEscuela').css({'display':'block'});
    else
    	$('#divEscuela').css({'display':'none'});
}

/**
 * [tipoTrabajo obtiene el nombre de la clave del tipo de trabajo]
 * @param  {[text]} trabajo [clave del tipo de trabajo]
 * @return {[type]}         [description]
 */
function tipoTrabajo(trabajo){
	switch(trabajo){
		case 'AEM':
			trabajo = 'AEM';
			return "Reporte de Autoempleo Profesional";
		case 'ART':
			return "Artículo Especializado para Publicar en Revista Indexada";
		case 'ENS':
			return "Ensayo";
		case 'MEL':
			return "Memoria de Experiencia Laboral";
		case 'RAC':
			return "Reporte de Aplicación de Conocimientos";
		case 'RIN':
			return "Reporte de Residencia de Investigación";
		case 'TSN':
			return "Tesina";
		case 'TSS':
			return "Tesis";
	}
}

/**
 * [obtenerTiposDeTrabajo muestra los tipos de trabajo escrito para realizar un protocolo]
 * @return {[type]} [description]
 */
function obtenerTiposDeTrabajo(){
	var dato = {
		funcion: 'obtenerTiposDeTrabajo',
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			$("#tipoTrabajo").empty();
			respuesta.forEach(function(e){
				$("#tipos_trabajo").append('<option value="'+ e[0] +'">'+ e[0] + " - " + e[1] +'</option>');
			})
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [cargarTipoTrabajo carga el tipo de formulario con base al elemento seleccionado del dropdown]
 * @param  {[type]} tipo_trabajo [clave del tipo de trabajo que se cargará]
 * @return {[type]}              [description]
 */
function cargarTipoTrabajo(tipo_trabajo){
	tipo_trabajo = tipo_trabajo.toLowerCase();
	$("#formularioTrabajo").empty().load("vista/modulos/tipos_trabajos/"+tipo_trabajo+".html", function(respuesta, status, xhr) {
		$(this).css('display', 'block');
		mostrarAsesor("#formGroupAsesor");
		mostrarAsesor("#formGroupCoasesor");
		mostrarAreaAcademica("#formGroupAreaAcademica");
	});
}

/**
 * [mostrarAsesor muestra los asesores disponibles]
 * @param  {[type]} campo [id del campo donde se desplegarán los asesores]
 * @return {[type]}       [description]
 */
function mostrarAsesor(campo){
	var dato = {
		funcion: 'obtenerAsesores',
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			respuesta.forEach(function(e){
				$(campo).append('<option value="'+e[0]+'">'+e[1]+" "+e[2]+" "+e[3]+'</option>');
			})
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}
/**
 * [mostrarAreaAcademica imprime las áreas académicas y las despleiga en un select]
 * @param  {[text]} campo [id del campo donde se desplegarán las áreas académicas]
 * @return {[type]}       [description]
 */
function mostrarAreaAcademica(campo){
	var dato = {
		funcion: 'obtenerAreaAcademica',
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			respuesta.forEach(function(e){
				$(campo).append('<option value="'+e[0]+'">'+e[1]+'</option>');
			})
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}













/**
 * [limpiarFormulario limpia los datos de un formulario dado su id]
 * @param  {[text]} nombre_formulario [id del formulario]
 * @return {[type]}                   [description]
 */
function limpiarFormulario(nombre_formulario) {
	$('#'+nombre_formulario)[0].reset();
}

/**
 * [mostrarModalAlerta mostrar modal de acción exitosa]
 * @param  {[text} tipo  [tipo de mensaje: error, info, exito]
 * @param  {[text} texto [mensaje que será desplegado]
 */
function mostrarModalAlerta(tipo, texto){
	$('#texto_mensaje_'+tipo).append(texto);
	$("#alerta_"+tipo).modal("show");
}

/**
 * [cerrarModal cierra un modal, identificado por id]
 * @param  {[text]} id [id del modal a cerrar]
 * @return {[type]}    [description]
 */
function cerrarModal(id){
	$('#'+id).modal('hide');
}

/**
 * [mostrarModal muestra un modal, identificado por id]
 * @param  {[ttext]} id [id del modal a mostrar]
 * @return {[type]}    [description]
 */
function mostrarModal(id){
	$('#'+id).modal('show');
}

/**
 * [limpiarContenido vacia el contenido de un elemento por su id]
 * @param  {[text]} id [id del objeto html]
 * @return {[type]}    [description]
 */
function limpiarContenido(clase){
	$('.'+clase).empty();
}

/**
 * [deshabilitarElementoID deshabilita un elemento por id]
 * @param  {[text]} id [id del objeto a deshabilitar]
 * @return {[type]}       [description]
 */
function deshabilitarElementoID(id){
	$("#"+id).attr('disabled', '');
}

/**
 * [habilitarElementoID habilita un elemento por id]
 * @param  {[text]} id [id del objeto a habilitar]
 * @return {[type]}       [description]
 */
function habilitarElementoID(id){
	$("#"+id).removeAttr('disabled', '');
}
'use strict'
$(document).ready(function (){

	//Muestra el nombre del usuario en la página de inicio.
	mostrarNombreTitulacion();

	//Muestra el número de solicitudes pendientes
	numeroSolicitudesPendientes();

	//Tabla de solicitudes pendientes
	solicitudesPendientes(0);

	$(document).on('click', '#btnAsigCveModal', function(){
		// console.log($(this).val());
		// $('#bntAsignarClavePro').val($(this).val());
		modalAsignarClaveProtocolo($(this).val());

	});

	$("#cveClaveAsignacion").blur(function(){
		let cve_pro = this.value;
		let trabajo = 
		buscarClaveProtocolo(clave);
	});

	// $('#btnAsigCveModal').click(function(){
		
		// mostrarModal('asignar_clave');
	// });
});

/**
 * [mostrarNombreTitulacion muestra el nombre del titulacion en la página de inicio]
 * @return {[type]} [description]
 */
function mostrarNombreTitulacion(){
	var dato = {
		funcion: 'obtenerNombreTitulacion'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/titulacion/titulacion.ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			$('#nombre_titulacion').append(respuesta['nombre']+" "+respuesta['apellidoPaterno']+" "+respuesta['apellidoMaterno']);
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [numeroSolicitudesPendientes obtiene el número de pasantes que requieren acceso al sistema y ya están registrados]
 * @return {[type]} [description]
 */
function numeroSolicitudesPendientes(){
	var dato = {
		funcion: 'numeroSolicitudesPendientes'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/titulacion/titulacion.ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			$('#badgeNumSolicitudesTitu').append(respuesta['num_sol_pend']);
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [solicitudesPendientes obtiene el número de pasantes que requieren acceso al sistema y ya están registrados]
 * @return {[type]} [description]
 */
function solicitudesPendientes(pag){
	var dato = {
		funcion: 'solicitudesPendientes',
		pagina:pag
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/titulacion/titulacion.ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			console.log(respuesta);
			$("#contenido-solicitudes").empty();
			respuesta.forEach(function (e){
				$("#contenido-solicitudes").append('<tr><td>'+e[0]+
				  '</td><td>'+e[3]+' '+e[4]+' '+e[5]+'</td><td>'+e[8]+'</td><td title="'+e[7]+
				  '">'+e[6]+'</td><td>'+e[1]+'</td><td><button class="btn btn-secondary"'+
				  ' data-toggle="modal" data-target="#asignar_clave" id="btnAsigCveModal" value="'+e[2]+
				  '-'+e[1]+'">Asignar</button></td></tr>');
				
			});
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

function modalAsignarClaveProtocolo(clave_temporal){
	let valor = clave_temporal.split('-');
	var dato = {
		funcion: 'datosSolicitudPasante',
		cve_tem:valor[0],
		trabajo: valor[1].toLowerCase()
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/titulacion/titulacion.ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(r){
			$('#nombreClaveAsignacion').val(r[0]+' '+r[1]+' '+r[2]);
			$('#licClaveAsignacion').val(r[3]);
			$('#escuelaClaveAsignacion').val(r[4]);
			$('#fechEntClaveAsignacion').val(r[5]);
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}
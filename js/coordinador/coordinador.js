'use strict';
$(document).ready(function (){

	//Muestra el nombre del coordinador en la página de inicio.
	mostrarNombreCoordinador();

	//Número de solicitudes de pasantes pendientes
	numeroPasantesPendientes();

	//Muestra la lista de los pasantes pendientes
	$('#pasantesPendientes').click(function (){
		mostrarListaPasantesPendientes(0);
	});

	//Pasantes pendientes páginado
	$(document).on('click', 'a.page-link', function(event) {
		var href = $(this).attr('href');
	    var pagina = href.split("=")
	    mostrarListaPasantesPendientes(pagina[1]);
	});

	// Aceptar pasantes
	$(document).on('click', '#btnAceptarPasante',function(){
		console.log($(this).val());
		aceptarPasantePendiente($(this).val());
	});

	// Rechazar pasantes
	$(document).on('click', '#btnRechazarPasante',function(){
		console.log($(this).val());
		rechazarPasantePendiente($(this).val());
	});

	// //Botón aceptar error del modal
	// $('#btnAceptarError').click(function (){
	// 	limpiarContenido('texto_mensaje_error');
	// });

	// //Botón aceptar exito del modal
	// $('#btnAceptarExito').click(function (){
	// 	limpiarContenido('texto_mensaje_exito');
	// });

	// //Botón aceptar info del modal
	// $('#btnAceptarInfo').click(function (){
	// 	limpiarContenido('texto_mensaje_info');
	// });

});


/**
 * [rechazarPasantePendiente rechaza a un pasante pendiente en el sistema(2)]
 * @param  {[text]} clave [la clave del pasante que se aceptará]
 * @return {[type]}       [description]
 */
function rechazarPasantePendiente(clave){
	var dato = {
		funcion: 'rechazarPasantePendiente',
		clave_pasante:clave
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/coordinador/coordinador.ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				$("#btnRechazarPasante").parent().parent().remove();
				$('#badgeNumSolicitudes').empty();
				numeroPasantesPendientes();
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}


/**
 * [aceptarPasantePendiente acepta a un pasante pendiente en el sistema(1), dándole acceso al sistema]
 * @param  {[text]} clave [la clave del pasante que se aceptará]
 * @return {[type]}       [description]
 */
function aceptarPasantePendiente(clave){
	var dato = {
		funcion: 'aceptarPasantePendiente',
		clave_pasante:clave
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/coordinador/coordinador.ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			if (respuesta.hasOwnProperty("COD_ERR"))
				mostrarModalAlerta('error', 'CODIGO: ' + respuesta['COD_ERR']+'<br> DRIVER: '+respuesta['ERR_DRI']+'<br> MENSAJE: '+respuesta['ERR_MSG']);
			else{
				$("#btnAceptarPasante").parent().parent().remove();
				$('#badgeNumSolicitudes').empty();
				numeroPasantesPendientes();
			}
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [mostrarListaPasantesPendientes muestra la lista de pasantes pendientes]
 * @param  {[type]} pagina [description]
 * @return {[type]}        [description]
 */
function mostrarListaPasantesPendientes(pagina){
	var dato = {
		funcion: 'listaPasantesPendientes',
		pagina:pagina
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/coordinador/coordinador.ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			var i = 0;
			$("#tablaPasantesPendientes").empty();
			if (pagina == 0 || pagina == 1) {
				i = 1;
			}else if(pagina >= 2){
				i = (pagina*10)-9;
			}
			respuesta.forEach(function(e){
				$('#tablaPasantesPendientes').append('<tr><td>'+e[0]+
				  '</td><td>'+e[1]+' '+e[2]+' '+e[3]+'</td><td>'+e[4]+'</td>'+
				  '<td><button class="btn" id="btnAceptarPasante" value="'+e[0]+'">'+
						'<img src="imagenes/iconos/aceptar.png"></button></td>'+
				  '<td><button class="btn" id="btnRechazarPasante" value="'+e[0]+'">'+
						'<img src="imagenes/iconos/eliminar.png"></button></td></tr>')
			});
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [numeroPasantesPendientes obtiene el número de pasantes que requieren acceso al sistema y ya están registrados]
 * @return {[type]} [description]
 */
function numeroPasantesPendientes(){
	var dato = {
		funcion: 'numeroPasantesPendientes'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/coordinador/coordinador.ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			$('#badgeNumSolicitudes').append(respuesta['num_pas_pend']);
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [mostrarNombreCoordinador muestra el nombre del coordinador en la página de inicio]
 * @return {[type]} [description]
 */
function mostrarNombreCoordinador(){
	var dato = {
		funcion: 'obtenerNombreCoordinador'
	}
	$.ajax({
		type: 'POST',
		url: "vista/ajax/coordinador/coordinador.ajax.php",
		data: dato,
		dataType: 'json',
		encode: true,
		success: function(respuesta){
			$('#nombre_coordinador').append(respuesta['nombre']+" "+respuesta['apellidoPaterno']+" "+respuesta['apellidoMaterno']);
		},
		error: function(jqXHR, textStatus, errorThrown){
			mostrarModalAlerta('error', "Error "+ jqXHR.status+ ": " + errorThrown);
		}
	});
}

/**
 * [mostrarModalAlerta mostrar modal de acción exitosa]
 * @param  {[text} tipo  [tipo de mensaje: error, info, exito]
 * @param  {[text} texto [mensaje que será desplegado]
 */
function mostrarModalAlerta(tipo, texto){
	$('#texto_mensaje_'+tipo).append(texto);
	$("#alerta_"+tipo).modal("show");
}

/**
 * [cerrarModal cierra un modal, identificado por id]
 * @param  {[text]} id [id del modal a cerrar]
 * @return {[type]}    [description]
 */
function cerrarModal(id){
	$('#'+id).modal('hide');
}

/**
 * [mostrarModal muestra un modal, identificado por id]
 * @param  {[ttext]} id [id del modal a mostrar]
 * @return {[type]}    [description]
 */
function mostrarModal(id){
	$('#'+id).modal('show');
}

/**
 * [limpiarContenido vacia el contenido de un elemento por su id]
 * @param  {[text]} id [id del objeto html]
 * @return {[type]}    [description]
 */
function limpiarContenido(id){
	$('#'+id).empty();
}

/**
 * [deshabilitarElementoID deshabilita un elemento por id]
 * @param  {[text]} id [id del objeto a deshabilitar]
 * @return {[type]}       [description]
 */
function deshabilitarElementoID(id){
	$("#"+id).attr('disabled', '');
}

/**
 * [habilitarElementoID habilita un elemento por id]
 * @param  {[text]} id [id del objeto a habilitar]
 * @return {[type]}       [description]
 */
function habilitarElementoID(id){
	$("#"+id).removeAttr('disabled', '');
}
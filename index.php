<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 13 de agosto del 2019
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: pagina index que carga los archivos necesarios para el funcionamiento del sistema.
*  ANOTACIONES: El MVC (modelo vista controlador) es un estilo de arquitectura de software que separa
*  los datos de una aplicación, la interfaz de usuario, y la lógica de control en tres componentes
*  distintos:
*      * El controlador actúa como intermediario entre el Modelo y la Vista, gestionando el flujo
*        de información entre ellos y las transformaciones para adaptar los datos a las necesidades de
*        cada uno.
*      * El modelo contiene una representación de los datos que maneja el sistema, su lógica de
*        negocio, y sus mecanismos de persistencia.
*      * La vista o interfaz de usuario, que compone la información que se envía al cliente y los
*        mecanismos interacción con éste.
*/
	require_once"controlador/controlador.php";
	require_once "modelo/modelo.php";
	require_once("modelo/enlaces.php");

	$pagina = new Controlador();
	$pagina->pagina();
?>
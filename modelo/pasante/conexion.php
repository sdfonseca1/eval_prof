<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 9 DE ABRIL 2010
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo de conexión a la base de datos.
*  ANOTACIONES:
*/
	class Conexion{
		
		/**
		 * [conectarPasante crea la conexión a la base de datos con el usuario pasante]
		 * @return [type] [description]
		 */
		public function conectarPasante(){
			try{
				$link = new PDO("mysql:host=localhost;dbname=dbProtocolosTitulacion","pasante","&Qr6LnkN2SAA");
				return $link;
			}catch(PDOException $e){
			 	echo "Error: " . $e->getMessage();
			}
		}
	}
?>
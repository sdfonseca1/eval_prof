<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 9 DE ABRIL 2020
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo que contiene los meétodos para hacer consultas a la base de datos y devolverlos al controlador.
*  ANOTACIONES: 
*
*/
	require ("conexion.php");
	/**
	 * Clase de comunicación con la base de datos para el pasante.
	 */
	class ModeloPasante{

		/**
		 * [buscarClavePasanteModelo busca la clave del pasante en la base de datos]
		 * @param  [text] $tabla         [nombre de la tabla para la sentencia]
		 * @param  [text] $clave_pasante [la clave del pasante]
		 * @return [array]               [regresa los datos de la consulta o el objeto de error]
		 */
		public function buscarClavePasanteModelo($tabla, $clave_pasante){
			$sentencia = Conexion::conectarPasante()->prepare("SELECT clavePasante FROM $tabla WHERE clavePasante=:clave_pasante");
			$sentencia->bindParam(":clave_pasante", $clave_pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch(PDO::FETCH_ASSOC);
			else
				return ModeloPasante::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [registrarPasanteModelo registra a un nuevo pasante]
		 * @param  [text]  $tabla         [nombre de la tabla para la sentencia]
		 * @param  [array] $datos_pasante [la clave del pasante]
		 * @return [array]                [regresa los datos de la consulta o el objeto de error]
		 */
		public function registrarPasanteModelo($tabla, $datos_modelo){
			$sentencia = Conexion::conectarPasante()->prepare("INSERT INTO $tabla VALUES(:clave, :nombre, :paterno, :materno, :nacimiento, :escuela, :licenciatura, :telefono, :email, :contrasena, '0')");
			$sentencia->bindParam(':clave', $datos_modelo['cuenta'], PDO::PARAM_STR);
			$sentencia->bindParam(':nombre', $datos_modelo['nombre'], PDO::PARAM_STR);
			$sentencia->bindParam(':paterno', $datos_modelo['ape_pat'], PDO::PARAM_STR);
			$sentencia->bindParam(':materno', $datos_modelo['ape_mat'], PDO::PARAM_STR);
			$sentencia->bindParam(':nacimiento', $datos_modelo['fecha_nac'], PDO::PARAM_STR);
			$sentencia->bindParam(':licenciatura', $datos_modelo['licenciatura'], PDO::PARAM_STR);
			$sentencia->bindParam(':escuela', $datos_modelo['escuela'], PDO::PARAM_STR);
			$sentencia->bindParam(':telefono', $datos_modelo['telefono'], PDO::PARAM_STR);
			$sentencia->bindParam(':email', $datos_modelo['correo'], PDO::PARAM_STR);
			$sentencia->bindParam(':contrasena', password_hash($datos_modelo['contrasena'],PASSWORD_BCRYPT), PDO::PARAM_STR);
			if ($sentencia->execute())
				return true;
			else
				return ModeloPasante::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [ingresoPasanteModelo obtiene los datos de ingreso del supuesto pasante]
		 * @param  [text] $tabla          [nombre de la tabla para la sentencia]
		 * @param  [text] $clave_pasante  [clave del pasante]
		 * @return [array]                [regresa los datos de la consulta o el objeto de error]
		 */
		public function ingresoPasanteModelo($tabla, $clave_pasante){
			$sentencia = Conexion::conectarPasante()->prepare("SELECT clavePasante, contrasena, aceptado, licenciatura FROM $tabla WHERE clavePasante=:clave");
			$sentencia->bindParam(":clave", $clave_pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloPasante::obtenerErrorConsulta($sentencia->errorInfo());
		}
		
		/**
		 * [obtenerNombrePasanteModelo obtiene el nombre y apellido paterno del pasante registrado]
		 * @param  [text] $tabla         [nombre de la tabla donde se realizará la búsqueda]
		 * @param  [text] $clave_pasante [regresa nombre completo del pasante o error, si lo hubiera]
		 * @return [array]               [regresa los datos de la consulta o el objeto de error]
		 */
		public function obtenerNombrePasanteModelo($tabla, $clave_pasante){
			$sentencia = Conexion::conectarPasante()->prepare("SELECT nombre, apellidoPaterno, apellidoMaterno FROM $tabla WHERE clavePasante=:clavePasante");
			$sentencia->bindParam(':clavePasante', $clave_pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloPasante::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerMisDatosPasanteModelo muestra los datos básicos del pasante]
		 * @param  [type] $tabla         [nombre de la tabla para la sentencia]
		 * @param  [type] $clave_pasante [la clave del pasante]
		 * @return [type]                [regresa los datos de la consulta o el objeto de error]
		 */
		public function obtenerMisDatosPasanteModelo($tabla, $clave_pasante){
			$sentencia = Conexion::conectarPasante()->prepare("SELECT pasante.nombre, pasante.apellidoPaterno, pasante.apellidoMaterno, pasante.escuela, escuela.nombre, pasante.licenciatura, licenciatura.nombreLicenciatura, pasante.telefono, pasante.email, pasante.fechaNacimiento FROM pasante JOIN escuela ON pasante.escuela=escuela.claveEscuela JOIN licenciatura ON pasante.licenciatura=licenciatura.claveLicenciatura WHERE clavePasante=:clave_pasante");
			$sentencia->bindParam(':clave_pasante', $clave_pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloPasante::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [actualizarDatosPasanteModelo actualiza los datos nuevos del pasante]
		 * @param  [text] $tabla         [nombre de la tabla para la sentencia]
		 * @param  [text] $clave_pasante [la clave del pasante]
		 * @param  [array] $datos        [los nuevos datos del pasante]
		 * @return [array]               [regresa los datos de la consulta o el objeto de error]
		 */
		public function actualizarDatosPasanteModelo($tabla, $clave_pasante, $datos){
			$sentencia = Conexion::conectarPasante()->prepare("UPDATE pasante SET nombre=:nombre, apellidoPaterno=:paterno, apellidoMaterno=:materno, fechaNacimiento=:fecha, telefono=:tel, email=:correo WHERE clavePasante=:clave_pasante");
			$sentencia->bindParam(':nombre', $datos['nombre'], PDO::PARAM_STR);
			$sentencia->bindParam(':paterno', $datos['ape_pat'], PDO::PARAM_STR);
			$sentencia->bindParam(':materno', $datos['ape_mat'], PDO::PARAM_STR);
			$sentencia->bindParam(':fecha', $datos['fecha'], PDO::PARAM_STR);
			$sentencia->bindParam(':tel', $datos['tel'], PDO::PARAM_STR);
			$sentencia->bindParam(':correo', $datos['correo'], PDO::PARAM_STR);
			$sentencia->bindParam(':clave_pasante', $clave_pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return true;
			else
				return ModeloPasante::obtenerErrorConsulta($sentencia->errorInfo());
		}
		
		/**
		 * [actualizarContrasenaPasanteModelo guarda la nueva contraseña del pasante]
		 * @param  [text] $tabla         [nombre de la tabla para la sentencia]
		 * @param  [text] $clave_pasante [la clave del pasante]
		 * @param  [text] $contrasena    [la nueva contraseña del pasante]
		 * @return [array]               [regresa los datos de la consulta o el objeto de error]
		 */
		public function actualizarContrasenaPasanteModelo($tabla, $clave_pasante, $contrasena){
			$sentencia = Conexion::conectarPasante()->prepare("UPDATE $tabla SET contrasena=:contrasena WHERE clavePasante=:clave_pasante");
			$sentencia->bindParam(':contrasena', password_hash($contrasena,PASSWORD_BCRYPT), PDO::PARAM_STR);
			$sentencia->bindParam(':clave_pasante', $clave_pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloPasante::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [existeSolicitudRegistradaModelo obtiene si el pasante tiene una solicitud]
		 * @param  [text] $tabla         [nombre de la tabla para la sentencia]
		 * @param  [text] $clave_pasante [la clave del pasante]
		 * @return [type]                [description]
		 */
		public function existeSolicitudRegistradaModelo($tabla, $clave_pasante){
			$sentencia = Conexion::conectarPasante()->prepare("SELECT clavePasante FROM $tabla WHERE clavePasante=:clavePasante AND estadoProtocolo=1");
			$sentencia->bindParam(':clavePasante', $clave_pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloPasante::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [existeBorradorProtocoloModelo verifica si hay un protocolo en edición]
		 * @param  [text] $tabla         [nombre de la tabla para la sentencia]
		 * @param  [text] $clave_pasante [la clave del pasante]
		 * @return [array]               [description]
		 */
		public function existeBorradorProtocoloModelo($tabla, $clave_pasante){
			$sentencia = Conexion::conectarPasante()->prepare("SELECT clavePasante, tipoTrabajo FROM $tabla WHERE clavePasante=:clave");
			$sentencia->bindParam(":clave", $clave_pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloPasante::obtenerErrorConsulta($sentencia->errorInfo());
		}



		/**
		 * [mostrarDatosAEdicionModelo obtiene los datos que se editarán del pasante]
		 * @param  [text]  $tabla         [nombre de la tabla para la sentencia]
		 * @param  [ttext] $clave_pasante [la clave del pasante]
		 * @return [array]                [regresa los datos de la consulta o el objeto de error]
		 */
		public function mostrarDatosAEdicionModelo($tabla, $clave_pasante){
			$sentencia = Conexion::conectarPasante()->prepare("SELECT pasante.nombre, pasante.apellidoPaterno, pasante.apellidoMaterno, pasante.telefono, pasante.email FROM pasante WHERE clavePasante=:clave_pasante");
			$sentencia->bindParam(':clave_pasante', $clave_pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloPasante::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerErrorConsulta crear arreglo asociativo con el arreglo numérico de entrada]
		 * @param  [array] $arreglo [arreglo de errores de mysql]
		 * @return [array]          [arreglo asociativo generado con el arreglo de entrada]
		 */
		private function obtenerErrorConsulta($arreglo){
			$asociativo = array('COD_ERR', 'ERR_DRI', 'ERR_MSG');
			return array_combine($asociativo, $arreglo);
		}

		/**
		 * [obtenerEtapaProcesoModelo obtiene la etapa en la que se encuentra el pasante]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [type] $pasante [clave del pasante]
		 * @return [array]          [resultado de la sentencia]
		 */
		public function obtenerEtapaProcesoModelo($tabla, $pasante){
			$sentencia = Conexion::conectarPasante()->prepare("SELECT estadoProtocolo FROM $tabla WHERE clavePasante=:pasante");
			$sentencia->bindParam(':pasante', $pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetchAll();
			else
				return ModeloPasante::obtenerErrorConsulta($sentencia->errorInfo());
			
		}

		/**
		 * [obtenerLicenciaturasModelo btiene la lista de licenciaturas registradas en el sistema]
		 * @param  [type] $tabla [nombre de la tabla para la sentencia]
		 * @return [type]        [description]
		 */
		public function obtenerLicenciaturasModelo($tabla){
			$sentencia = Conexion::conectarPasante()->prepare("SELECT * FROM $tabla");
			$sentencia->execute();
			return $sentencia->fetchAll();
		}

		/**
		 * [obtenerEscuelasModelo description]
		 * @param  [type] $tabla [nombre de la tabla para la sentencia]
		 * @return [type]        [description]
		 */
		public function obtenerEscuelasModelo($tabla){
			$sentencia = Conexion::conectarPasante()->prepare("SELECT * FROM $tabla");
			$sentencia->execute();
			return $sentencia->fetchAll();
		}






	}
?>
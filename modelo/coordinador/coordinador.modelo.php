<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 8 de Abril 2020
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo que contiene los meétodos para hacer consultas a la base de datos y devolverlos al controlador.
*  ANOTACIONES:
*/
	require "conexion.php";
	
	class ModeloCoordinador{

		/**
		 * [ingresoCoordinadorModelo obtiene los datos de ingreso del supuesto coordinador]
		 * @param  [text] $tabla             [nombre de la tabla para la sentencia]
		 * @param  [text] $clave_coordinador [clave del coordinador]
		 * @return [type]                    [resultado de la sentencia]
		 */
		public function ingresoCoordinadorModelo($tabla, $clave_coordinador){
			$sentencia = Conexion::conectarCoordinador()->prepare("SELECT claveCoordinador, contrasena, licenciatura FROM $tabla WHERE claveCoordinador=:clave");
			$sentencia->bindParam(":clave", $clave_coordinador, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloCoordinador::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerNombreCoordinadorModelo obtiene el nombre y apellido paterno del coordinador registrado]
		 * @param  [text] $tabla [nombre de la tabla donde se realizará la búsqueda]
		 * @param  [text] $clave [clave del coordinador]
		 * @return [object] $sentencia->fetch() [regresa los datos de la consulta o el objeto de error]
		 */
		public function obtenerNombreCoordinadorModelo($tabla, $clave_coordinador){
			$sentencia = Conexion::conectarCoordinador()->prepare("SELECT nombre, apellidoPaterno, apellidoMaterno FROM $tabla WHERE claveCoordinador=:claveCoordinador");
			$sentencia->bindParam(':claveCoordinador', $clave_coordinador, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloCoordinador::obtenerErrorConsulta($sentencia->errorInfo());
		
		}

		/**
		 * [numeroPasantesPendientesModelo obtiene el número de pasantes que se han registrado pero no han sido aprobados]
		 * @param  [text] $licenciatura [licenciatura del coordinador]
		 * @return [array]               []
		 */
		public function numeroPasantesPendientesModelo($tabla, $licenciatura){
			$sentencia = Conexion::conectarCoordinador()->prepare("SELECT COUNT(clavePasante) AS num_pas_pend FROM $tabla WHERE licenciatura=:licenciatura AND aceptado=0");
			$sentencia->bindParam(":licenciatura", $licenciatura, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloCoordinador::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [listaPasantesPendientesModelo uestra la lista de pasantes pendientes en registros de 10 en 10]
		 * @param  [text] $tabla        [nombre de la tabla donde se realizará la búsqueda]
		 * @param  [text] $licenciatura [licenciatura del coordinador]
		 * @param  [int]  $reg_ini      [indica en qué numero de registro comenará]
		 * @return [array]              [regresa los datos de la consulta o el objeto de error]
		 */
		public function listaPasantesPendientesModelo($tabla, $licenciatura, $reg_ini){
			$sentencia = Conexion::conectarCoordinador()->prepare("SELECT pasante.clavePasante, pasante.nombre, pasante.apellidoPaterno, pasante.apellidoMaterno, escuela.nombre FROM $tabla JOIN escuela ON pasante.escuela=escuela.claveEscuela WHERE pasante.licenciatura=:licenciatura AND aceptado=0 LIMIT $reg_ini, 10");
			$sentencia->bindParam(":licenciatura", $licenciatura, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetchAll();
			else
				return ModeloCoordinador::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [aceptarPasantePendienteModelo actualiza el estado del campo aceptado de un pasante]
		 * @param  [text]  $tabla         [nombre de la tabla donde se realizará la sentencia]
		 * @param  [text]  $clave_pasante [la clave del pasante]
		 * @return [array]                [description]
		 */
		public function aceptarPasantePendienteModelo($tabla, $clave_pasante){
			$sentencia = Conexion::conectarCoordinador()->prepare("UPDATE $tabla SET aceptado='1' WHERE clavePasante=:clave");
			$sentencia->bindParam(":clave", $clave_pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return true;
			else
				return ModeloCoordinador::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [rechazarPasanteModelo actualiza el estado del campo aceptado de un pasante]
		 * @param  [text]  $tabla         [nombre de la tabla donde se realizará la sentencia]
		 * @param  [text]  $clave_pasante [la clave del pasante]
		 * @return [type]                [description]
		 */
		public function rechazarPasantePendienteModelo($tabla, $clave_pasante){
			$sentencia = Conexion::conectarCoordinador()->prepare("UPDATE $tabla SET aceptado='2' WHERE clavePasante=:clave");
			$sentencia->bindParam(":clave", $clave_pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return true;
			else
				return ModeloCoordinador::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerErrorConsulta crear arreglo asociativo con el arreglo numérico de entrada]
		 * @param  [array] $arreglo [arreglo de errores de mysql]
		 * @return [array]          [arreglo asociativo generado con el arreglo de entrada]
		 */
		private function obtenerErrorConsulta($arreglo){
			$asociativo = array('COD_ERR', 'ERR_DRI', 'ERR_MSG');
			return array_combine($asociativo, $arreglo);
		}




	}
?>
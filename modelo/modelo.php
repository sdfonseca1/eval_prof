<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 9 DE ABRIL 2020
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo que contiene los meétodos para hacer consultas a la base de datos y devolverlos al controlador.
*  ANOTACIONES: 
	require ("conexion.php");

*/
	require ("conexion.php");
	
	class Modelo extends Conexion{

		/**
		 * [contarPasantesPendientesModelo obtiene el número de pasantes registrados pero que no han sido aceptados]
		 * @param  [text] $tabla        [nombre de la tabla para la setencia]
		 * @param  [type] $licenciatura [licenciatura del coordinador]
		 * @return [type]               [description]
		 */
		public function contarPasantesPendientesModelo($tabla, $licenciatura){
			$sentencia = Conexion::conectar()->prepare("SELECT COUNT(clavePasante) FROM $tabla WHERE licenciatura=:licenciatura AND aceptado=0");
			$sentencia->bindParam(":licenciatura", $licenciatura, PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetch();
		}

		/**
		 * [numeroSolicitudesPendientesModelo obtiene el número de solicitudes que se han registrado pero no han sido asignadas]
		 * @param  [text] $tabla [nombre de la tabla donde se realizará la búsqueda]
		 * @return [array]       []
		 */
		public function numeroSolicitudesPendientesModelo($tabla){
			$sentencia = Conexion::conectar()->prepare("SELECT COUNT(claveProtocoloTemporal) AS num_sol_pend FROM $tabla WHERE estadoProtocolo='1'");
			$sentencia->execute();
			return $sentencia->fetch(PDO::FETCH_NUM);
		}



		// public function aceptarPasanteModelo($tabla, $clave_pasante){
		// 	/*UTILIDAD: actualiza el estado del campo aceptado de un pasante.
		// 	  PRECONDICION: el pasante debe estar registrado.
		// 	  POSTCONDICIÓN: regresa falso o verdadero si se actualizó.
		// 	*/
		// 	$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET aceptado='1' WHERE clavePasante=:clave");
		// 	$sentencia->bindParam(":clave", $clave_pasante, PDO::PARAM_STR);
		// 	return $sentencia->execute();
		// }

		// public function rechazarPasanteModelo($tabla, $clave_pasante){
		// 	/*UTILIDAD: actualiza el estado del campo aceptado de un pasante.
		// 	  PRECONDICION: el pasante debe estar registrado.
		// 	  POSTCONDICIÓN: regresa falso o verdadero si se actualizó.
		// 	*/
		// 	$sentencia = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE clavePasante=:clave");
		// 	$sentencia->bindParam(":clave", $clave_pasante, PDO::PARAM_STR);
		// 	return $sentencia->execute();
		// }

		

	/*********************** AEM ************************/



		
		
		









		/**
		 * [obtenerListaAsesoresModelo obtiene la lista de asesores registrados en el sistema.]
		 * @param  [type] $tabla [nombre de la tabla para la sentencia]
		 * @return [type]        [description]
		 */
		public function obtenerListaAsesoresModelo($tabla){
			$sentencia = Conexion::conectar()->prepare("SELECT claveAsesor, nombre, apellidoPaterno, apellidoMaterno FROM $tabla");
			$sentencia->execute();
			return $sentencia->fetchAll();
		}

		/**
		 * [obtenerAreaAcademicaModelo obtiene la lista de las áreas académicas registradas en el sistema]
		 * @param  [text] $licenciatura [la licenciatura de la cúal se requiere las áreas académicas]
		 * @return [type]               [description]
		 */
		public function obtenerAreaAcademicaModelo($tabla, $licenciatura){
			$sentencia = Conexion::conectar()->prepare("SELECT claveArea, nombreArea FROM $tabla WHERE licenciatura=:licenciatura");
			$sentencia->bindParam(":licenciatura", $licenciatura, PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetchAll();
		}

		
		/**
		 * [obtenerEscuelasModelo description]
		 * @param  [type] $tabla [description]
		 * @return [type]        [description]
		 */
		public function obtenerEscuelasModelo($tabla){
			$sentencia = Conexion::conectar()->prepare("SELECT * FROM $tabla");
			$sentencia->execute();
			return $sentencia->fetchAll();
		}

		public function obtenerNombreCoordinadorModelo($tabla, $clave_coordinador){
			/*UTILIDAD: obtiene los datos del administrador buscado.
			  PRECONDICION: recibe la clave del administrador.
			  POSTCONDICIÓN: regresa los datos del administrador si encontró los datos en la BD.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT nombre, apellidoPaterno, apellidoMaterno FROM $tabla WHERE  	claveCoordinador=:clave_coordinador");
			$sentencia->bindParam(':clave_coordinador', $clave_coordinador, PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetch();
		}

		public function obtenerSolicitudesPasantesModelo($tabla, $licenciatura, $pagina){
			$numero_registros = 10;
			if ($pagina == 0 || $pagina == 1) {
				$inicio = 0;
			}else{
				$inicio = ($pagina-1)*10;
			}
			$sentencia = Conexion::conectar()->prepare("SELECT clavePasante, nombre, apellidoPaterno, apellidoMaterno, escuela, licenciatura FROM $tabla WHERE licenciatura=:licenciatura AND aceptado=0 LIMIT $inicio, $numero_registros");
			$sentencia->bindParam(':licenciatura', $licenciatura, PDO::PARAM_STR);
			$sentencia->execute();
			// var_dump($sentencia->fetchAll());
			return $sentencia->fetchAll();
		}

		public function obtenerTiposDeTrabajoModelo($tabla){
			/*UTILIDAD: obtiene los tipos de trabajo escrito.
			  PRECONDICION:
			  POSTCONDICIÓN: regresa los datos del pasante si es que los encontró.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT * FROM $tabla");
			$sentencia->execute();
			// var_dump($sentencia->fetchAll());
			return $sentencia->fetchAll();
		}

		/**
		 * [eliminarBorradorProtocoloModelo elimina el protocolo ART]
		 * @param  [type] $tabla [nombre de la tabla para la sentencia]
		 * @param  [type] $pasante [clave del pasante]
		 * @return [boolean]                [resultado de la sentencia]
		 */ 
		public function eliminarBorradorProtocoloModelo($tabla, $pasante){
			$sentencia = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE clavePasante=:clavePasante");
			$sentencia->bindParam(":clavePasante", $pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return Modelo::obtenerErrorConsulta($sentencia->errorInfo());
		}

		

		/**
		 * [obtenerErrorConsulta crear arreglo asociativo con el arreglo numérico de entrada]
		 * @param  [array] $arreglo [arreglo de errores de mysql]
		 * @return [array]          [arreglo asociativo generado con el arreglo de entrada]
		 */
		private function obtenerErrorConsulta($arreglo){
			$asociativo = array('COD_ERR', 'ERR_DRI', 'ERR_MSG');
			return array_combine($asociativo, $arreglo);
		}

	}

?>

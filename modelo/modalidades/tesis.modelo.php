<?php

	require_once("conexion.php");

	/**
	 * 
	 */
	class ModeloTesis extends Conexion {

		/**
		 * [guardarProtocoloTSSModelo guarda por primera vez los datos de un protocolo TSS]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [datos del formulario de protocolo TSS]
		 * @return [array]         [Falso o Verdadero si la sentencia se ejecuta, si hay un error lo devuelve]
		 */
		public function guardarProtocoloTSSModelo($tabla, $pasante, $datos){
			$cve_tem = date('Y') . date("md") . date("H") . date("i") . date("s");
			$sentencia = Conexion::conectar()->prepare("INSERT INTO $tabla VALUES (null, :cve_tem, :pasante, null, null, :titulo, :asesor, :coasesor, :area_academica, :planteamiento, :justificacion, :hipotesis, :objetivo, :metodologia, :referencias, :cronograma)");
			$sentencia->bindParam(":cve_tem", $cve_tem, PDO::PARAM_STR);
			$sentencia->bindParam(":pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
			$sentencia->bindParam(":asesor", $datos['asesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":coasesor", $datos['coasesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":area_academica", $datos['area'], PDO::PARAM_STR);
			$sentencia->bindParam(":planteamiento", $datos['planteamiento'], PDO::PARAM_STR);
			$sentencia->bindParam(":justificacion", $datos['justificacion'], PDO::PARAM_STR);
			$sentencia->bindParam(":hipotesis", $datos['hipotesis'], PDO::PARAM_STR);
			$sentencia->bindParam(":objetivo", $datos['objetivo'], PDO::PARAM_STR);
			$sentencia->bindParam(":metodologia", $datos['metodologia'], PDO::PARAM_STR);
			$sentencia->bindParam(":referencias", $datos['referencias'], PDO::PARAM_STR);
			$sentencia->bindParam(":cronograma", json_encode($datos['cronograma']), PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloTesis::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerProtocoloTSSModelo obtiene los valores del protocolo guardado]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]]
		 * @param  [type] $trabajo [clave del protocolo que se utilizará]
		 * @return [type]          [Falso o Verdadero si la sentencia se ejecuta, si hay un error lo devuelve]
		 */
		public function obtenerProtocoloTSSModelo($tabla, $pasante, $trabajo){
			$sentencia = Conexion::conectar()->prepare("SELECT pro_tss.claveTemporalProtocolo, pro_tss.titulo, pro_tss.asesor, pro_tss.coasesor, pro_tss.areaAcademica, pro_tss.planteamiento, pro_tss.justificacion, pro_tss.hipotesis, pro_tss.objetivoGeneral, pro_tss.metodologia, pro_tss.referencias, pro_tss.cronograma FROM $tabla JOIN pro_tss ON pasante_protocolo.clavePasante=pro_tss.clavePasante AND pasante_protocolo.claveProtocoloTemporal=pro_tss.claveTemporalProtocolo");
			if ($sentencia->execute())
				return $sentencia->fetchAll();
			else
				return ModeloTesis::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [actualizarProtocoloTSSModelo actualiza los datos de un protocolo TSS]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [datos del formulario de protocolo TSS]
		 * @return [type]          [description]
		 */
		public function actualizarProtocoloTSSModelo($tabla, $pasante, $datos){
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET titulo=:titulo, asesor=:asesor, coasesor=:coasesor, areaAcademica=:area_academica, planteamiento=:planteamiento, justificacion=:justificacion, hipotesis=:hipotesis, objetivoGeneral=:objetivo, metodologia=:metodologia, referencias=:referencias, cronograma=:cronograma WHERE clavePasante=:clave_pasante");
			$sentencia->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
			$sentencia->bindParam(":asesor", $datos['asesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":coasesor", $datos['coasesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":area_academica", $datos['area'], PDO::PARAM_STR);
			$sentencia->bindParam(":planteamiento", $datos['planteamiento'], PDO::PARAM_STR);
			$sentencia->bindParam(":justificacion", $datos['justificacion'], PDO::PARAM_STR);
			$sentencia->bindParam(":hipotesis", $datos['hipotesis'], PDO::PARAM_STR);
			$sentencia->bindParam(":objetivo", $datos['objetivo'], PDO::PARAM_STR);
			$sentencia->bindParam(":metodologia", $datos['metodologia'], PDO::PARAM_STR);
			$sentencia->bindParam(":referencias", $datos['referencias'], PDO::PARAM_STR);
			$sentencia->bindParam(":cronograma", json_encode($datos['cronograma']), PDO::PARAM_STR);
			$sentencia->bindParam(":clave_pasante", $pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloTesis::obtenerErrorConsulta($sentencia->errorInfo());
		}
		
		/**
		 * [cambioSolicitudProtocoloTSSModelo generación de solicitud del protocolo TSS]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $trabajo [datos del formulario de protocolo TSS]
		 * @return [type]          [description]
		 */
		public function cambioSolicitudProtocoloTSSModelo($tabla, $pasante, $trabajo){
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET estadoProtocolo=1 WHERE clavePasante=:clave_pasante AND tipoTrabajo=:trabajo");
			$sentencia->bindParam(":clave_pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":trabajo", $trabajo, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloTesis::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerErrorConsulta crear arreglo asociativo con el arreglo numérico de entrada]
		 * @param  [array] $arreglo [arreglo de errores de mysql]
		 * @return [array]          [arreglo asociativo generado con el arreglo de entrada]
		 */
		private function obtenerErrorConsulta($arreglo){
			$asociativo = array('COD_ERR', 'ERR_DRI', 'ERR_MSG');
			return array_combine($asociativo, $arreglo);
		}
	}
?>
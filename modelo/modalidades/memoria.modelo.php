<?php

	require_once("conexion.php");

	/**
	 * 
	 */
	class ModeloMemoria extends Conexion {

		/**
		 * [guardarProtocoloMELModelo guarda por primera vez los datos de un protocolo MEL]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [datos del formulario de protocolo MEL]
		 * @return [array]         [Falso o Verdadero si la sentencia se ejecuta, si hay un error lo devuelve]
		 */
		public function guardarProtocoloMELModelo($tabla, $pasante, $datos){
			$cve_tem = date('Y') . date("md") . date("H") . date("i") . date("s");
			$sentencia = Conexion::conectar()->prepare("INSERT INTO $tabla VALUES (null, :cve_tem, :pasante, null, null, :titulo, :periodoInicio, :periodoTermino, :asesor, :area_academica, :descripcion, :justificacion, :objetivo, :referencias, :cronograma)");
			$sentencia->bindParam(":cve_tem", $cve_tem, PDO::PARAM_STR);
			$sentencia->bindParam(":pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
			$sentencia->bindParam(":periodoInicio", $datos['fecha_ini'], PDO::PARAM_STR);
			$sentencia->bindParam(":periodoTermino", $datos['fecha_fin'], PDO::PARAM_STR);
			$sentencia->bindParam(":asesor", $datos['asesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":area_academica", $datos['area'], PDO::PARAM_STR);
			$sentencia->bindParam(":descripcion", $datos['descripcion'], PDO::PARAM_STR);
			$sentencia->bindParam(":justificacion", $datos['justificacion'], PDO::PARAM_STR);
			$sentencia->bindParam(":objetivo", $datos['objetivo'], PDO::PARAM_STR);
			$sentencia->bindParam(":referencias", $datos['referencias'], PDO::PARAM_STR);
			$sentencia->bindParam(":cronograma", json_encode($datos['cronograma']), PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloMemoria::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerProtocoloMELModelo obtiene los valores del protocolo guardado]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]]
		 * @param  [type] $trabajo [clave del protocolo que se utilizará]
		 * @return [type]          [Falso o Verdadero si la sentencia se ejecuta, si hay un error lo devuelve]
		 */
		public function obtenerProtocoloMELModelo($tabla, $pasante, $trabajo){
			$sentencia = Conexion::conectar()->prepare("SELECT pro_mel.claveTemporalProtocolo, pro_mel.titulo, pro_mel.periodoInicio, pro_mel.periodoTermino, pro_mel.asesor, pro_mel.areaAcademica, pro_mel.descripcion, pro_mel.justificacion, pro_mel.objetivoGeneral, pro_mel.referencias, pro_mel.cronograma FROM $tabla JOIN pro_mel ON pasante_protocolo.clavePasante=pro_mel.clavePasante AND pasante_protocolo.claveProtocoloTemporal=pro_mel.claveTemporalProtocolo");
			if ($sentencia->execute())
				return $sentencia->fetchAll();
			else
				return ModeloMemoria::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [actualizarProtocoloMELModelo actualiza los datos de un protocolo MEL]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [datos del formulario de protocolo MEL]
		 * @return [type]          [description]
		 */
		public function actualizarProtocoloMELModelo($tabla, $pasante, $datos){
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET titulo=:titulo, periodoInicio=:periodoInicio, periodoTermino=:periodoTermino, asesor=:asesor, areaAcademica=:area_academica, descripcion=:descripcion, justificacion=:justificacion, objetivoGeneral=:objetivo, referencias=:referencias, cronograma=:cronograma WHERE clavePasante=:clave_pasante");
			$sentencia->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
			$sentencia->bindParam(":periodoInicio", $datos['fecha_ini'], PDO::PARAM_STR);
			$sentencia->bindParam(":periodoTermino", $datos['fecha_fin'], PDO::PARAM_STR);
			$sentencia->bindParam(":asesor", $datos['asesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":area_academica", $datos['area'], PDO::PARAM_STR);
			$sentencia->bindParam(":descripcion", $datos['descripcion'], PDO::PARAM_STR);
			$sentencia->bindParam(":justificacion", $datos['justificacion'], PDO::PARAM_STR);
			$sentencia->bindParam(":objetivo", $datos['objetivo'], PDO::PARAM_STR);
			$sentencia->bindParam(":referencias", $datos['referencias'], PDO::PARAM_STR);
			$sentencia->bindParam(":cronograma", json_encode($datos['cronograma']), PDO::PARAM_STR);
			$sentencia->bindParam(":clave_pasante", $pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloMemoria::obtenerErrorConsulta($sentencia->errorInfo());
		}
		
		/**
		 * [cambioSolicitudProtocoloMELModelo generación de solicitud del protocolo MEL]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $trabajo [datos del formulario de protocolo MEL]
		 * @return [type]          [description]
		 */
		public function cambioSolicitudProtocoloMELModelo($tabla, $pasante, $trabajo){
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET estadoProtocolo=1 WHERE clavePasante=:clave_pasante AND tipoTrabajo=:trabajo");
			$sentencia->bindParam(":clave_pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":trabajo", $trabajo, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloMemoria::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerErrorConsulta crear arreglo asociativo con el arreglo numérico de entrada]
		 * @param  [array] $arreglo [arreglo de errores de mysql]
		 * @return [array]          [arreglo asociativo generado con el arreglo de entrada]
		 */
		private function obtenerErrorConsulta($arreglo){
			$asociativo = array('COD_ERR', 'ERR_DRI', 'ERR_MSG');
			return array_combine($asociativo, $arreglo);
		}
	}
?>
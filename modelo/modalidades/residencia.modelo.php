<?php

	require_once("conexion.php");

	/**
	 * 
	 */
	class ModeloResidencia extends Conexion {

		/**
		 * [guardarProtocoloRINModelo guarda por primera vez los datos de un protocolo RIN]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [datos del formulario de protocolo RIN]
		 * @return [array]         [Falso o Verdadero si la sentencia se ejecuta, si hay un error lo devuelve]
		 */
		public function guardarProtocoloRINModelo($tabla, $pasante, $datos){
			$cve_tem = date('Y') . date("md") . date("H") . date("i") . date("s");
			$sentencia = Conexion::conectar()->prepare("INSERT INTO $tabla VALUES (null, :cve_tem, :pasante, null, null, :periodoInicio, :periodoTermino, :investigador, :asesor, :coasesor, :area_academica, :titulo, :cve_proy, :descripcion, :reporte, :objetivo, :cronograma)");
			$sentencia->bindParam(":cve_tem", $cve_tem, PDO::PARAM_STR);
			$sentencia->bindParam(":pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":periodoInicio", $datos['fecha_ini'], PDO::PARAM_STR);
			$sentencia->bindParam(":periodoTermino", $datos['fecha_fin'], PDO::PARAM_STR);
			$sentencia->bindParam(":investigador", $datos['investigador'], PDO::PARAM_STR);
			$sentencia->bindParam(":asesor", $datos['asesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":coasesor", $datos['coasesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":area_academica", $datos['area'], PDO::PARAM_STR);
			$sentencia->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
			$sentencia->bindParam(":cve_proy", $datos['cve_proy'], PDO::PARAM_STR);
			$sentencia->bindParam(":descripcion", $datos['descripcion'], PDO::PARAM_STR);
			$sentencia->bindParam(":reporte", $datos['reporte'], PDO::PARAM_STR);
			$sentencia->bindParam(":objetivo", $datos['objetivo'], PDO::PARAM_STR);
			$sentencia->bindParam(":cronograma", json_encode($datos['cronograma']), PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloResidencia::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerProtocoloRINModelo obtiene los valores del protocolo guardado]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]]
		 * @param  [type] $trabajo [clave del protocolo que se utilizará]
		 * @return [type]          [Falso o Verdadero si la sentencia se ejecuta, si hay un error lo devuelve]
		 */
		public function obtenerProtocoloRINModelo($tabla, $pasante, $trabajo){
			$sentencia = Conexion::conectar()->prepare("SELECT pro_rin.claveTemporalProtocolo, pro_rin.periodoInicio, pro_rin.periodoTermino, pro_rin.nombreProfInves, pro_rin.asesor, pro_rin.coasesor, pro_rin.areaAcademica, pro_rin.tituloProy,pro_rin.claveRegProy, pro_rin.descripcion, pro_rin.tituloRep, pro_rin.objetivoGeneral, pro_rin.cronograma FROM $tabla JOIN pro_rin ON pasante_protocolo.clavePasante=pro_rin.clavePasante AND pasante_protocolo.claveProtocoloTemporal=pro_rin.claveTemporalProtocolo");
			if ($sentencia->execute())
				return $sentencia->fetchAll();
			else
				return ModeloResidencia::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [actualizarProtocoloRINModelo actualiza los datos de un protocolo RIN]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [datos del formulario de protocolo RIN]
		 * @return [type]          [description]
		 */
		public function actualizarProtocoloRINModelo($tabla, $pasante, $datos){
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET periodoInicio=:periodoInicio, periodoTermino=:periodoTermino, nombreProfInves=:investigador, asesor=:asesor, coasesor=:coasesor, areaAcademica=:area_academica, tituloProy=:titulo, claveRegProy=:cve_proy, descripcion=:descripcion, tituloRep=:reporte, objetivoGeneral=:objetivo, cronograma=:cronograma WHERE clavePasante=:clave_pasante");
			$sentencia->bindParam(":clave_pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":periodoInicio", $datos['fecha_ini'], PDO::PARAM_STR);
			$sentencia->bindParam(":periodoTermino", $datos['fecha_fin'], PDO::PARAM_STR);
			$sentencia->bindParam(":investigador", $datos['investigador'], PDO::PARAM_STR);
			$sentencia->bindParam(":asesor", $datos['asesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":coasesor", $datos['coasesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":area_academica", $datos['area'], PDO::PARAM_STR);
			$sentencia->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
			$sentencia->bindParam(":cve_proy", $datos['cve_proy'], PDO::PARAM_STR);
			$sentencia->bindParam(":descripcion", $datos['descripcion'], PDO::PARAM_STR);
			$sentencia->bindParam(":reporte", $datos['reporte'], PDO::PARAM_STR);
			$sentencia->bindParam(":objetivo", $datos['objetivo'], PDO::PARAM_STR);
			$sentencia->bindParam(":cronograma", json_encode($datos['cronograma']), PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloResidencia::obtenerErrorConsulta($sentencia->errorInfo());
		}
		
		/**
		 * [cambioSolicitudProtocoloRINModelo generación de solicitud del protocolo RIN]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $trabajo [datos del formulario de protocolo RIN]
		 * @return [type]          [description]
		 */
		public function cambioSolicitudProtocoloRINModelo($tabla, $pasante, $trabajo){
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET estadoProtocolo=1 WHERE clavePasante=:clave_pasante AND tipoTrabajo=:trabajo");
			$sentencia->bindParam(":clave_pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":trabajo", $trabajo, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloResidencia::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerErrorConsulta crear arreglo asociativo con el arreglo numérico de entrada]
		 * @param  [array] $arreglo [arreglo de errores de mysql]
		 * @return [array]          [arreglo asociativo generado con el arreglo de entrada]
		 */
		private function obtenerErrorConsulta($arreglo){
			$asociativo = array('COD_ERR', 'ERR_DRI', 'ERR_MSG');
			return array_combine($asociativo, $arreglo);
		}
	}
?>
<?php

	require_once("conexion.php");

	/**
	 * 
	 */
	class ModeloAutoempleo extends Conexion {

		/**
		 * [guardarProtocoloAEMModelo guarda por primera vez los datos de un protocolo AEM]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [datos del formulario de protocolo AEM]
		 * @return [array]         [Falso o Verdadero si la sentencia se ejecuta, si hay un error lo devuelve]
		 */
		public function guardarProtocoloAEMModelo($tabla, $pasante, $datos){
			$cve_tem = date('Y') . date("md") . date("H") . date("i") . date("s");
			$sentencia = Conexion::conectar()->prepare("INSERT INTO $tabla VALUES (null, :cve_tem, :pasante, null, null, :titulo, :periodoInicio, :periodoTermino, :asesor, :area_academica, :planteamiento, :justificacion, :objetivo, :referencias, :cronograma)");
			$sentencia->bindParam(":cve_tem", $cve_tem, PDO::PARAM_STR);
			$sentencia->bindParam(":pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
			$sentencia->bindParam(":periodoInicio", $datos['fecha_ini'], PDO::PARAM_STR);
			$sentencia->bindParam(":periodoTermino", $datos['fecha_fin'], PDO::PARAM_STR);
			$sentencia->bindParam(":asesor", $datos['asesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":area_academica", $datos['area'], PDO::PARAM_STR);
			$sentencia->bindParam(":planteamiento", $datos['planteamiento'], PDO::PARAM_STR);
			$sentencia->bindParam(":justificacion", $datos['justificacion'], PDO::PARAM_STR);
			$sentencia->bindParam(":objetivo", $datos['objetivo'], PDO::PARAM_STR);
			$sentencia->bindParam(":referencias", $datos['referencias'], PDO::PARAM_STR);
			$sentencia->bindParam(":cronograma", json_encode($datos['cronograma']), PDO::PARAM_STR);
			if ($sentencia->execute())
				return true;
			else
				return ModeloAutoempleo::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerProtocoloAEMModelo obtiene los valores del protocolo guardado]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]]
		 * @param  [type] $trabajo [clave del protocolo que se utilizará]
		 * @return [type]          [Falso o Verdadero si la sentencia se ejecuta, si hay un error lo devuelve]
		 */
		public function obtenerProtocoloAEMModelo($tabla, $pasante, $trabajo){
			$sentencia = Conexion::conectar()->prepare("SELECT pro_aem.claveTemporalProtocolo, pro_aem.titulo, pro_aem.periodoInicio, pro_aem.periodoTermino, pro_aem.asesor, pro_aem.areaAcademica, pro_aem.planteamientoProblema, pro_aem.justificacion, pro_aem.objetivoGeneral, pro_aem.objetivoGeneral, pro_aem.referencias, pro_aem.cronograma FROM $tabla JOIN pro_aem ON pasante_protocolo.clavePasante=pro_aem.clavePasante AND pasante_protocolo.claveProtocoloTemporal=pro_aem.claveTemporalProtocolo");
			if ($sentencia->execute())
				return $sentencia->fetchAll();
			else
				return ModeloAutoempleo::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [actualizarProtocoloAEMModelo actualiza los datos de un protocolo AEM]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [datos del formulario de protocolo AEM]
		 * @return [type]          [description]
		 */
		public function actualizarProtocoloAEMModelo($tabla, $pasante, $datos){
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET titulo=:titulo, periodoInicio=:periodoInicio, periodoTermino=:periodoTermino, asesor=:asesor, areaAcademica=:area_academica, planteamientoProblema=:planteamiento, justificacion=:justificacion, objetivoGeneral=:objetivo, referencias=:referencias, cronograma=:cronograma WHERE clavePasante=:clave_pasante");
			$sentencia->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
			$sentencia->bindParam(":periodoInicio", $datos['fecha_ini'], PDO::PARAM_STR);
			$sentencia->bindParam(":periodoTermino", $datos['fecha_fin'], PDO::PARAM_STR);
			$sentencia->bindParam(":asesor", $datos['asesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":area_academica", $datos['area'], PDO::PARAM_STR);
			$sentencia->bindParam(":planteamiento", $datos['planteamiento'], PDO::PARAM_STR);
			$sentencia->bindParam(":justificacion", $datos['justificacion'], PDO::PARAM_STR);
			$sentencia->bindParam(":objetivo", $datos['objetivo'], PDO::PARAM_STR);
			$sentencia->bindParam(":referencias", $datos['referencias'], PDO::PARAM_STR);
			$sentencia->bindParam(":cronograma", json_encode($datos['cronograma']), PDO::PARAM_STR);
			$sentencia->bindParam(":clave_pasante", $pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloAutoempleo::obtenerErrorConsulta($sentencia->errorInfo());
		}
		
		/**
		 * [registrarProtocoloAEMModelo( generación de solicitud del protocolo AEM]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $trabajo [datos del formulario de protocolo AEM]
		 * @return [type]          [description]
		 */
		public function registrarProtocoloAEMModelo($tabla, $pasante, $trabajo){
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET estadoProtocolo=1 WHERE clavePasante=:clave_pasante AND tipoTrabajo=:trabajo");
			$sentencia->bindParam(":clave_pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":trabajo", $trabajo, PDO::PARAM_STR);
			if ($sentencia->execute())
				return true;
			else
				return ModeloAutoempleo::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerErrorConsulta crear arreglo asociativo con el arreglo numérico de entrada]
		 * @param  [array] $arreglo [arreglo de errores de mysql]
		 * @return [array]          [arreglo asociativo generado con el arreglo de entrada]
		 */
		private function obtenerErrorConsulta($arreglo){
			$asociativo = array('COD_ERR', 'ERR_DRI', 'ERR_MSG');
			return array_combine($asociativo, $arreglo);
		}
	}
?>
<?php

	require_once("conexion.php");

	/**
	 * 
	 */
	class ModeloAplicacion extends Conexion {

		/**
		 * [guardarProtocoloRACModelo guarda por primera vez los datos de un protocolo RAC]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [datos del formulario de protocolo RAC]
		 * @return [array]         [Falso o Verdadero si la sentencia se ejecuta, si hay un error lo devuelve]
		 */
		public function guardarProtocoloRACModelo($tabla, $pasante, $datos){
			$cve_tem = date('Y') . date("md") . date("H") . date("i") . date("s");
			$sentencia = Conexion::conectar()->prepare("INSERT INTO $tabla VALUES (null, :cve_tem, :pasante, null, null, :titulo, :asesor, :area_academica,  :planteamiento, :justificacion, :objetivo, :metodologia, :referencias, :cronograma)");
			$sentencia->bindParam(":cve_tem", $cve_tem, PDO::PARAM_STR);
			$sentencia->bindParam(":pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
			$sentencia->bindParam(":area_academica", $datos['area'], PDO::PARAM_STR);
			$sentencia->bindParam(":asesor", $datos['asesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":planteamiento", $datos['planteamiento'], PDO::PARAM_STR);
			$sentencia->bindParam(":justificacion", $datos['justificacion'], PDO::PARAM_STR);
			$sentencia->bindParam(":objetivo", $datos['objetivo'], PDO::PARAM_STR);
			$sentencia->bindParam(":metodologia", $datos['metodologia'], PDO::PARAM_STR);
			$sentencia->bindParam(":referencias", $datos['referencias'], PDO::PARAM_STR);
			$sentencia->bindParam(":cronograma", json_encode($datos['cronograma']), PDO::PARAM_STR);
			if ($sentencia->execute())
				return true;
			else
				return ModeloAplicacion::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerProtocoloRACModelo obtiene los valores del protocolo guardado]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]]
		 * @param  [type] $trabajo [clave del protocolo que se utilizará]
		 * @return [type]          [Falso o Verdadero si la sentencia se ejecuta, si hay un error lo devuelve]
		 */
		public function obtenerProtocoloRACModelo($tabla, $pasante, $trabajo){
			$sentencia = Conexion::conectar()->prepare("SELECT pro_rac.claveTemporalProtocolo, pro_rac.titulo, pro_rac.asesor, pro_rac.areaAcademica, pro_rac.planteamientoProblema, pro_rac.justificacion, pro_rac.objetivoGeneral, pro_rac.metodologia, pro_rac.referencias, pro_rac.cronograma FROM $tabla JOIN pro_rac ON pasante_protocolo.clavePasante=pro_rac.clavePasante AND pasante_protocolo.claveProtocoloTemporal=pro_rac.claveTemporalProtocolo");
			if ($sentencia->execute())
				return $sentencia->fetchAll();
			else
				return ModeloAplicacion::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [actualizarProtocoloRACModelo actualiza los datos de un protocolo RAC]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [datos del formulario de protocolo RAC]
		 * @return [type]          [description]
		 */
		public function actualizarProtocoloRACModelo($tabla, $pasante, $datos){
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET titulo=:titulo, asesor=:asesor, areaAcademica=:area_academica, planteamientoProblema=:planteamiento, justificacion=:justificacion, objetivoGeneral=:objetivo, metodologia=:metodologia, referencias=:referencias, cronograma=:cronograma WHERE clavePasante=:pasante");
			$sentencia->bindParam(":pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
			$sentencia->bindParam(":area_academica", $datos['area'], PDO::PARAM_STR);
			$sentencia->bindParam(":asesor", $datos['asesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":planteamiento", $datos['planteamiento'], PDO::PARAM_STR);
			$sentencia->bindParam(":justificacion", $datos['justificacion'], PDO::PARAM_STR);
			$sentencia->bindParam(":objetivo", $datos['objetivo'], PDO::PARAM_STR);
			$sentencia->bindParam(":metodologia", $datos['metodologia'], PDO::PARAM_STR);
			$sentencia->bindParam(":referencias", $datos['referencias'], PDO::PARAM_STR);
			$sentencia->bindParam(":cronograma", json_encode($datos['cronograma']), PDO::PARAM_STR);
			if ($sentencia->execute())
				return true;
			else
				return ModeloAplicacion::obtenerErrorConsulta($sentencia->errorInfo());
		}
		
		/**
		 * [registrarProtocoloRACModelo generación de solicitud del protocolo RAC]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $trabajo [datos del formulario de protocolo RAC]
		 * @return [type]          [description]
		 */
		public function registrarProtocoloRACModelo($tabla, $pasante, $trabajo){
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET estadoProtocolo=1 WHERE clavePasante=:clave_pasante AND tipoTrabajo=:trabajo");
			$sentencia->bindParam(":clave_pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":trabajo", $trabajo, PDO::PARAM_STR);
			if ($sentencia->execute())
				return true;
			else
				return ModeloAutoempleo::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerErrorConsulta crear arreglo asociativo con el arreglo numérico de entrada]
		 * @param  [array] $arreglo [arreglo de errores de mysql]
		 * @return [array]          [arreglo asociativo generado con el arreglo de entrada]
		 */
		private function obtenerErrorConsulta($arreglo){
			$asociativo = array('COD_ERR', 'ERR_DRI', 'ERR_MSG');
			return array_combine($asociativo, $arreglo);
		}

	}
?>
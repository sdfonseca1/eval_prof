<?php

	require_once("conexion.php");

	/**
	 * 
	 */
	class ModeloTesina extends Conexion {

		/**
		 * [guardarProtocoloTSNModelo guarda por primera vez los datos de un protocolo TSN]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [datos del formulario de protocolo TSN]
		 * @return [array]         [Falso o Verdadero si la sentencia se ejecuta, si hay un error lo devuelve]
		 */
		public function guardarProtocoloTSNModelo($tabla, $pasante, $datos){
			$cve_tem = date('Y') . date("md") . date("H") . date("i") . date("s");
			$sentencia = Conexion::conectar()->prepare("INSERT INTO $tabla VALUES (null, :cve_tem, :pasante, null, null, :titulo, :asesor, :area_academica, :planteamiento, :justificacion, :objetivo, :metodologia, :referencias, :cronograma)");
			$sentencia->bindParam(":cve_tem", $cve_tem, PDO::PARAM_STR);
			$sentencia->bindParam(":pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
			$sentencia->bindParam(":asesor", $datos['asesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":area_academica", $datos['area'], PDO::PARAM_STR);
			$sentencia->bindParam(":planteamiento", $datos['planteamiento'], PDO::PARAM_STR);
			$sentencia->bindParam(":justificacion", $datos['justificacion'], PDO::PARAM_STR);
			$sentencia->bindParam(":objetivo", $datos['objetivo'], PDO::PARAM_STR);
			$sentencia->bindParam(":metodologia", $datos['metodologia'], PDO::PARAM_STR);
			$sentencia->bindParam(":referencias", $datos['referencias'], PDO::PARAM_STR);
			$sentencia->bindParam(":cronograma", json_encode($datos['cronograma']), PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloTesina::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerProtocoloTSNModelo obtiene los valores del protocolo guardado]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]]
		 * @param  [type] $trabajo [clave del protocolo que se utilizará]
		 * @return [type]          [Falso o Verdadero si la sentencia se ejecuta, si hay un error lo devuelve]
		 */
		public function obtenerProtocoloTSNModelo($tabla, $pasante, $trabajo){
			$sentencia = Conexion::conectar()->prepare("SELECT pro_tsn.claveTemporalProtocolo, pro_tsn.titulo, pro_tsn.asesor, pro_tsn.areaAcademica, pro_tsn.planteamientoInv, pro_tsn.justificacion, pro_tsn.objetivoGeneral, pro_tsn.metodologia, pro_tsn.referencias, pro_tsn.cronograma FROM $tabla JOIN pro_tsn ON pasante_protocolo.clavePasante=pro_tsn.clavePasante AND pasante_protocolo.claveProtocoloTemporal=pro_tsn.claveTemporalProtocolo");
			if ($sentencia->execute())
				return $sentencia->fetchAll();
			else
				return ModeloTesina::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [actualizarProtocoloTSNModelo actualiza los datos de un protocolo TSN]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [datos del formulario de protocolo TSN]
		 * @return [type]          [description]
		 */
		public function actualizarProtocoloTSNModelo($tabla, $pasante, $datos){
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET titulo=:titulo, asesor=:asesor, areaAcademica=:area_academica, planteamientoInv=:planteamiento, justificacion=:justificacion, objetivoGeneral=:objetivo, metodologia=:metodologia, referencias=:referencias, cronograma=:cronograma WHERE clavePasante=:clave_pasante");
			$sentencia->bindParam(":clave_pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
			$sentencia->bindParam(":asesor", $datos['asesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":area_academica", $datos['area'], PDO::PARAM_STR);
			$sentencia->bindParam(":planteamiento", $datos['planteamiento'], PDO::PARAM_STR);
			$sentencia->bindParam(":justificacion", $datos['justificacion'], PDO::PARAM_STR);
			$sentencia->bindParam(":objetivo", $datos['objetivo'], PDO::PARAM_STR);
			$sentencia->bindParam(":metodologia", $datos['metodologia'], PDO::PARAM_STR);
			$sentencia->bindParam(":referencias", $datos['referencias'], PDO::PARAM_STR);
			$sentencia->bindParam(":cronograma", json_encode($datos['cronograma']), PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloTesina::obtenerErrorConsulta($sentencia->errorInfo());
		}
		
		/**
		 * [cambioSolicitudProtocoloTSNModelo generación de solicitud del protocolo TSN]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $trabajo [datos del formulario de protocolo TSN]
		 * @return [type]          [description]
		 */
		public function cambioSolicitudProtocoloTSNModelo($tabla, $pasante, $trabajo){
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET estadoProtocolo=1 WHERE clavePasante=:clave_pasante AND tipoTrabajo=:trabajo");
			$sentencia->bindParam(":clave_pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":trabajo", $trabajo, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloTesina::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerErrorConsulta crear arreglo asociativo con el arreglo numérico de entrada]
		 * @param  [array] $arreglo [arreglo de errores de mysql]
		 * @return [array]          [arreglo asociativo generado con el arreglo de entrada]
		 */
		private function obtenerErrorConsulta($arreglo){
			$asociativo = array('COD_ERR', 'ERR_DRI', 'ERR_MSG');
			return array_combine($asociativo, $arreglo);
		}
	}
?>
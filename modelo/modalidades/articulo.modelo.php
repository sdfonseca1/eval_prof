<?php

	require_once("conexion.php");

	/**
	 * 
	 */
	class ModeloArticulo extends Conexion {

		/**
		 * [guardarProtocoloARTModelo guarda por primera vez los datos de un protocolo ART]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [datos del formulario de protocolo ART]
		 * @return [array]         [Falso o Verdadero si la sentencia se ejecuta, si hay un error lo devuelve]
		 */
		public function guardarProtocoloARTModelo($tabla, $pasante, $datos){
			$cve_tem = date('Y') . date("md") . date("H") . date("i") . date("s");
			$sentencia = Conexion::conectar()->prepare("INSERT INTO $tabla VALUES (null, :cve_tem, :pasante, null, null, :titulo, :area_academica, :asesor, :coasesor, :revista, :indice, :descripcion, :referencias, :cronograma)");
			$sentencia->bindParam(":cve_tem", $cve_tem, PDO::PARAM_STR);
			$sentencia->bindParam(":pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
			$sentencia->bindParam(":area_academica", $datos['area'], PDO::PARAM_STR);
			$sentencia->bindParam(":asesor", $datos['asesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":coasesor", $datos['coasesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":revista", $datos['revista'], PDO::PARAM_STR);
			$sentencia->bindParam(":indice", $datos['indice'], PDO::PARAM_STR);
			$sentencia->bindParam(":descripcion", $datos['descripcion'], PDO::PARAM_STR);
			$sentencia->bindParam(":referencias", $datos['referencias'], PDO::PARAM_STR);
			$sentencia->bindParam(":cronograma", json_encode($datos['cronograma']), PDO::PARAM_STR);
			if ($sentencia->execute())
				return true;
			else
				return ModeloArticulo::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerProtocoloARTModelo obtiene los valores del protocolo guardado]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]]
		 * @param  [type] $trabajo [clave del protocolo que se utilizará]
		 * @return [type]          [Falso o Verdadero si la sentencia se ejecuta, si hay un error lo devuelve]
		 */
		public function obtenerProtocoloARTModelo($tabla, $pasante, $trabajo){
			$sentencia = Conexion::conectar()->prepare("SELECT pro_art.claveTemporalProtocolo, pro_art.tituloArticulo, pro_art.areaAcademica, pro_art.asesor, pro_art.coasesor, pro_art.nombreRevista, pro_art.nombreIndice, pro_art.descripcion, pro_art.referencias, pro_art.cronograma FROM $tabla JOIN pro_art ON pasante_protocolo.clavePasante=pro_art.clavePasante AND pasante_protocolo.claveProtocoloTemporal=pro_art.claveTemporalProtocolo");
			if ($sentencia->execute())
				return $sentencia->fetchAll();
			else
				return ModeloArticulo::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [actualizarProtocoloAEMModelo actualiza los datos de un protocolo ART]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $datos  [datos del formulario de protocolo ART]
		 * @return [type]          [description]
		 */
		public function actualizarProtocoloARTModelo($tabla, $pasante, $datos){
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET tituloArticulo=:titulo, areaAcademica=:area_academica, asesor=:asesor, coasesor=:asesor, nombreRevista=:revista, nombreIndice=:indice, descripcion=:descripcion, referencias=:referencias, cronograma=:cronograma WHERE clavePasante=:pasante");
			$sentencia->bindParam(":pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":titulo", $datos['titulo'], PDO::PARAM_STR);
			$sentencia->bindParam(":area_academica", $datos['area'], PDO::PARAM_STR);
			$sentencia->bindParam(":asesor", $datos['asesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":coasesor", $datos['coasesor'], PDO::PARAM_STR);
			$sentencia->bindParam(":revista", $datos['revista'], PDO::PARAM_STR);
			$sentencia->bindParam(":indice", $datos['indice'], PDO::PARAM_STR);
			$sentencia->bindParam(":descripcion", $datos['descripcion'], PDO::PARAM_STR);
			$sentencia->bindParam(":referencias", $datos['referencias'], PDO::PARAM_STR);
			$sentencia->bindParam(":cronograma", json_encode($datos['cronograma']), PDO::PARAM_STR);
			if ($sentencia->execute())
				return true;
			else
				return ModeloArticulo::obtenerErrorConsulta($sentencia->errorInfo());
		}
		
		/**
		 * [registrarProtocoloARTModelo generación de solicitud del protocolo ART]
		 * @param  [text] $tabla   [nombre de la tabla para la sentencia]
		 * @param  [text] $pasante [clave del pasante]
		 * @param  [array] $trabajo [datos del formulario de protocolo ART]
		 * @return [type]          [description]
		 */
		public function registrarProtocoloARTModelo($tabla, $pasante, $trabajo){
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET estadoProtocolo=1 WHERE clavePasante=:clave_pasante AND tipoTrabajo=:trabajo");
			$sentencia->bindParam(":clave_pasante", $pasante, PDO::PARAM_STR);
			$sentencia->bindParam(":trabajo", $trabajo, PDO::PARAM_STR);
			return $sentencia->execute();
		}

		public function existeProtocoloARTModelo($clave_pasante){
			$sentencia = Conexion::conectar()->prepare("SELECT claveTemporalProtocolo FROM pro_art WHERE clavePasante=:pasante");
			$sentencia->bindParam(':pasante', $clave_pasante, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloArticulo::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerErrorConsulta crear arreglo asociativo con el arreglo numérico de entrada]
		 * @param  [array] $arreglo [arreglo de errores de mysql]
		 * @return [array]          [arreglo asociativo generado con el arreglo de entrada]
		 */
		private function obtenerErrorConsulta($arreglo){
			$asociativo = array('COD_ERR', 'ERR_DRI', 'ERR_MSG');
			return array_combine($asociativo, $arreglo);
		}

	}
?>
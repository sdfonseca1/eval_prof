<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 13 AGOSTO 2019
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: direccionamiento de los enlaces en la barra de direcciones con los archivos que corresponden.
*  ANOTACIONES: por favor, cuando edites este archivo, recuerda seguir las reglas de codificación establecidas.
*/
	class Paginas{

		public function enlacesPaginasModelo($enlaces_modelo){
			/*UTILIDAD: lista blanca de valores permitidos en la dirección.
			  PRECONDICION: recibe el nuevo valor de la variale accion de la dirección.
			  POSTCONDICIÓN: muestra la página a la que se quería acceder.
			*/
			// Enlaces de la página en general
			if ($enlaces_modelo == "inicio") {
				$modulo = "vista/modulos/inicio.html";
			}else if ($enlaces_modelo == "contacto"){
				$modulo = "vista/modulos/contacto.html";
			}else if ($enlaces_modelo == "index"){
				$modulo = "vista/modulos/inicio.html";
			}else if ($enlaces_modelo == "cerrarSesion") {
				$modulo = "vista/modulos/cerrarSesion.php";
			}else if($enlaces_modelo == "prueba"){
				$modulo = "vista/modulos/prueba.php";
			}else if ($enlaces_modelo == "olvideMiContrasena"){
				$modulo = "vista/modulos/contrasena_olvidada.php";
			}
			// Enlaces del coordinador
			else if ($enlaces_modelo == "ingresoCoordinador") {
				$modulo = "vista/modulos/coordinador/coordinadorIngreso.php";
			}else if($enlaces_modelo == "coordinador"){
				$modulo = "vista/modulos/coordinador/coordinadorInicio.php";
			}else if($enlaces_modelo == "inicioSesionError_1"){
				$modulo = "vista/modulos/coordinador/coordinadorIngreso.php";
			}
			// Enlaces del pasante
			else if ($enlaces_modelo == "ingresoPasante"){
				$modulo = "vista/modulos/pasante/pasanteIngreso.php";
			}else if ($enlaces_modelo == "registroPasante"){
				$modulo = "vista/modulos/pasante/pasanteRegistro.php";
			}else if ($enlaces_modelo == "pasante"){
				$modulo = "vista/modulos/pasante/pasanteInicio.php";
			}else if($enlaces_modelo == "inicioSesionError_2"){
				$modulo = "vista/modulos/pasante/pasanteIngreso.php";
			}


			// Enlaces del revisor
			else if ($enlaces_modelo == "ingresoRevisor") {
				$modulo = "vista/modulos/revisorIngreso.php";
			}else if ($enlaces_modelo == "revisor") {
				$modulo = "vista/modulos/revisorInicio.html";
			}else if($enlaces_modelo == "inicioSesionError_3"){
				$modulo = "vista/modulos/revisorIngreso.php";
			}
			// Enlaces del encargado de titulación
			else if ($enlaces_modelo == "ingresoTitulacion") {
				$modulo = "vista/modulos/titulacion/titulacionIngreso.php";
			}else if ($enlaces_modelo == "titulacion") {
				$modulo = "vista/modulos/titulacion/titulacionInicio.php";
			}else if($enlaces_modelo == "inicioSesionError_4"){
				$modulo = "vista/modulos/titulacion/titulacionIngreso.php";
			}else{
				$modulo = "vista/modulos/noEncontrado.php";
			}

			
			return $modulo;
		}

	}
?>	
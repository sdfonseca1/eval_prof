<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 13 AGOSTO 2019
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo de conexión a la base de datos.
*  ANOTACIONES: por favor, cuando edites este archivo, recuerda seguir las reglas de codificación establecidas.
*/
	class Conexion{
	
		public function conectarTitulacion(){
			/*UTILIDAD: realiza la conexión a la base de datos.
			  PRECONDICION: se debe definir los valores de las variables para la conexion.
			  POSTCONDICIÓN: la conexión se realiza exitosamente o muestra menssaje de error.
			*/
			try{
				$link = new PDO("mysql:host=localhost;dbname=dbProtocolosTitulacion","root","SDF10ico-@");
				return $link;
			}catch(PDOException $e){
			 	echo "Error: " . $e->getMessage();
			}
		}
	}
?>
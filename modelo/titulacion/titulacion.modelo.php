<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 11 de Abril 2020
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: archivo que contiene los meétodos para hacer consultas a la base de datos y devolverlos al controlador.
*  ANOTACIONES:
*/
	require "conexion.php";
	
	class ModeloTitulacion{

		/**
		 * [ingresoTitulacionModelo obtiene los datos de ingreso del supuesto depto. titulacion]
		 * @param  [text] $tabla             [nombre de la tabla para la sentencia]
		 * @param  [text] $$clave_titulacion [clave del depto. titulacion]
		 * @return [type]                    [resultado de la sentencia]
		 */
		public function ingresoTitulacionModelo($tabla, $titulacion){
			$sentencia = Conexion::conectarTitulacion()->prepare("SELECT claveDepTitu, contrasena FROM $tabla WHERE claveDepTitu=:clave");
			$sentencia->bindParam(":clave", $titulacion, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch();
			else
				return ModeloTitulacion::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [obtenerNombreTitulacionModelo obtiene el nombre y apellido paterno del depto. titulacion registrado]
		 * @param  [text] $tabla [nombre de la tabla donde se realizará la búsqueda]
		 * @param  [text] $clave [clave del depto. titulacion]
		 * @return [object] $sentencia->fetch() [regresa los datos de la consulta o el objeto de error]
		 */
		public function obtenerNombreTitulacionModelo($tabla, $titulacion){
			$sentencia = Conexion::conectarTitulacion()->prepare("SELECT nombre, apellidoPaterno, apellidoMaterno FROM $tabla WHERE claveDepTitu=:claveTitulacion");
			$sentencia->bindParam(":claveTitulacion", $titulacion, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch(PDO::FETCH_ASSOC);
			else
				return ModeloTitulacion::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [numeroSolicitudesPendientesModelo obtiene el número de solicitudes que se han registrado pero no han sido asignadas]
		 * @param  [text] $tabla [nombre de la tabla donde se realizará la búsqueda]
		 * @return [array]       []
		 */
		public function numeroSolicitudesPendientesModelo($tabla){
			$sentencia = Conexion::conectarTitulacion()->prepare("SELECT COUNT(claveProtocoloTemporal) AS num_sol_pend FROM $tabla WHERE estadoProtocolo='1'");
			if ($sentencia->execute())
				return $sentencia->fetch(PDO::FETCH_ASSOC);
			else
				return ModeloTitulacion::obtenerErrorConsulta($sentencia->errorInfo());
		}

		/**
		 * [listaSolicitudesPendientesModelo muestra las solicitudes que no tienen clave del protocolo]
		 * @param  [text] $tabla   [nombre de la tabla donde se realizará la búsqueda]
		 * @param  [int] $reg_ini [Indica el número de registro para comenzar la consulta]
		 * @return [type]          [description]
		 */
		public function listaSolicitudesPendientesModelo($tabla, $reg_ini){
			$sentencia = Conexion::conectarTitulacion()->prepare("SELECT pasante_protocolo.clavePasante, pasante_protocolo.tipotrabajo, pasante_protocolo.claveProtocoloTemporal, pasante.nombre, pasante.apellidoPaterno, pasante.apellidoMaterno, escuela.claveEscuela, escuela.nombre, pasante.licenciatura FROM $tabla JOIN pasante ON pasante_protocolo.clavePasante=pasante.clavePasante JOIN escuela ON pasante.escuela=escuela.claveEscuela WHERE estadoProtocolo=1 LIMIT $reg_ini, 10");
			if ($sentencia->execute())
				return $sentencia->fetchAll(PDO::FETCH_NUM);
			else
				return ModeloTitulacion::obtenerErrorConsulta($sentencia->errorInfo());
		}

		public function obtenerSolicitudProtocoloModelo($tabla, $clave_temporal){
			// var_dump($tabla);
			// var_dump($clave_temporal);
			$sentencia = Conexion::conectarTitulacion()->prepare("SELECT pasante.nombre, pasante.apellidoPaterno, pasante.apellidoMaterno, pasante.licenciatura, pasante.escuela, $tabla.fechaEntrega FROM $tabla JOIN pasante ON $tabla.clavePasante=pasante.clavePasante WHERE $tabla.claveTemporalProtocolo=:clave_temporal");
			$sentencia->bindParam(':clave_temporal', $clave_temporal, PDO::PARAM_STR);
			if ($sentencia->execute())
				return $sentencia->fetch(PDO::FETCH_NUM);
			else
				return ModeloTitulacion::obtenerErrorConsulta($sentencia->errorInfo());
		}





		/**
		 * [obtenerErrorConsulta crear arreglo asociativo con el arreglo numérico de entrada]
		 * @param  [array] $arreglo [arreglo de errores de mysql]
		 * @return [array]          [arreglo asociativo generado con el arreglo de entrada]
		 */
		private function obtenerErrorConsulta($arreglo){
			$asociativo = array('COD_ERR', 'ERR_DRI', 'ERR_MSG');
			return array_combine($asociativo, $arreglo);
		}




	}
?>
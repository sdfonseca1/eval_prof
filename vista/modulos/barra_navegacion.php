    <nav class="navbar navbar-light bg-primary navbar-expand-md fixed-top">
        <div class="container">
            <a href="#" class="navbar-brand">
                <p class="h3 text-white">
                	<img src="imagenes/fingenieria.png" width="120">
                </p>
            <!-- AGREGAR LOGO DE LA PAGINA -->
            </a>
            <button type="button" class="navbar-toggler bg-white" data-toggle="collapse" data-target="#menu-principal" aria-controls="menu-principal" aria-expanded="false" aria-label="Desplegar menu de navegación">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="menu-principal">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a href="index.php?accion=inicio" class="nav-link text-white">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a href="index.php?accion=ingresoCoordinador" class="nav-link text-white">Coordinador</a>
                    </li>
                    <li class="nav-item">
                        <a href="index.php?accion=ingresoPasante" class="nav-link text-white">Pasante</a>
                    </li>
                    <li class="nav-item">
                        <a href="index.php?accion=ingresoRevisor" class="nav-link text-white">Revisor</a>
                    </li>
                    <li class="nav-item">
                        <a href="index.php?accion=ingresoTitulacion" class="nav-link text-white">Titulación</a>
                    </li>
                    <li class="nav-item">
                        <a href="index.php?accion=contacto" class="nav-link text-white">Contacto</a>
                    </li>
                    <?php
                    session_start();
                    if (isset($_SESSION['usuario'])) {
                        echo '
                            <li class="nav-item">
                                <a id="opcCerrarSesion" href="index.php?accion=cerrarSesion" class="nav-link text-white">Salir</a>
                            </li>
                        ';
                    }
                    ?>
                    

                </ul>
           </div>

        </div>
    </nav>
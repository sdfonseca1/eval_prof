<?php
    session_start();
    if (isset($_SESSION['usuario'])) {
        if (!$_SESSION['esPasante'])
            header('location:index.php?accion=ingresoPasante');
    }else{
        header('location:index.php?accion=ingresoPasante');
    }

?>
<script type="text/javascript" src="js/pasante/pasante.js"></script>
<p class="text-center py-3 display-4" id="nombre_pasante">Hola de nuevo, </p>
<div id="accordion" role="tablist">
    <!-- ACORDEÓN MIS DATOS -->
    <div class="card">
        <div class="card-header" role="tab" id="headingOne">
            <h5 class="mb-0">
                <a class="" id="misDatosPasante" data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    Mis datos
                </a>
                <img src="imagenes/info.png" alt="" title="Datos personales del pasante, así como opciones para editarlos o realizar un cambio de contraseña.">
            </h5>
        </div>

        <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="card-body">
                <div class="card">
                    <div class="card-body" id="mdPasante"></div>
                    <div>
                        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal_editar_datos" id="btnEditarDatos">
                            Editar mis datos
                        </button>
                        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal_contrasena">
                            Cambiar contraseña
                        </button>
                    </div>
                </div>
                
            </div>
        </div>
    </div>

    <!-- ACORDEON GENERAR SOLICTUD -->
    <div class="card">
        <div class="card-header" role="tab" id="headingTwo">
            <h5 class="mb-0">
                <a class="" id="solicitudPasante" class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo" style="display: inline;">
                    Generar solicitud
                </a>
                <img src="imagenes/info.png" alt="" title="Selección de tipo de trabajo, formularios, creación y edición de un protocolo, y generación de solicitud.">
            </h5>
        </div>
        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
            <div class="card-body" id="contenidoSolicitudPasante">
                <div class="alert alert-warning" role="alert" id="alerta_solicitud">
                    Una vez que elija la modalidad de trabajo escrito y sea guardado, <strong>NO SE PODRÁ CAMBIAR</strong> de dicha modalidad <strong>ALMENOS</strong> que seleccione la opción del botón <strong>"ELIMINAR"</strong> que aparecerá en lugar de esta nota de información.
                </div>
                <!-- Verificar que si hay o no una solicitud. -->
                <div id="notaExistencia">
                    <select class="custom-select dropdown" id="tipos_trabajo">
                        <option selected value="">Elija una modalidad de trabajo escrito</option>
                    </select>
                </div>
                <hr>
                <!-- Formulario del tipo de trabajo -->
                <div class="my-3" id="formularioTrabajo" style="display: none;"></div>
            </div>
        </div>
    </div>
    <!-- ACORDEON CONSULTAR SOLICITUD -->
    <div class="card">
        <div class="card-header" role="tab" id="headingThree">
            <h5 class="mb-0">
                <a data-toggle="collapse" id="estadoSolicitud" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                    Estado del proceso de evaluación de protocolos
                </a>
                <img src="imagenes/info.png" alt="" title="Consulta el estado de la solicitud que hays generado.">
            </h5>
        </div>
        <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="card-body">
                <p class="alert alert-info">El estado actual del proceso de la evaluación de su protocolo es:</p>
                
            </div>

            <div class="progress-bar-wrapper"></div>
            <div class="my-5"></div>

        </div>
    </div>
</div>

<!-- Modal para cambio de contraseña -->
<div class="modal" id="modal_contrasena" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenteredLabel">Cambio de contraseña</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="cambio_contrasena">
                    <div class="form-group">
                        <label for="cambioContrasena1">Contraseña nueva</label>
                        <input type="password" class="form-control" id="cambioContrasena1" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="cambioContrasena2">Repita contraseña</label>
                        <input type="password" class="form-control" id="cambioContrasena2" placeholder="">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="btnActualizaContrasena">Actualizar</button>
            </div>
        </div>
  </div>
</div>
<!-- modal para cambio de datos -->
<div class="modal" id="modal_editar_datos" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenteredLabel">Editar mis datos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="cambio_datos">
                    <div class="form-group">
                        <label for="cambioNombre">Nombre</label>
                        <input type="text" class="form-control" id="cambioNombre" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="cambioApellidoPaterno">Apellido Paterno</label>
                        <input type="text" class="form-control" id="cambioApellidoPaterno" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="cambioApellidoMaterno">Apellido Materno</label>
                        <input type="text" class="form-control" id="cambioApellidoMaterno" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="cambioFechaNacimiento">Fecha de nacimiento</label>
                        <input type="date" class="form-control" id="cambioFechaNacimiento" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="cambioTelefono">Número de teléfono</label>
                        <input type="text" class="form-control" id="cambioTelefono" placeholder="">
                    </div>
                    <div class="form-group">
                        <label for="cambioCorreo">Correo</label>
                        <input type="text" class="form-control" id="cambioCorreo" placeholder="">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnCerrarModalDatos">Cerrar</button>
                <button type="button" class="btn btn-primary" id="bntActualizarDatos">Actualizar</button>
            </div>
        </div>
  </div>
</div>

<!-- MODAL PARA ELIMINAR EL PROTOCOLO -->
<div class="modal" id="eliminarModal" tabindex="-1" role="dialog" aria-labelledby="eliminarModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="eliminarModalLabel">Eliminar borrador de protocolo de Autoempleo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p class="alert alert-danger">Estás a un paso de eliminar el borrador del protocolo. Si deseas continuar con la <strong>ELIMINACIÓN</strong>, presiona el botón "Eliminar", de lo contrario en botón "Cerrar"</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                <button id="btnEliminarProtocolo" type="button" class="btn btn-danger">Eliminar</button>
            </div>
        </div>
    </div>
</div>
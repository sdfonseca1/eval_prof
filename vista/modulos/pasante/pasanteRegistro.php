<script>
    var url = "js/pasante/pasante.js";
    $.getScript(url);
</script>
<div class="text-center">
	<h1 class="my-5">Registrar pasante...</h1>
	<form method="POST" id="formPasanteRegistro">
  		<div class="form-group">
            <style>
                input[id=pasanteClave]::-webkit-inner-spin-button, 
                input[id=pasanteClave]::-webkit-outer-spin-button { 
                  -webkit-appearance: none; 
                  margin: 0; 
                }
                input[type=number] {
                    -moz-appearance:textfield;
                }
            </style>
    		<label for="pasanteClave">No. de cuenta</label>
    		<input type="number" class="form-control col-8 offset-2 col-md-4 offset-md-4" id="pasanteClave">
        <span id="clave_existente" style="display: none;color: red;">La clave ya existe</span>
  		</div>
  		<div class="form-group">
    		<label for="pasanteNombre">Nombre(s)</label>
    		<input type="text" class="form-control col-8 offset-2 col-md-4 offset-md-4" id="pasanteNombre">
  		</div>
  		<div class="form-group">
    		<label for="pasanteApePat">Apellido paterno</label>
    		<input type="text" class="form-control col-8 offset-2 col-md-4 offset-md-4" id="pasanteApePat">
  		</div>
  		<div class="form-group">
    		<label for="pasanteApeMat">Apellido materno</label>
    		<input type="text" class="form-control col-8 offset-2 col-md-4 offset-md-4" id="pasanteApeMat">
  		</div>
  		<div class="form-group">
    		<label for="pasanteApePat">Fecha de nacimiento</label>
    		<input type="date" class="form-control col-8 offset-2 col-md-4 offset-md-4" id="pasanteFechaNacimiento">
  		</div>
        <div class="form-group">
            <label for="pasanteLicenciatura">Licenciatura</label>
            <select class="form-control col-8 offset-2 col-md-4 offset-md-4" id="pasanteLicenciatura">
                <option>Seleccione uno</option>
            </select>
        </div>
  		<div class="form-group" style="display:none" id="divEscuela">
    		<label for="pasanteEscuela">Escuela de procedencia</label>
    		<select class="form-control col-8 offset-2 col-md-4 offset-md-4" id="pasanteEscuela">
                <option>Seleccione uno</option>
            </select>
  		</div>
  		<div class="form-group">
    		<label for="pasanteTelefono">No. de teléfono</label>
    		<input type="number" class="form-control col-8 offset-2 col-md-4 offset-md-4" id="pasanteTelefono">
  		</div>
  		<div class="form-group">
    		<label for="pasanteCorreo">Correo electrónico</label>
    		<input type="email" class="form-control col-8 offset-2 col-md-4 offset-md-4" id="pasanteCorreo">
  		</div>
  		<div class="form-group">
    		<label for="pasanteContrasena">Contraseña</label>
    		<input type="password" class="form-control col-8 offset-2 col-md-4 offset-md-4" id="pasanteContrasena">
  		</div>
  		<div class="form-group">
    		<label for="pasanteContrasenaRep">Repetir contraseña</label>
    		<input type="password" class="form-control col-8 offset-2 col-md-4 offset-md-4" id="pasanteContrasenaRep">
  		</div>
  		<button id="btnRegistrarPasante" class="btn btn-secondary">Registrar</button>
        <a href="index.php?accion=ingresoPasante" class="btn btn-primary">Regresar</a>
	</form>
</div>
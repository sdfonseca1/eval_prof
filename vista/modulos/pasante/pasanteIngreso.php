<?php
    session_start();
    if (isset($_SESSION['usuario'])) {
        if (!$_SESSION['esPasante']) {
            header('location:index.php?accion=ingresoPasante');
        }else{
        	header('location:index.php?accion=pasante');
        }
    }
?>
<div id="formularioPasante" class="my-5 text-center">
	<h3>Bienvenido Pasante</h3>
	<form class="align-content-center" method="post">
		<label class="" >Usuario</label><br>
		<input type="text" id="clavePasante" placeholder="Usuario">
		<br>
		<label class="">Contraseña</label><br>
		<input type="password" id="contrasenaPasante" placeholder="Contraseña">
		<br>
		<button id="btnIngresoPasante" class="btn btn-secondary my-4">Ingresar</button>
		<a id="btnRegistroPasante" class="btn btn-primary my-4" href="index.php?accion=registroPasante">Registrar</a>
        <br>
        <a href="index.php?accion=olvideMiContrasena">Olvidé mi contraseña</a>
	</form>
</div>


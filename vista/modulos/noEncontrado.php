<br>
<br>
<div class="text-center">
	<p class="display-3 alert alert-danger">404</p>
	<p class="display-4 text-danger">¡Recurso no encontrado!</p>	
</div>
<div class="text-center">
	<img src="imagenes/recurso_no_encontrado.png" alt="" width="400">
</div>
<div>
	<p class="alert alert-info">El recurso al que intentas acceder, no existe o no está disponible. En caso de tener problemas con alguna parte funcional del sitio, haz <a href="index.php?accion=contacto">click aquí</a> para reportar fallas en el sitio.</p>
</div>

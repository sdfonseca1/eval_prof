<?php

	session_start();
	if (isset($_SESSION['usuario'])) {
		$respuesta = session_destroy();
		if ($respuesta) {
			echo "<h1 class='py-5'>Su sesión ha sido finalizada correctamente, en un momento será redirigido.</h1>";
			header('refresh:5; url=index.php?accion=inicio');
		}
	}else{
		header('location:index.php?accion=inicio');
	}

?>
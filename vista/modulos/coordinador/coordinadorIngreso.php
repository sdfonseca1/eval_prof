<?php
    session_start();
    if (isset($_SESSION['esCoordinador']))
    	header('location:index.php?accion=coordinador');
?>
<div id="formularioCoordinador" class="my-5 text-center">
	<h3>Bienvenido Coordinador</h3>
	<form class="align-content-center" method="post">
		<label class="" >Usuario</label><br>
		<input type="text" id="claveCoordinador" placeholder="Usuario">
		<br>
		<label class="">Contraseña</label><br>
		<input type="password" id="contrasenaCoordinador" placeholder="Contraseña" >
		<br>
		<input type="submit" id="btnIngresoCoordinador" class="btn btn-secondary my-4" value="Ingresar">
		<br>
        <a href="index.php?accion=olvideMiContrasena">Olvidé mi contraseña</a>
	</form>
</div>
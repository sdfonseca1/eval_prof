<?php        
    session_start();     
    $num_reg = 10;
    $objeto = new Modelo();
    $num_pasantes = $objeto->contarPasantesPendientesModelo("pasante", $_SESSION['licenciatura']);
    if ($num_pasantes[0] > 0) {
        $paginas = ceil($num_pasantes[0]/$num_reg);
        echo '<nav aria-label="Page navigation example">
                  <ul class="pagination">';
        for ($i=0; $i < $paginas; $i++) { 
            echo '<li class="page-item" data-pagina="'.($i+1).'"><a class="page-link" href="#&pagina='.($i+1).'">'.($i+1).'</a></li>';
        } 
        echo '</ul>
            </nav>';
    }
    else{
        echo "<script>$('#tabla-pasantes').css('display','none')</script>";
        echo '<div class="alert alert-warning" role="alert">
        Sin solicitudes de pasantes pendientes por aceptar.
    </div>';
    }
?>
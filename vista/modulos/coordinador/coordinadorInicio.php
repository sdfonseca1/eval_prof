<?php
	session_start();
    if (isset($_SESSION['esPasante']))
        header('location:index.php?accion=pasante');
    else if (isset($_SESSION['esRevisor']))
        header('location:index.php?accion=revisor');
    else if (isset($_SESSION['esTitulacion']))
        header('location:index.php?accion=titulacion');
?>
<script type="text/javascript" src="js/coordinador/coordinador.js"></script>
<p class="text-center py-3 display-4" id="nombre_coordinador">Hola de nuevo, </p>
<div class="card">
    <div class="card-header" role="tab" id="headingOne">
        <h5 class="mb-0">
            <a class="collapsed" id="pasantesPendientes" data-toggle="collapse" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                Pasantes pendientes
            </a>
            <span class="badge badge-primary" id="badgeNumSolicitudes"></span>
            <img src="imagenes/info.png" alt="" title="Se muestran las solicitudes de los pasantes que han requerido acceso al sistema.">
        </h5>
    </div>
    <!-- Acordeón pasantes pendientes -->
    <div id="collapseOne" class="collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="card-body">
            <div>
                <div class="text-center table-responsive">
                    <table class="table table-sm" id="tabla-pasantes">
                        <thead class="bg-secondary text-white">
                            <tr>
                                <th>No. Cuenta</th>
                                <th>Nombre</th>
                                <th>Escuela</th>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody id="tablaPasantesPendientes"></tbody>
                    </table>
                    <?php
                        require"vista/modulos/coordinador/coordinadorPaginadorPasantes.php";
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Asignar Comités -->
    <div class="modal" id="modalAsignarComite" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Asignación de comité</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <div class="modal-body">
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary">Asignar</button>
            </div>
            </div>
        </div>
    </div>
    <!-- CREACIÓN DE CÓMITES EVALUADORES -->
    <div class="card-header" role="tab" id="headingThree">
        <h5 class="mb-0">
            <a class="collapsed" id="asignacionRevisores" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                Creación de comités evaluadores
            </a>
            <img src="imagenes/info.png" alt="" title="Se crean comites evaluadores para la asignación de revisiones de las solicitudes de los pasantes.">
        </h5>
    </div>
    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
        <div class="card-body">
            <div class="text-center">

            </div>
        </div>
    </div>
    <!-- Acordeón de solicitudes pendientes por asignar revisores. -->
    <div class="card-header" role="tab" id="headingTwo">
        <h5 class="mb-0">
            <a class="collapsed" id="asignacionRevisores" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Asignación de revisores
            </a>
            <span class="badge badge-primary" id="badgeNumSolicitudes">2</span>
            <img src="imagenes/info.png" alt="" title="Asignación de cómites evaluadores a las solicitudes de los pasantes que ya han sido registradas.">
        </h5>
    </div>
    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
        <div class="card-body">
            <div>
                <div class="text-center table-responsive">
                    <form>
                        <p>Datos de ejemplo</p>
                        <table class="table table-sm" id="tabla-integrante">
                            <thead class="bg-secondary text-white">
                                <tr>
                                    <th>No. cuenta</th>
                                    <th>Solicitud</th>
                                    <th>Nombre</th>
                                    <th>Escuela</th>
                                    <th>Licenciatura</th>
                                    <th>Asignar</th>
                                </tr>
                            
                            </thead>
                            <tbody id="solicitudRevisores"></tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
    




    



</div>
<?php
    session_start();
    if (isset($_SESSION['esPasante']))
        header('location:index.php?accion=pasante');
    else if (isset($_SESSION['esRevisor']))
        header('location:index.php?accion=revisor');
    else if (isset($_SESSION['esCoordinador']))
        header('location:index.php?accion=coordinador');
?>
<script type="text/javascript" src="js/titulacion/titulacion.js"></script>
<p class="text-center py-3 display-4" id="nombre_titulacion">Hola de nuevo, </p>
<!-- ACORDEÓN MIS SOLICITUDES -->
<div class="card">
    <div class="card-header" role="tab" id="headingOne">
        <h5 class="mb-0">
            <a class="" id="misSolicitudes" data-toggle="collapse" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                Solicitudes pendientes
            </a>
            <span class="badge badge-primary" id="badgeNumSolicitudesTitu"></span>
            <img src="imagenes/info.png" alt="" title="Se muestran las solicitudes pendientes de los pasantes, listas para asignar su clave de protocolo.">
        </h5>
    </div>
    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
        <div class="card-body table-responsive">
            <table class="table table-sm text-center" id="tabla-solicitudes">
                <thead class="bg-secondary text-white">
                    <tr>
                        <th>Clave</th>
                        <th>Nombre</th>
                        <th>Carrera</th>
                        <th>Escuela</th>
                        <th>Trabajo</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="contenido-solicitudes"></tbody>
            </table>
            <?php
                require"vista/modulos/titulacion/paginadorSolicitudes.php";
            ?>
        </div>
    </div>
</div>

<!-- ACORDEÓN BUSCAR SOLICITUDES YA APROBADAS -->
<div class="card">
    <div class="card-header" role="tab" id="headingThree">
        <h5 class="mb-0">
            <a class="" id="misSolicitudes" data-toggle="collapse" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                Búsqueda de solicitudes
            </a>
            <img src="imagenes/info.png" alt="" title="Se pueden consultar las solicitudes registradas, ya sea por clave de protocolo, clave del pasante o por fecha.">
        </h5>
    </div>

    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
        <div class="card-body">
            <div class="row">
                <div class="dropdown col-sm-1">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Elija una opción
                    </button>
                    <div class="dropdown-menu text-center" aria-labelledby="dropdownMenu2">
                        <a class="dropdown-item" href="">Clave de protocolo</a>
                        <a class="dropdown-item" href="">Clave de pasante</a>
                        <a class="dropdown-item" href="">Fecha</a>
                    </div>
                </div>
                
            </div>
            <div id="contenido-busqueda">
                
            </div>
        </div>
    </div>
</div>

<!-- Modal Asignar Clave de Protocolo-->
<div class="modal" id="asignar_clave" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalCenteredLabel">Asignación de clave</h5>
                <button type="button" class="close btnModal" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <form id="formClaveAsignacion">
                    <div class="form-group">
                        <label for="nombreClaveAsignacion">Nombre</label>
                        <input id="nombreClaveAsignacion" class="form-control" type="text" readonly="">
                    </div>
                    <div class="form-group">
                        <label for="licClaveAsignacion">Licenciatura</label>
                        <input id="licClaveAsignacion" class="form-control" type="text" readonly="">
                    </div>
                    <div class="form-group">
                        <label for="escuelaClaveAsignacion">Escuela</label>
                        <input id="escuelaClaveAsignacion" class="form-control" type="text" readonly="">
                    </div>
                    
                    <div class="form-group">
                        <label for="fechEntClaveAsignacion">Fecha y Hora de entrega</label>
                        <input id="fechEntClaveAsignacion" class="form-control" type="text" readonly="">
                    </div>
                    <div class="form-group">
                        <label for="cveClaveAsignacion">Clave a Asignar</label>
                        <input id="cveClaveAsignacion" class="form-control" type="text">
                        <span class="alert alert-danger" style="display: none;"></span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btnModal" data-dismiss="modal" id="bntAsignarClavePro">Asignar clave</button>
            </div>
        </div>
    </div>
</div>
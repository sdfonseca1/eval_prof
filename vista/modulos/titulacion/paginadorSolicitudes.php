<?php        
    session_start();     
    $num_reg = 10;
    $objeto = new Modelo();
    $num_solicitudes = $objeto->numeroSolicitudesPendientesModelo("pasante_protocolo");
    if ($num_solicitudes[0] > 0) {
        $paginas = ceil($num_solicitudes[0]/$num_reg);
        echo '<nav aria-label="Page navigation example">
                  <ul class="pagination">';
        for ($i=0; $i < $paginas; $i++) { 
            echo '<li class="page-item" data-pagina="'.($i+1).'"><a class="page-link" href="#&pagina='.($i+1).'">'.($i+1).'</a></li>';
        } 
        echo '</ul>
            </nav>';
    }
    else{
        echo "<script>$('#tabla-solicitudes').css('display','none')</script>";
        echo '<div class="alert alert-warning" role="alert">
        Sin solicitudes pendientes por aceptar.
    </div>';
    }
?>
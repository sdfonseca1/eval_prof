<!DOCTYPE html>
<html lang="es">
	<head>
	    <title>Solicitud Y Seguimiento de Evaluación Profesional - UAEM</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" type="image/x.icon" href="imagenes/favicon.ico">
        <meta http-equiv="x-ua-compatible" content="ie-edge">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/estilos.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="css/main.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
        <script type="text/javascript" src="js/funciones.js"></script>


    </head>
<!-- BARRA DE NAVEGACIÓN -->

<br>
<body>
<?php
require_once("vista/modulos/barra_navegacion.php");
?>
    <!-- Inicio -->
    <div class="container my-5 p-4">
        <?php
            if ($_SERVER['REQUEST_URI'] == "/eval_prof/"){
                header('location:index.php?accion=inicio');
            }
            $mvc = new Controlador();
            $mvc->enlacesPaginasControlador();
        ?>
    </div>
    <?php 
        require_once "modulos/extras/modal_error.html";
        require_once "modulos/extras/modal_exito.html";
        require_once "modulos/extras/modal_info.html";
    ?>

    <!-- PIE DE PAGINA -->

	<script type="text/javascript" src="js/jquery-3.4.1.min.js" ></script>
	<script type="text/javascript" src="js/popper.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.tabletojson.min.js"></script>
    <script type="text/javascript" src="js/raphael.js"></script>
    <script type="text/javascript" src="js/progress-bar.js"></script>    
</body>

<!-- Icons made by <a href="https://www.flaticon.com/authors/kiranshastry" title="Kiranshastry">Kiranshastry</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a> -->
<?php
// require_once("vista/modulos/pie_pagina.html");
?>

</html>
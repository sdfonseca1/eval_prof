<?php
	session_start();
	require_once "../../../controlador/coordinador/coordinador.controlador.php";
	require_once "../../../modelo/coordinador/coordinador.modelo.php";
	
	$funcion = $_POST["funcion"];
	switch ($funcion) {
		case 'ingresoCoordinador':
			$respuesta = ControladorCoordinador::ingresoCoordinadorControlador($_POST['usuario'], $_POST['contrasena']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerNombreCoordinador':
			$respuesta = ControladorCoordinador::obtenerNombreCoordinadorControlador($_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'numeroPasantesPendientes':
			$respuesta = ControladorCoordinador::numeroPasantesPendientesControlador($_SESSION['licenciatura']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'listaPasantesPendientes':
			$respuesta = ControladorCoordinador::listaPasantesPendientesControlador($_SESSION['licenciatura'], $_POST['pagina']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'aceptarPasantePendiente':
			$respuesta = ControladorCoordinador::aceptarPasantePendienteControlador($_POST['clave_pasante']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'rechazarPasantePendiente':
			$respuesta = ControladorCoordinador::rechazarPasantePendienteControlador($_POST['clave_pasante']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
	}
?>
<?php
	session_start();
	require_once "../../../controlador/titulacion/titulacion.controlador.php";
	require_once "../../../modelo/titulacion/titulacion.modelo.php";
	
	$funcion = $_POST["funcion"];
	switch ($funcion) {
		case 'ingresoTitulacion':
			$respuesta = ControladorTitulacion::ingresoTitulacionControlador($_POST['usuario'], $_POST['contrasena']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerNombreTitulacion':
			$respuesta = ControladorTitulacion::obtenerNombreTitulacionControlador($_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'numeroSolicitudesPendientes':
			$respuesta = ControladorTitulacion::numeroSolicitudesPendientesControlador();
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'solicitudesPendientes':
			$respuesta = ControladorTitulacion::listaSolicitudesPendientesControlador($_POST['pagina']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'datosSolicitudPasante':
			$respuesta = ControladorTitulacion::obtenerSolicitudControlador($_POST['cve_tem'], $_POST['trabajo']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		
	}
?>
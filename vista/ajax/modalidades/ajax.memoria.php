<?php
	session_start();
	require_once "../../../controlador/modalidades/memoria.controlador.php";
	require_once "../../../modelo/modalidades/memoria.modelo.php";

	$funcion = $_POST["funcion"];

	switch ($funcion) {
		case 'guardarProtocoloMEL':
			$respuesta = ControladorMemoria::guardarProtocoloMELControlador($_SESSION['usuario'], $_POST['datos']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerProtocoloMEL':
			$respuesta = ControladorMemoria::obtenerProtocoloMELControlador($_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'actualizarProtocoloMEL':
			$respuesta = ControladorMemoria::actualizarProtocoloMELControlador($_SESSION['usuario'], $_POST['protocolo']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'cambioSolicitudProtocoloMEL':
			$respuesta = ControladorMemoria::cambioSolicitudProtocoloMELControlador($_SESSION['usuario'], 'MEL');
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
	}




?>
<?php
	session_start();
	require_once "../../../controlador/modalidades/tesina.controlador.php";
	require_once "../../../modelo/modalidades/tesina.modelo.php";

	$funcion = $_POST["funcion"];

	switch ($funcion) {
		case 'guardarProtocoloTSN':
			$respuesta = ControladorTesina::guardarProtocoloTSNControlador($_SESSION['usuario'], $_POST['datos']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerProtocoloTSN':
			$respuesta = ControladorTesina::obtenerProtocoloTSNControlador($_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'actualizarProtocoloTSN':
			$respuesta = ControladorTesina::actualizarProtocoloTSNControlador($_SESSION['usuario'], $_POST['protocolo']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'cambioSolicitudProtocoloTSN':
			$respuesta = ControladorTesina::cambioSolicitudProtocoloTSNControlador($_SESSION['usuario'], 'TSN');
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
	}
?>
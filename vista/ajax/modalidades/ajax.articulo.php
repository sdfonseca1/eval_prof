<?php
	session_start();
	require_once "../../../controlador/modalidades/articulo.controlador.php";
	require_once "../../../modelo/modalidades/articulo.modelo.php";

	$funcion = $_POST["funcion"];

	switch ($funcion) {
		case 'guardarProtocoloART':
			$respuesta = ControladorArticulo::guardarProtocoloARTControlador($_SESSION['usuario'], $_POST['datos']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerProtocoloART':
			$respuesta = ControladorArticulo::obtenerProtocoloARTControlador($_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'actualizarProtocoloART':
			$respuesta = ControladorArticulo::actualizarProtocoloARTControlador($_SESSION['usuario'], $_POST['protocolo']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'cambioSolicitudProtocoloART':
			$respuesta = ControladorArticulo::registrarProtocoloARTControlador($_SESSION['usuario'], 'ART', $_POST['protocolo']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
	}




?>
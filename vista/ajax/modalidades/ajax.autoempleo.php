<?php
	session_start();
	require_once "../../../controlador/modalidades/autoempleo.controlador.php";
	require_once "../../../modelo/modalidades/autoempleo.modelo.php";

	$funcion = $_POST["funcion"];

	switch ($funcion) {
		case 'guardarProtocoloAEM':
			$respuesta = ControladorAutoempleo::guardarProtocoloAEMControlador($_SESSION['usuario'], $_POST['datos']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerProtocoloAEM':
			$respuesta = ControladorAutoempleo::obtenerProtocoloAEMControlador($_POST, $_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'actualizarProtocoloAEM':
			$respuesta = ControladorAutoempleo::actualizarProtocoloAEMControlador($_SESSION['usuario'], $_POST['protocolo']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'cambioSolicitudProtocoloAEM':
			$respuesta = ControladorAutoempleo::cambioSolicitudProtocoloAEMControlador($_SESSION['usuario'], 'AEM');
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'registrarProtocoloAEM':
			$respuesta = ControladorAutoempleo::registrarProtocoloAEMControlador($_SESSION['usuario'], 'AEM', $_POST['protocolo']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
	}
?>
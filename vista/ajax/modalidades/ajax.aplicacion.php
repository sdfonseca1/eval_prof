<?php
	session_start();
	require_once "../../../controlador/modalidades/aplicacion.controlador.php";
	require_once "../../../modelo/modalidades/aplicacion.modelo.php";

	$funcion = $_POST["funcion"];

	switch ($funcion) {
		case 'guardarProtocoloRAC':
			$respuesta = ControladorAplicacion::guardarProtocoloRACControlador($_SESSION['usuario'], $_POST['datos']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerProtocoloRAC':
			$respuesta = ControladorAplicacion::obtenerProtocoloRACControlador($_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'actualizarProtocoloRAC':
			$respuesta = ControladorAplicacion::actualizarProtocoloRACControlador($_SESSION['usuario'], $_POST['protocolo']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'cambioSolicitudProtocoloRAC':
			$respuesta = ControladorAplicacion::cambioSolicitudProtocoloRACControlador($_SESSION['usuario'], 'RAC', $_POST['protocolo']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
	}




?>
<?php
	session_start();
	require_once "../../../controlador/modalidades/residencia.controlador.php";
	require_once "../../../modelo/modalidades/residencia.modelo.php";

	$funcion = $_POST["funcion"];

	switch ($funcion) {
		case 'guardarProtocoloRIN':
			$respuesta = ControladorResidencia::guardarProtocoloRINControlador($_SESSION['usuario'], $_POST['datos']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerProtocoloRIN':
			$respuesta = ControladorResidencia::obtenerProtocoloRINControlador($_POST, $_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'actualizarProtocoloRIN':
			$respuesta = ControladorResidencia::actualizarProtocoloRINControlador($_SESSION['usuario'], $_POST['protocolo']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'cambioSolicitudProtocoloRIN':
			$respuesta = ControladorResidencia::cambioSolicitudProtocoloRINControlador($_SESSION['usuario'], 'RIN');
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
	}
?>
<?php
	session_start();
	require_once "../../../controlador/modalidades/ensayo.controlador.php";
	require_once "../../../modelo/modalidades/ensayo.modelo.php";

	$funcion = $_POST["funcion"];

	switch ($funcion) {
		case 'guardarProtocoloENS':
			$respuesta = ControladorEnsayo::guardarProtocoloENSControlador($_SESSION['usuario'], $_POST['datos']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerProtocoloENS':
			$respuesta = ControladorEnsayo::obtenerProtocoloENSControlador($_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'actualizarProtocoloENS':
			$respuesta = ControladorEnsayo::actualizarProtocoloENSControlador($_SESSION['usuario'], $_POST['protocolo']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'cambioSolicitudProtocoloENS':
			$respuesta = ControladorEnsayo::registrarProtocoloENSControlador($_SESSION['usuario'], 'ART', $_POST['protocolo']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
	}
?>
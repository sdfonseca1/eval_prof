<?php
	session_start();
	require_once "../../../controlador/modalidades/tesis.controlador.php";
	require_once "../../../modelo/modalidades/tesis.modelo.php";

	$funcion = $_POST["funcion"];

	switch ($funcion) {
		case 'guardarProtocoloTSS':
			$respuesta = ControladorTesis::guardarProtocoloTSSControlador($_SESSION['usuario'], $_POST['datos']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerProtocoloTSS':
			$respuesta = ControladorTesis::obtenerProtocoloTSSControlador($_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'actualizarProtocoloTSS':
			$respuesta = ControladorTesis::actualizarProtocoloTSSControlador($_SESSION['usuario'], $_POST['protocolo']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'cambioSolicitudProtocoloTSS':
			$respuesta = ControladorTesis::cambioSolicitudProtocoloTSSControlador($_SESSION['usuario'], 'TSS');
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
	}
?>
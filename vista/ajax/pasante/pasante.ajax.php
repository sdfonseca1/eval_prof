<?php
	session_start();
	require_once "../../../controlador/pasante/pasante.controlador.php";
	require_once "../../../modelo/pasante/pasante.modelo.php";

	$funcion = $_POST['funcion'];
	
	switch ($funcion) {
		case 'ingresoPasante':
			$respuesta = ControladorPasante::ingresoPasanteControlador($_POST);
			echo json_encode($respuesta);
			break;
		case 'buscarClavePasante':
			$respuesta = ControladorPasante::buscarClavePasanteControlador($_POST['clave']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerSolicitudesPasantes':
			header('Content-Type: applikcation/json');
			$respuesta = ControladorPasante::obtenerSolicitudesPasantesControlador($_SESSION['licenciatura'], $_POST['num_reg']);
			echo json_encode($respuesta);
			break;
		case 'obtenerMisDatosPasante':
			$respuesta = ControladorPasante::obtenerMisDatosPasanteControlador($_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'mostrarDatosAEdicion':
			$respuesta = ControladorPasante::mostrarDatosAEdicionControlador($_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'actualizarDatosPasante':
			$respuesta = ControladorPasante::actualizarDatosPasanteControlador($_SESSION['usuario'], $_POST);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerNombrePasante':
			$respuesta = ControladorPasante::obtenerNombrePasanteControlador($_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'actualizarContrasena':
			$respuesta = ControladorPasante::actualizarContrasenaPasanteControlador($_SESSION['usuario'], $_POST);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'existeBorradorProtocolo':
			$respuesta = ControladorPasante::existeBorradorProtocoloControlador($_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'registrarPasante':
			$respuesta = ControladorPasante::registrarPasanteControlador($_POST['registro']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'solicitudRegistrada':
			$respuesta = ControladorPasante::existeSolicitudRegistradaControlador($_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'etapaProceso':
			$respuesta = ControladorPasante::obtenerEtapaProcesoControlador($_SESSION['usuario']);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerLicenciaturas':
			$respuesta = ControladorPasante::obtenerLicenciaturasControlador();
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerEscuelas':
			$respuesta = ControladorPasante::obtenerEscuelasControlador();
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
	}
	
?>